
			<!-- MAIN -->
			<div class="main">
				<!-- MAIN CONTENT -->
				<div class="main-content">
					<div class="container">
						<div class="content-heading clearfix">
							<div class="heading-left">
								<h1 class="page-title">Hi <?php echo $this->session->userdata('user_name') ?></h1>
								<p class="page-subtitle"><b>Arafah</b> Electronics & Furniture</p>
							</div>
							<ul class="breadcrumb">
								<li><a href="#"><i class="fa fa-home"></i> Dashboad</a></li>
								<!-- <li><a href="#">Pages</a></li> -->
								<!-- <li class="active">Blank Page</li> -->
							</ul>
						</div>
						<!-- <div class="container-fluid">
							asdasdas
						</div> -->
						<div class="row">
							<div class="col-md-12">
								<div class="panel">
									<div class="panel-heading">
										<h3 class="panel-title">Generate QR Code</h3>
									</div>
									<div class="panel-body">
										<form id="basic-form" class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>qrc/main/generate" enctype="multipart/form-data">
												
												<!-- <div class="form-group">
													<label for="preview-image" class="col-sm-3 control-label"></label>
													<div class="col-sm-9">
														<img src="<?php //echo base_url().'assets/images/image-empty.png'; ?>" id="preview-image" style="width: 250px;" class="w3-border w3-padding" alt="Image">
													</div>
												</div> -->

												<!-- <div class="form-group">
													<label for="upload-pictures" class="col-sm-3 control-label">Uplooad Photo ID Konsumen</label>
													<div class="col-md-9">
														<input type="file" id="upload-pictures" name="post_file" accept=".jpg,.jpeg,.png" required>
														<p class="help-block">
															<em>Valid file type: .jpg, .jpeg, .png.</em>
														</p>
													</div>
												</div> -->

												<div class="form-group">
													<label for="qrc-ksm-no-id" class="col-sm-3 control-label">Nomor ID Konsumen</label>
													<div class="col-sm-9">
														<input type="text" class="form-control" id="qrc-ksm-no-id" name="post_ksm_no_id" placeholder="No. ID" required>
													</div>
												</div>

												<div class="form-group">
													<label for="qrc-ksm-nama" class="col-sm-3 control-label">Nama Konsumen</label>
													<div class="col-sm-9">
														<input type="text" class="form-control" id="qrc-ksm-nama" name="post_ksm_nama" placeholder="Nama" required>
													</div>
												</div>

												<div class="form-group">
													<label for="qrc-amount" class="col-sm-3 control-label">Total Belanja</label>
													<div class="col-sm-9">
														<div class="input-group">
															<input type="number" min="1" class="form-control" id="qrc-amount" name="post_amount" placeholder="-" required>
															<span class="input-group-addon">HKD</span>
														</div>
														<!-- 
														<p class="help-block">
															<em>Isikan 0 jika konsumen ingin mengisi sendiri jumlahnya</em>
														</p> -->
													</div>
												</div>

												<div class="form-group">
													<label for="qrc-ksm-keterangan" class="col-sm-3 control-label">Keterangan</label>
													<div class="col-sm-9">
														<textarea class="form-control" id="qrc-ksm-keterangan" name="post_ksm_keterangan" placeholder="" rows="4"></textarea>
													</div>
												</div>

												<div class="form-group">
													<div class="col-sm-offset-3 col-sm-9">
														<button type="submit" class="btn btn-primary btn-block" onclick="return confirm('Apakah data sudah benar semua?')">Generate QR Code</button>
													</div>
												</div>
										</form>
									</div>
								</div>

							</div>

						</div>
					</div>
				</div>
				<!-- END MAIN CONTENT -->
			</div>
			<!-- END MAIN -->