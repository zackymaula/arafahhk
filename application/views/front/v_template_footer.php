<?php
$this->db->select('*');
$this->db->from('u_contact_official');
$query = $this->db->get();
$data = $query->row();

if ($this->session->userdata('staff_id') == '') {
	$data_nama = 'Arafah';
	$data_cp = $data->contact_person;
	$data_email = $data->email;
	$data_photo = 'assets/images/icon-customer-service.png';
	$data_address = $data->address;

} else {
	$data_nama = $this->session->userdata('staff_nama');
	$data_cp = $this->session->userdata('staff_cp');
	$data_email = $this->session->userdata('staff_email');
	$data_photo = $this->session->userdata('staff_photo');
	$data_address = $data->address;
}
?>

			<footer id="footer">
				<div class="container mt-4 pt-2 pb-5"> <!-- footer-top-light-border -->
					<div class="row py-4">
						<div class="col-md-3 text-center text-md-left">
							<h5 class="text-4 text-color-light mb-3 mt-4 mt-lg-0">INFORMASI KONTAK</h5>
							<p class="text-3 mb-3"><i class="fas fa-map-marker text-color-primary"></i> <?php echo $data_address ?></p>
							<p class="text-3 mb-3"><i class="fas fa-phone text-color-primary"></i><a href="tel:<?php echo $data_cp ?>"> +<?php echo $data_cp ?></a></p>
							<p class="text-3 mb-3"><i class="fas fa-envelope text-color-primary"></i><a href="<?php echo $data_email ?>"> <?php echo $data_email ?></a></p>
							<p class="text-3 mb-0"><i class="fab fa-facebook-f text-color-primary"></i><a href="https://www.facebook.com/halloarafah/" target="_blank"> Hello Arafah</a></p>
						</div>
						<div class="col-md-9 text-center text-md-left">
							<div class="row">
								<!-- <div class="col-md-8 col-lg-8 mb-0">
									<h5 class="text-4 text-color-light mb-3 mt-4 mt-lg-0">PANDUAN BELANJA</h5>
									<p class="mb-1"><a href="#" class="text-3 link-hover-style-1"><i class="fas fa-shopping-cart text-color-primary"></i> Cara Belanja</a></p>
									<p class="mb-1"><a href="#" class="text-3 link-hover-style-1"><i class="fas fa-credit-card text-color-primary"></i> Cara Membayar</a></p>
								</div> -->

								<!-- <div class="col-md-5 col-lg-4">
									<h5 class="text-4 text-color-light mb-3 mt-4 mt-lg-0">JAM KERJA</h5>
									<p class="mb-1"><a href="#" class="text-3 link-hover-style-1"><i class="fas fa-clock text-color-primary"></i> Senin - Sabtu / 7:00 - 17:00</a></p>
								</div> -->
							</div>
							<div class="row footer-top-light-border mt-4 pt-4">
								<div class="col-lg-4 text-center text-md-left">
									<p class="text-2">Merk : </p>
								</div>
								<div class="col-lg-8 text-center text-md-left">
									<img src="<?php echo base_url(); ?>assets/images/contents/merk-1-sharp.jpg" alt="Product icons" class="img-fluid mt-1 mt-lg-1 rounded">
									<img src="<?php echo base_url(); ?>assets/images/contents/merk-2-samsung.jpg" alt="Product icons" class="img-fluid mt-1 mt-lg-1 rounded">
									<img src="<?php echo base_url(); ?>assets/images/contents/merk-3-honda.jpg" alt="Product icons" class="img-fluid mt-1 mt-lg-1 rounded">
									<img src="<?php echo base_url(); ?>assets/images/contents/merk-4-yamaha.jpg" alt="Product icons" class="img-fluid mt-1 mt-lg-1 rounded">
									<img src="<?php echo base_url(); ?>assets/images/contents/merk-5-toshiba.jpg" alt="Product icons" class="img-fluid mt-1 mt-lg-1 rounded">
									<img src="<?php echo base_url(); ?>assets/images/contents/merk-6-polytron.jpg" alt="Product icons" class="img-fluid mt-1 mt-lg-1 rounded">
									<img src="<?php echo base_url(); ?>assets/images/contents/merk-7-lg.jpg" alt="Product icons" class="img-fluid mt-1 mt-lg-1 rounded">
									<img src="<?php echo base_url(); ?>assets/images/contents/merk-8-lenovo.jpg" alt="Product icons" class="img-fluid mt-1 mt-lg-1 rounded">
									<img src="<?php echo base_url(); ?>assets/images/contents/merk-9-acer.jpg" alt="Product icons" class="img-fluid mt-1 mt-lg-1 rounded">
									<img src="<?php echo base_url(); ?>assets/images/contents/merk-10-asus.jpg" alt="Product icons" class="img-fluid mt-1 mt-lg-1 rounded">
									<img src="<?php echo base_url(); ?>assets/images/contents/merk-11-airland.jpg" alt="Product icons" class="img-fluid mt-1 mt-lg-1 rounded">
									<img src="<?php echo base_url(); ?>assets/images/contents/merk-12-canon.jpg" alt="Product icons" class="img-fluid mt-1 mt-lg-1 rounded">
									<img src="<?php echo base_url(); ?>assets/images/contents/merk-13-sony.jpg" alt="Product icons" class="img-fluid mt-1 mt-lg-1 rounded">
									<img src="<?php echo base_url(); ?>assets/images/contents/merk-14-fujifilm.jpg" alt="Product icons" class="img-fluid mt-1 mt-lg-1 rounded">
									<img src="<?php echo base_url(); ?>assets/images/contents/merk-15-nikon.jpg" alt="Product icons" class="img-fluid mt-1 mt-lg-1 rounded">
									<img src="<?php echo base_url(); ?>assets/images/contents/merk-16-xiaomi.jpg" alt="Product icons" class="img-fluid mt-1 mt-lg-1 rounded">
									<img src="<?php echo base_url(); ?>assets/images/contents/merk-17-olympus.jpg" alt="Product icons" class="img-fluid mt-1 mt-lg-1 rounded">
								</div>
							</div>
							<div class="row mt-3 pt-3">
								<div class="col-lg-4 text-center text-md-left">
									<p class="text-2">Secure by : </p>
								</div>
								<div class="col-lg-8 text-center text-md-left">
									<span id="siteseal"><script async type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=7zvObvt4FVs0MhFkNZ96QE5j3Q8X4VPUmKPdfxmTE4s1azRX0xzy4AyZSbX8"></script></span>
								</div>
							</div>
							<div class="row mt-3 pt-3">
								<div class="col-lg-12 text-center text-md-right">
									<p class="text-2">© Copyright 2019 Arafah.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</footer>
		</div>

		<!-- <div class="mybuttonWA">
			<a class="buttonWA" href="https://api.whatsapp.com/send?phone=<?php //echo $data_cp ?>&amp;text=Hello%20Admin" rel="nofollow" target="_blank"><i class="fab fa-whatsapp text-4"> </i> <b>Chat Langsung Yuk!</b></a>
		</div> -->

		<div class="placeButton">
			<a href="https://api.whatsapp.com/send?phone=<?php echo $data_cp ?>&amp;text=Hi <?php echo $data_nama ?>, %0A%0ASaya tertarik untuk berbelanja di ARAFAH. Mohon dibantu bagaimana cara belanja di ARAFAH. %0A%0ATerima kasih" data-action="share/whatsapp/share" rel="nofollow" target="_blank">
				<!-- <img class="designButton" src="<?php echo base_url().$data_photo; ?>" alt="Photo Profil"> -->
				<div class="designButtonBackground" href="" rel="nofollow" target="_blank">
					<img src="<?php echo base_url().'assets/images/icon-wa-chat-yuk.png'; ?>" alt="Photo Profil" style="width:50px; height:50px;"><br>
					<img class="designButtonProfile" src="<?php echo base_url().$data_photo; ?>" alt="Photo Profil">
				</div>
			</a>
		</div>

		<!-- Vendor -->
		<script src="<?php echo base_url(); ?>assets/template-frontend/vendor/jquery/jquery.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-frontend/vendor/jquery.appear/jquery.appear.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-frontend/vendor/jquery.easing/jquery.easing.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-frontend/vendor/jquery.cookie/jquery.cookie.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-frontend/vendor/popper/umd/popper.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-frontend/vendor/bootstrap/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-frontend/vendor/common/common.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-frontend/vendor/jquery.validation/jquery.validate.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-frontend/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-frontend/vendor/jquery.gmap/jquery.gmap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-frontend/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-frontend/vendor/isotope/jquery.isotope.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-frontend/vendor/owl.carousel/owl.carousel.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-frontend/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-frontend/vendor/vide/jquery.vide.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-frontend/vendor/vivus/vivus.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-frontend/vendor/bootstrap-star-rating/js/star-rating.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-frontend/vendor/bootstrap-star-rating/themes/krajee-fas/theme.min.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url(); ?>assets/template-frontend/js/theme.js"></script>

		<!-- Current Page Vendor and Views -->
		<script src="<?php echo base_url(); ?>assets/template-frontend/js/views/view.shop.js"></script>
		
		<!-- Theme Custom -->
		<script src="<?php echo base_url(); ?>assets/template-frontend/js/custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url(); ?>assets/template-frontend/js/theme.init.js"></script>
		
		<!-- Examples -->
		<script src="<?php echo base_url(); ?>assets/template-frontend/js/examples/examples.gallery.js"></script>

		<!-- SIGNATURE -->
		<script src="<?php echo base_url(); ?>assets/signature/js/numeric-1.2.6.min.js"></script> 
		<script src="<?php echo base_url(); ?>assets/signature/js/bezier.js"></script>
		<script src="<?php echo base_url(); ?>assets/signature/js/jquery.signaturepad.js"></script> 
		<script src="<?php echo base_url(); ?>assets/signature/js/html2canvas.js" type='text/javascript'></script>
		<script src="<?php echo base_url(); ?>assets/signature/js/json2.min.js"></script>

		<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
			ga('create', 'UA-12345678-1', 'auto');
			ga('send', 'pageview');
		</script>
		 -->

		<script type="text/javascript">

			$('#loading').hide();
			$('#row_btn_more').show();
			$('#btn_more').css('cursor','pointer');
			$(document).ready(function(){
				var i = 24;
				//var last_id = 0;
				$(document).on('click', '#btn_more', function(){
					/*if (i == 0) {
						last_id = $(this).data("lid");
						i = last_id;
					};*/
					//$('#btn_more').html("Loading...");
					$('#loading').show();
					$('#row_btn_more').hide();
					$.ajax({
						url:"<?php echo base_url().'main/load_more_product' ?>",
						method:"POST",
						data:{'offset': i,
								'limit': 24},
						dataType:"text",
						success:function(data){
							if (data != '') {
								//$('#remove_row').remove();
								$('#loading').hide();
								$('#row_btn_more').show();
								$('#load_data_table').append(data);
							} else{
								$('#row_btn_more').hide();
								//$('#btn_more').html('No Data');
							}
						}
					});
					i += 24;
				});
			});

		</script>

		<script type="text/javascript">

			$(document).ready(function() {
				$('#signArea').signaturePad({drawOnly:true, drawBezierCurves:true, lineTop:90});
			});

			$("#btnSaveSign").click(function(e){
				/*if (confirm('Are you sure you want to save this thing into the database?')) {
				    // Save it!
				} else {
				    // Do nothing!
				}*/
				html2canvas([document.getElementById('sign-pad')], {
					onrendered: function (canvas) {
						var canvas_img_data = canvas.toDataURL('image/png');
						var img_data = canvas_img_data.replace(/^data:image\/(png|jpg);base64,/, "");
						//ajax call to save image inside folder
						$.ajax({
							url: "<?php echo base_url().'marketing/order/insert_pic_sign' ?>",
							data: { img_data:img_data },
							type: 'post',
							dataType: 'json'
						});
					}
				});
			});

		</script>

	</body>
</html>
