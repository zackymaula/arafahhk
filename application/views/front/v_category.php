<?php
$this->db->select('*');
$this->db->from('u_contact_official');
$query = $this->db->get();
$data = $query->row();
$country_kurs = $data->country_kurs;
?>
			<div role="main" class="main shop">

				<section class="page-header page-header-modern bg-color-light page-header-sm" style="margin-top: -2rem">
					<div class="container">
						<div class="row">
							
							<?php if($content_category!='0') { ?>
							<div class="col-md-12 align-self-center p-static order-2 text-center">
								<h1 class="text-dark text-uppercase">Kategori <strong><?php echo $content_categories_name ?></strong></h1>
							</div>
							<?php } ?>

							<div class="col-md-12 align-self-center order-1">

								<ul class="breadcrumb d-block text-center">
									<li><a href="<?php echo base_url(); ?>main">Beranda</a></li>
									<li class="active"><?php echo $content_categories_name ?></li>
								</ul>
							</div>
						</div>
					</div>
				</section>

				<div class="container" style="margin-top: -3rem">
					
					<?php if($content_category!='0') { ?>

					<div class="row">
						<div class="col">
							<div class="row">
								<div class="col-lg-12 mx-auto">

									<div class="d-flex" style="margin-top: -2rem">
										<div class="mr-auto p-2"><i class="fas fa-angle-left text-color-primary"></i> geser kiri</div>
										<div class="p-2">geser kanan <i class="fas fa-angle-right text-color-primary"></i></div>
									</div>

									<div class="thumb-gallery">
										<div class="owl-carousel owl-theme manual thumb-gallery-thumbs d-flex justify-content-center" id="thumbGalleryThumbs" >

											<?php
											foreach ($content_category_sub as $content_category_sub) {
											?>

											<div>
												<a href="<?php echo base_url(); ?>category_sub/view/<?php echo $content_category_sub['id_category_sub'] ?>">
												<span class="img-thumbnail d-block cur-pointer" style="border: 3px outset #00AAFF;">
													<img alt="Project Image" src="<?php echo base_url().$content_category_sub['file_path'].$content_category_sub['file_name']; ?>" class="img-fluid"> <!-- assets/template-frontend/img/projects/project-2.jpg -->
												</span>
												</a>
												<span>
													<h4 class="text-4 text-primary font-weight-extra-bold" style="text-align:center"><?php echo $content_category_sub['cs_name'] ?></h4>
												</span>
											</div>

											<?php }; ?>

										</div>
									</div>
								</div>
							</div>

						</div>
					</div>

					<?php } ?>

					<div class="row">
						<div class="col">
							<div class="heading heading-border heading-middle-border heading-middle-border-center text-center">
								<h2>Produk <span class="text-color-primary"><?php echo $content_categories_name ?></span></h2>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-12">
							<div class="masonry-loader masonry-loader-showing">
								
								<div class="row products product-thumb-info-list" data-plugin-masonry data-plugin-options="{'layoutMode': 'fitRows'}">
								
									<!-- col-4 col-md-3 mb-4 mb-md-0 / col-12 col-sm-6 col-lg-3 -->

									<?php foreach ($data_products as $data_products) { ?>

									<div class="col-4 col-md-3 mb-4 mb-md-0 product"> 
										<?php //if ($data_products['price_discount']!=NULL) { ?>
										<!-- <a href="<?php //echo base_url().'product/view/'.$data_products['id_products']; ?>">
											<span class="onsale">Promo</span>
										</a> -->
										<?php //} ?>
										<span class="product-thumb-info border-0">
											<!-- <a href="<?php //echo base_url(); ?>cart" class="add-to-cart-product bg-color-primary">
												<span class="text-uppercase text-1">Masukkan Keranjang</span>
											</a> -->
											<a href="<?php echo base_url().'product/view/'.$data_products['id_products']; ?>">
												<span class="product-thumb-info-image">
													<img alt="" class="img-fluid" src="<?php echo base_url().$data_products['file_path'].$data_products['file_name']; ?>"> <!-- assets/template-frontend/img/products/product-grey-1.jpg -->
												</span>
											</a>
											<span class="product-thumb-info-content product-thumb-info-content pl-0 bg-color-light">
												<a href="<?php echo base_url().'product/view/'.$data_products['id_products']; ?>">
													<h4 class="text-4 text-primary"><?php echo $data_products['product_name']; ?></h4>
													<span class="price">
														<?php if ($data_products['price_discount']==NULL) { ?>
															<span class="amount text-dark font-weight-semibold"><?php echo $data_products['price_regular'].' '.$country_kurs; ?></span>
														<?php } else { ?>
															<del><span class="amount"><?php echo $data_products['price_regular'].' '.$country_kurs; ?></span></del>
															<ins><span class="amount text-secondary font-weight-semibold"><?php echo $data_products['price_discount'].' '.$country_kurs; ?></span></ins>
														<?php } ?>
														
													</span>
												</a>
											</span>
										</span>
									</div>

									<?php }; ?>
								
								</div>

								<!-- <div class="row">
									<div class="col">
										<ul class="pagination float-right">
											<li class="page-item"><a class="page-link" href="#"><i class="fas fa-angle-left"></i></a></li>
											<li class="page-item active"><a class="page-link" href="#">1</a></li>
											<li class="page-item"><a class="page-link" href="#">2</a></li>
											<li class="page-item"><a class="page-link" href="#">3</a></li>
											<a class="page-link" href="#"><i class="fas fa-angle-right"></i></a>
										</ul>
									</div>
								</div> -->
							</div>
						</div>
					</div>

				</div>

			</div>
