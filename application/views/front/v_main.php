<?php
if ($this->session->userdata('staff_id') != '') {
	$this->db->select('*');
	$this->db->from('c_sliders cs');
	$this->db->join('u_staff us', 'cs.id_staff = us.id_staff');
	$this->db->order_by('cs.id_staff', 'asc');
	$this->db->where('cs.id_staff', $this->session->userdata('staff_id'));
	$query = $this->db->get();
	$data_slider = $query->result_array();
} 

$this->db->select('*');
$this->db->from('u_contact_official');
$query = $this->db->get();
$data = $query->row();
$country_kurs = $data->country_kurs;
?>

			<div role="main" class="main shop">

				<div class="container">

					<div class="row">
						<div class="col-12">
							<div class="appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="500">
								<div class="owl-carousel owl-theme nav-inside nav-style-1 nav-light mt-2" data-plugin-options="{'items': 1, 'margin': 10, 'nav': true, 'dots': false, 'animateOut': 'fadeOut', 'autoplay': true, 'autoplayTimeout': 5000, 'loop': true}">
									
									<?php foreach ($data_slider as $slider) { ?>
									<div>
										<a href="<?php echo base_url().'slide/'.$slider['id_slide']; ?>">
											<div class="img-thumbnail border-0 p-0 d-block">
												<img class="img-fluid border-radius-1" src="<?php echo base_url().$slider['file_path'].$slider['file_name']; ?>" alt="">
											</div>
										</a>
									</div>
									<?php }; ?>

								</div>
							</div>
						</div>
					</div>


					<div class="row mt-3">
						<div class="col">
							<div class="heading heading-border heading-middle-border heading-middle-border-center text-center heading-border-xl">
								<!-- <h2>Pilih <span class="text-color-secondary">Kategori</span></h2> -->
								<!-- <h2 class="word-rotator clip is-full-width">
									<span class="word-rotator-words bg-secondary rounded">
										<b class="is-visible">New!</b>
										<b>New!</b>
									</span>
								</h2> -->
								
								<h2 class="word-rotator rotate-1 mb-2">
									<span class="word-rotator-words bg-primary rounded">
										<b class="is-visible">Promo <?php echo date('F'); ?></b>
										<b>Promo <?php echo date('F'); ?></b>
										<!-- <b class="text-color-quaternary">Pilih Kategori</b> -->
									</span>
								</h2>
								<br>
								<h2 class="word-rotator push mb-2">
									<span class="word-rotator-words bg-secondary rounded">
										<b class="is-visible">New!</b>
										<b>New!</b>
									</span>
								</h2>
								<!-- <h2 class="word-rotator push mb-2">
									<span class="word-rotator-words">
										<b class="is-visible text-danger">New!</b>
										<b class="text-danger">New!</b>
									</span>
								</h2> -->
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-12">
							<div class="masonry-loader masonry-loader-showing">
								<div class="row products product-thumb-info-list" data-plugin-masonry data-plugin-options="{'layoutMode': 'fitRows'}" >

									<?php foreach ($data_promo as $data_promo) { ?>
									<div class="col-4 col-md-4 mb-4 mb-md-0 product"> 
										<?php //if ($data_products['price_discount']!=NULL) { ?>
										<!-- <a href="<?php //echo base_url().'product/view/'.$data_products['id_products']; ?>">
											<span class="onsale">Promo</span>
										</a> -->
										<?php //} ?>
										<a href="<?php echo base_url().'promo/'.$data_promo['id_promo']; ?>">
											<span class="onsale">New!</span>
										</a>
										<span class="product-thumb-info border-0">
											<a href="<?php echo base_url().'promo/'.$data_promo['id_promo']; ?>">
												<span class="product-thumb-info-image">
													<img alt="" class="img-fluid" src="<?php echo base_url().$data_promo['promo_file_path'].$data_promo['promo_file_name']; ?>">
												</span>
											</a>
											<span class="product-thumb-info-content product-thumb-info-content pl-0 bg-color-light">
												<a href="<?php echo base_url().'promo/'.$data_promo['id_promo']; ?>">
													<h4 class="text-4 text-primary"><?php echo $data_promo['promo_name']; ?></h4>
													<span class="price">
														<?php //if ($data_promo['promo_discount']==NULL) { ?>
															<span class="amount text-dark font-weight-semibold"><?php echo $data_promo['promo_price'].' '.$country_kurs; ?></span>
														<?php //} else { ?>
															<!-- <del><span class="amount"><?php //echo $data_promo['promo_price']; ?></span></del>
															<ins><span class="amount text-dark font-weight-semibold"><?php //echo $data_promo['promo_discount']; ?></span></ins> -->
														<?php //} ?>
														
													</span>
												</a>
											</span>
										</span>
									</div>
									<?php 
									//$last_id = $data_products['id_products'];
									}; ?>

								</div>
							</div>
						</div>
					</div>

					<!-- <div class="row mt-3">
						<div class="col">
							<div class="heading heading-border heading-middle-border heading-middle-border-center text-center">
								<h2><span class="text-color-primary">Layanan</span> Kita</h2>
							</div>
						</div>
					</div> -->

					<div class="row mt-3">
						<div class="col">
							<div class="heading heading-border heading-middle-border heading-middle-border-center text-center heading-border-xl">
								<h2 class="word-rotator rotate-1 mb-2">
									<span class="word-rotator-words bg-primary rounded">
										<b class="is-visible">Layanan Kita</b>
										<b>Layanan Kita</b>
									</span>
								</h2>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-12">
							<div class="masonry-loader masonry-loader-showing">
								<div class="row products product-thumb-info-list" data-plugin-masonry data-plugin-options="{'layoutMode': 'fitRows'}">
									
									<!-- col-4 col-md-3 mb-4 mb-md-0 / col-12 col-sm-6 col-lg-3 -->

									<div class="col-3 col-md-3 mb-4 mb-md-0 product"> 
										<span class="product-thumb-info border-0 bg-color-light">
											<span class="product-thumb-info-image">
												<img alt="" class="img-fluid" src="<?php echo base_url(); ?>assets/images/services/icon-kirim-ke-rumah.png">
											</span>
											<span class="pl-0">
												<h4 class="text-3" style="text-align:center">Kirim ke Rumah</h4>
											</span>
										</span>
									</div>

									<div class="col-3 col-md-3 mb-4 mb-md-0 product">
										<span class="product-thumb-info border-0 bg-color-light">
											<span class="product-thumb-info-image">
												<img alt="" class="img-fluid" src="<?php echo base_url(); ?>assets/images/services/icon-cara-bayar.png">
											</span>
											<span class="pl-0">
												<h4 class="text-3" style="text-align:center">Pilih Sendiri Cara Bayarnya</h4>
											</span>
										</span>
									</div>
									<div class="col-3 col-md-3 mb-4 mb-md-0 product"> 
										<span class="product-thumb-info border-0 bg-color-light">
											<span class="product-thumb-info-image">
												<img alt="" class="img-fluid" src="<?php echo base_url(); ?>assets/images/services/icon-cicilan.png">
											</span>
											<span class="pl-0">
												<h4 class="text-3" style="text-align:center">Pilih Sendiri Cicilannya</h4>
											</span>
										</span>
									</div>

									<div class="col-3 col-md-3 mb-4 mb-md-0 product">
										<span class="product-thumb-info border-0 bg-color-light">
											<span class="product-thumb-info-image">
												<img alt="" class="img-fluid" src="<?php echo base_url(); ?>assets/images/services/icon-tanpa-biaya-admin.png">
											</span>
											<span class="pl-0">
												<h4 class="text-3" style="text-align:center">Tanpa Biaya Admin</h4>
											</span>
										</span>
									</div>

									<div class="col-4 col-md-4 mb-4 mb-md-0 product">
										<span class="product-thumb-info border-0 bg-color-light">
											<span class="product-thumb-info-image">
												<img alt="" class="img-fluid" src="<?php echo base_url(); ?>assets/images/services/icon-tanpa-dp.png">
											</span>
											<span class="pl-0">
												<h4 class="text-3" style="text-align:center">Tanpa DP (Down Payment)</h4>
											</span>
										</span>
									</div>
									<div class="col-4 col-md-4 mb-4 mb-md-0 product"> 
										<span class="product-thumb-info border-0 bg-color-light">
											<span class="product-thumb-info-image">
												<img alt="" class="img-fluid" src="<?php echo base_url(); ?>assets/images/services/icon-free-ongkir.png">
											</span>
											<span class="pl-0">
												<h4 class="text-3" style="text-align:center">Free Ongkir</h4>
											</span>
										</span>
									</div>

									<div class="col-4 col-md-4 mb-4 mb-md-0 product">
										<span class="product-thumb-info border-0 bg-color-light">
											<span class="product-thumb-info-image">
												<img alt="" class="img-fluid" src="<?php echo base_url(); ?>assets/images/services/icon-garansi-resmi.png">
											</span>
											<span class="pl-0">
												<h4 class="text-3" style="text-align:center">Garansi Resmi dan Original Baru</h4>
											</span>
										</span>
									</div>

								</div>
							</div>
						</div>
					</div>


					<div class="row mt-5">
						<div class="col">
							<div class="heading heading-border heading-middle-border heading-middle-border-center text-center heading-border-xl">
								<h2 class="word-rotator rotate-1 mb-2">
									<span class="word-rotator-words bg-primary rounded">
										<b class="is-visible">Keluarga ARAFAH</b>
										<b>Keluarga ARAFAH</b>
									</span>
								</h2>
							</div>
						</div>
					</div>


					<div class="row">
						<div class="col-12">

							<div class="owl-carousel owl-theme stage-margin" data-plugin-options="{'items': 3, 'autoplay': true, 'autoplayTimeout': 3000, 'margin': 10, 'nav': true, 'dots': false, 'stagePadding': 40}">
								<?php foreach ($data_testimoni as $testimoni) { ?>
								<div>
									<img alt="" class="img-fluid rounded" src="<?php echo base_url().$testimoni['testi_file_path'].$testimoni['testi_file_name']?>">
								</div>
								<?php }; ?>
							</div>
						</div>
					</div>


					<!-- <div class="row">
						<div class="col-12">
							<div class="owl-carousel owl-theme" data-plugin-options="{'items': 1, 'autoplay': true, 'autoplayTimeout': 4000, 'margin': 10, 'animateIn': 'slideInDown', 'animateOut': 'slideOutDown'}">
								<a href="#">
									<div>
										<img alt="" class="img-fluid rounded" src="<?php //echo base_url(); ?>assets/images/contents/2_1.jpeg">
									</div>
								</a>
								<a href="#">
									<div>
										<img alt="" class="img-fluid rounded" src="<?php //echo base_url(); ?>assets/images/contents/2_2.jpeg">
									</div>
								</a>
							</div>
						</div>
					</div> -->

					<!-- <div class="row mt-3">
						<div class="col">
							<div class="heading heading-border heading-middle-border heading-middle-border-center text-center">
								<h2><span class="text-color-primary">Kategori</span> Kita</h2>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-12">
						<div class="masonry-loader masonry-loader-showing">
							<div class="row products product-thumb-info-list" data-plugin-masonry data-plugin-options="{'layoutMode': 'fitRows'}">
								 -->
								<!-- col-4 col-md-3 mb-4 mb-md-0 / col-12 col-sm-6 col-lg-3 -->

								<!-- <?php
								//foreach ($data_categories as $categories) {
								?>

								<div class="col-6 col-md-6 mb-4 mb-md-0 product"> 
									<span class="product-thumb-info border-0  bg-color-light">
										<a href="<?php //echo base_url(); ?>category/view/<?php //echo $categories['id_categories'] ?>">
											<span class="product-thumb-info-image">
												<img alt="" class="img-fluid" src="<?php //echo base_url().$categories['file_path'].$categories['file_name']; ?>">
											</span>
										</a>
										<a href="<?php //echo base_url(); ?>category/view/<?php //echo $categories['id_categories'] ?>">
											<span class="pl-0">
												<h4 class="text-5 text-primary" style="text-align:center"><?php //echo $categories['name']; ?></h4>
											</span>
										</a>
									</span>
								</div>
								<?php //}; ?> -->

								<!-- <div class="col-6 col-md-6 mb-4 mb-md-0 product">
									<span class="product-thumb-info border-0 bg-color-light">
										<a href="#">
											<span class="product-thumb-info-image">
												<img alt="" class="img-fluid" src="<?php //echo base_url(); ?>assets/images/icon-arafahelectronics.png">
											</span>
										</a>
										<span class="pl-0">
											<a href="#">
												<h4 class="text-5 text-primary" style="text-align:center">Furniture</h4>
											</a>
										</span>
									</span>
								</div>

							</div>
						</div>
						</div>
					</div> -->

					<div class="row mt-5">
						<div class="col">
							<div class="heading heading-border heading-middle-border heading-middle-border-center text-center heading-border-xl">
								<!-- <h2>Pilih <span class="text-color-secondary">Kategori</span></h2> -->
								<h2 class="word-rotator rotate-1 mb-2">
									<span class="word-rotator-words bg-primary rounded">
										<b class="is-visible">Pilih Kategori</b>
										<b>Pilih Kategori</b>
										<!-- <b class="text-color-quaternary">Pilih Kategori</b> -->
									</span>
								</h2>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col">
							<div class="row">
								<div class="col-lg-12 mx-auto">

									<div class="d-flex" style="margin-top: -2rem">
										<div class="mr-auto p-2"><i class="fas fa-angle-left text-color-primary"></i> geser kiri</div>
										<div class="p-2">geser kanan <i class="fas fa-angle-right text-color-primary"></i></div>
									</div>

									<div class="thumb-gallery">
										<div class="owl-carousel owl-theme manual thumb-gallery-thumbs d-flex justify-content-center" id="thumbGalleryThumbs" >

											<?php foreach ($data_categories as $categories) { ?>

											<div>
												<a href="
												<?php 
												if ($categories['id_categories']=='1') {
													echo base_url().'paket';
												} else if ($categories['id_categories']=='7') {
													echo base_url().'bangunan';
												} else {
													echo base_url().'category/view/'.$categories['id_categories']; 
												}
												?>
												">
												<!-- <span class="img-thumbnail d-block cur-pointer">
													<img alt="Project Image" src="<?php //echo base_url().$categories['file_path'].$categories['file_name']; ?>" class="img-fluid">
												</span> -->
												<!-- <img class="img-fluid rounded" src="<?php //echo base_url().$categories['file_path'].$categories['file_name']; ?>" alt="Project Image"> -->
												<span class="img-thumbnail d-block cur-pointer" style="border: 3px outset #00AAFF;">
													<img src="<?php echo base_url().$categories['file_path'].$categories['file_name']; ?>" alt="Project Image">
												</span>
												</a>
												<span style="text-align:center">
													<h4 class="text-4 text-primary font-weight-extra-bold" style="text-align:center"><?php echo $categories['name']; ?> </h4>
												</span> 
											</div>

											<?php }; ?>

										</div>
									</div>
								</div>
							</div>

						</div>
					</div>

					 <!-- style="margin-top: -2rem" -->
					<!-- <div class="row"> 
						<div class="col">
							<div class="heading heading-border heading-middle-border heading-middle-border-center text-center">
								<h2>Produk-Produk <span class="text-color-primary">Arafah</span></h2>
							</div>
						</div>
					</div> -->

					<div class="row">
						<div class="col">
							<div class="heading heading-border heading-middle-border heading-middle-border-center text-center heading-border-xl">
								<!-- <h2>Pilih <span class="text-color-secondary">Kategori</span></h2> -->
								<h2 class="word-rotator rotate-1 mb-2">
									<span class="word-rotator-words bg-primary rounded">
										<b class="is-visible">Produk-Produk Arafah</b>
										<b>Produk-Produk Arafah</b>
										<!-- <b class="text-color-quaternary">Pilih Kategori</b> -->
									</span>
								</h2>
							</div>
						</div>
					</div>

					<div class="row" >
						<div class="col-12">
							<div class="masonry-loader masonry-loader-showing">
								<div class="row products product-thumb-info-list" data-plugin-masonry data-plugin-options="{'layoutMode': 'fitRows'}" >

									<?php foreach ($data_product_limit as $data_products) { ?>
									<div class="col-4 col-md-3 mb-4 mb-md-0 product"> 
										<?php //if ($data_products['price_discount']!=NULL) { ?>
										<!-- <a href="<?php //echo base_url().'product/view/'.$data_products['id_products']; ?>">
											<span class="onsale">Promo</span>
										</a> -->
										<?php //} ?>
										<span class="product-thumb-info border-0">
											<a href="<?php echo base_url().'product/view/'.$data_products['id_products']; ?>">
												<span class="product-thumb-info-image">
													<img alt="" class="img-fluid" src="<?php echo base_url().$data_products['file_path'].$data_products['file_name']; ?>">
												</span>
											</a>
											<span class="product-thumb-info-content product-thumb-info-content pl-0 bg-color-light">
												<a href="<?php echo base_url().'product/view/'.$data_products['id_products']; ?>">
													<h4 class="text-4 text-primary"><?php echo $data_products['product_name']; ?></h4>
													<span class="price">
														<?php if ($data_products['price_discount']==NULL) { ?>
															<span class="amount text-dark font-weight-semibold"><?php echo $data_products['price_regular'].' '.$country_kurs; ?></span>
														<?php } else { ?>
															<del><span class="amount"><?php echo $data_products['price_regular'].' '.$country_kurs; ?></span></del>
															<ins><span class="amount text-secondary font-weight-semibold"><?php echo $data_products['price_discount'].' '.$country_kurs; ?></span></ins>
														<?php } ?>
														
													</span>
												</a>
											</span>
										</span>
									</div>
									<?php 
									//$last_id = $data_products['id_products'];
									}; ?>

								</div>
							</div>
						</div>
					</div>

					<div class="col-12">
						<div class="row products product-thumb-info-list" id="load_data_table"></div>
					</div>

					<div class="row" id="loading">
						<div class="col">
							<div class="heading heading-border heading-middle-border heading-middle-border-center text-center">
								<div class="progress mb-2">
									<div class="progress-bar progress-bar-primary progress-bar-striped progress-bar-animated active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
										<!-- <span class="sr-only">45% Complete</span> -->
									</div>
								</div>
								<!-- <h4 class="word-rotator letters scale mb-2">
									<span>loading...</span> -->
									<!-- 
									<span class="word-rotator-words bg-primary rounded">
										<b class="is-visible">loading...</b>
										<b>loading...</b>
									</span> -->
								<!-- </h4> -->
							</div>
						</div>
					</div>

					<div class="row" id="row_btn_more">
						<div class="col">
							<div class="heading heading-border heading-middle-border heading-middle-border-center text-center">
								<a id="btn_more"> <!-- data-lid="<?php //echo $last_id; ?>" -->
									<h4 class="word-rotator letters scale mb-2">
										<span>Lihat produk lainnya</span>
										<span class="word-rotator-words bg-primary rounded">
											<b class="is-visible">Klik Disini !!!</b>
											<b>Klik Disini !!!</b>
										</span>
									</h4>
								</a>
							</div>
						</div>
					</div>



				</div>

			</div>
