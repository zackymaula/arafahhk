			<div role="main" class="main shop">

				<section class="page-header page-header-modern bg-color-light page-header-sm" style="margin-top: -2rem">
					<div class="container">
						<div class="row">

							<div class="col-md-12 align-self-center order-1">

								<ul class="breadcrumb d-block text-center">
									<li><a href="<?php echo base_url(); ?>main">Beranda</a></li>
									<li class="active">Detail</li>
								</ul>
							</div>
						</div>
					</div>
				</section>

				<div class="container" style="margin-top: -3rem">

					<div class="row">
						<div class="col">
							<span class="img-thumbnail d-block">
								<img class="img-fluid" src="<?php echo base_url().$content_slide_filepath.$content_slide_filename ?>" alt="Slide">
							</span>
						</div>
					</div>

					<div class="row mt-4">
						<div class="col">

							<div class="accordion" id="accordion10">
								<div class="card card-default">
									<div class="card-header">
										<h4 class="card-title m-0">
											<a class="accordion-toggle text-color-dark font-weight-bold" data-toggle="collapse" data-parent="#accordion10" href="#collapse10Three">
												<i class="fas fa-bars text-color-primary"></i> Deskripsi
											</a>
										</h4>
									</div>
									<div id="collapse10Three" class="collapse show">
										<div class="card-body">
											<?php echo $content_slide_description ?>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>

			</div>
