			<div role="main" class="main shop">

				<section class="page-header page-header-modern bg-color-light page-header-sm" style="margin-top: -2rem">
					<div class="container">
						<div class="row">

							<div class="col-md-12 align-self-center order-1">

								<ul class="breadcrumb d-block text-center">
									<li><a href="<?php echo base_url(); ?>main">Beranda</a></li>
									<li class="active">Promo Detail</li>
								</ul>
							</div>
						</div>
					</div>
				</section>

				<div class="container" style="margin-top: -3rem">

					<div class="row">
						<div class="col-lg-6">

							<div class="owl-carousel owl-theme" data-plugin-options="{'items': 1}">

								<div>
									<img alt="" class="img-fluid" src="<?php echo base_url().$content_promo_filepath.$content_promo_filename; ?>"> <!-- assets/images/contents/c_television.png -->
								</div>

							</div>

						</div>

						<div class="col-lg-6">

							<div class="summary entry-summary">

								<h1 class="mb-0 font-weight-bold text-7"><?php echo $content_promo_name ?></h1>
								<!-- 
								<div class="pb-0 clearfix">
									<div title="Rated 3 out of 5" class="float-left">
										<input type="text" class="d-none" value="3" title="" data-plugin-star-rating data-plugin-options="{'displayOnly': true, 'color': 'primary', 'size':'xs'}">
									</div>

									<div class="review-num">
										<span class="count" itemprop="ratingCount">2</span> reviews
									</div>
								</div> -->

								<p class="price">
									<?php //if ($content_promo_discount==NULL) { ?>
										<span class="amount"><?php echo $content_promo_price ?></span>
									<?php //} else { ?>
										<!-- <del class="text-1"><span class="amount text-1"><?php //echo $content_promo_price ?></span></del>
										<ins class="text-decoration-none"><span class="amount text-dark font-weight-semibold"><?php //echo $content_promo_discount ?></span></ins> -->
									<?php //} ?>
									<!-- <span class="amount">$22</span> -->
									<!-- <del class="text-1"><span class="amount text-1">$325</span></del>
									<ins class="text-decoration-none"><span class="amount text-dark font-weight-semibold">$299</span></ins> -->
								</p>
								<!-- 
								<p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus nibh sed elimttis adipiscing. Fusce in hendrerit purus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus nibh sed elimttis adipiscing. Fusce in hendrerit purus. </p>
								 -->
								 
								<!--
								<form enctype="multipart/form-data" method="post" class="cart">
									<div class="quantity quantity-lg">
										<input type="button" class="minus" value="-">
										<input type="text" class="input-text qty text" title="Qty" value="1" name="quantity" min="1" step="1">
										<input type="button" class="plus" value="+">
									</div>
									<a href="<?php //echo base_url(); ?>cart" class="btn btn-primary btn-modern text-uppercase">Masukkan Keranjang</a>
								</form> -->

								<!-- <div class="product-meta">
									<span class="posted-in">Tag: <a rel="tag" href="#">elektronik</a>, <a rel="tag" href="#">televisi</a>, <a rel="tag" href="#">tv</a>, <a rel="tag" href="#">sharp</a>, <a rel="tag" href="#">led</a>.</span>
								</div> -->

							</div>


						</div>
					</div>

					<div class="row mt-4">
						<div class="col">

							<div class="accordion" id="accordion10">
								<!-- <div class="card card-default">
									<div class="card-header">
										<h4 class="card-title m-0">
											<a class="accordion-toggle text-color-dark font-weight-bold" data-toggle="collapse" data-parent="#accordion10" href="#collapse10Three">
												<i class="fas fa-bars text-color-primary"></i> Spesifikasi
											</a>
										</h4>
									</div>
									<div id="collapse10Three" class="collapse show">
										<div class="card-body">
											<?php //echo $content_product_specification ?>
										</div>
									</div>
								</div> -->
								<div class="card card-default">
									<div class="card-header">
										<h4 class="card-title m-0">
											<a class="accordion-toggle text-color-dark font-weight-bold" data-toggle="collapse" data-parent="#accordion10" href="#collapse10Two">
												<i class="fas fa-bars text-color-primary"></i> Deskripsi
											</a>
										</h4>
									</div>
									<div id="collapse10Two" class="collapse show">
										<div class="card-body">
											<?php echo $content_promo_description ?>
											<!-- <p class="mb-0">Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sagittis, massa fringilla consequat blandit, mauris ligula porta nisi, non tristique enim sapien vel nisl. Suspendisse vestibulum lobortis dapibus.</p>
											 -->
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>


				</div>

			</div>