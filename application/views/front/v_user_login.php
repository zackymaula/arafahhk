<?php
if ($this->session->userdata('member_id') != '') {
  redirect('user/transaction');
}
?>

			<div role="main" class="main">

				<section class="page-header page-header-modern bg-color-light page-header-sm" style="margin-top: -2rem">
					<div class="container">
						<div class="row">

							<div class="col-md-12 align-self-center order-1">

								<ul class="breadcrumb d-block text-center">
									<li><a href="<?php echo base_url(); ?>main">Beranda</a></li>
									<li class="active">Login Member</li>
								</ul>
							</div>
						</div>
					</div>
				</section>

				<div class="container" style="margin-top: -6rem">

					<div class="row">
						<div class="col">

							<div class="featured-boxes">
								<div class="row">
									
									<div class="col-md-12">
										<div class="featured-box featured-box-primary text-left mt-5">
											<div class="box-content">
												<h4 class="color-primary font-weight-semibold text-4 text-uppercase mb-3">Login Member</h4>
												<form action="<?php echo base_url(); ?>login/proses_login" id="frmSignIn" method="post" class="needs-validation">
													<div class="form-row">
														<div class="form-group col">
															<label class="font-weight-bold text-dark text-2">No. Member</label>
															<input type="text" name="post_usernm"  class="form-control form-control-lg" required>
														</div>
													</div>
													<div class="form-row">
														<div class="form-group col">
															<label class="font-weight-bold text-dark text-2">Password</label>
															<input type="password" name="post_passwd" class="form-control form-control-lg" required>
														</div>
													</div>
													<div class="form-row">
														<div class="form-group col-lg-6">
														</div>
														<div class="form-group col-lg-6">
															<input type="submit" value="Login" class="btn btn-primary btn-modern float-right" data-loading-text="Loading...">
														</div>
													</div>
												</form>
											</div>
										</div>
									</div>
									
									<!-- <div class="col-md-12">
										<div class="card border-0 border-radius-10 bg-color-primary">
											<div class="card-body">
												<h4 class="card-title mb-1 text-4 font-weight-bold text-light">Belum punya akun member Arafah?</h4>
												<a href="<?php //echo base_url(); ?>user/regist" class="btn btn-light btn-modern">Daftar Member</a>
											</div>
										</div>
									</div> -->

								</div>

							</div>
						</div>
					</div>

				</div>

			</div>