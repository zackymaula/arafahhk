			<div role="main" class="main shop">

				<section class="page-header page-header-modern bg-color-light page-header-sm" style="margin-top: -2rem">
					<div class="container">
						<div class="row">

							<div class="col-md-12 align-self-center order-1">

								<ul class="breadcrumb d-block text-center">
									<li><a href="<?php echo base_url(); ?>main">Beranda</a></li>
									<li class="active">Bahan Bangunan</li>
								</ul>
							</div>
						</div>
					</div>
				</section>

				<div class="container" style="margin-top: -2rem">

					<div class="row">
						<div class="col-12">
							<div class="masonry-loader masonry-loader-showing">
								
								<div class="row products product-thumb-info-list" data-plugin-masonry data-plugin-options="{'layoutMode': 'fitRows'}">
								
									<div class="col-lg-12">
										<img class="img-fluid rounded" src="<?php echo base_url(); ?>assets/images/products/7.1-product-bahan-bangunan.jpg" alt="Project Image">
									</div>

									<div class="col-lg-6">
										<img class="img-fluid rounded" src="<?php echo base_url(); ?>assets/images/products/7.2-product-bahan-bangunan.jpg" alt="Project Image">
									</div>

									<div class="col-lg-6">
										<img class="img-fluid rounded" src="<?php echo base_url(); ?>assets/images/products/7.3-product-bahan-bangunan.jpg" alt="Project Image">
									</div>

								</div>

							</div>
						</div>
					</div>

				</div>

			</div>
