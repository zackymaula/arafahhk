<?php
$this->db->select('*');
$this->db->from('u_contact_official');
$query = $this->db->get();
$data = $query->row();
$country_name = $data->country_name;
$country_code = $data->country_code;
?>

<!DOCTYPE html>
<html>
	<head>

		<!-- Basic -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">	

		<title>Arafah Electronics & Furniture</title>

		<meta name="keywords" content="Arafah Electronics & Furniture" />
		<meta name="description" content="Arafah Electronics & Furniture">
		<meta name="author" content="arafah.<?php echo $country_code ?>">
		<meta property="og:title" content="Arafah Electronics & Furniture" />
		<meta property="og:url" content="https://www.arafah.<?php echo $country_code ?>/" />
		<meta property="og:description" content="Arafah Electronics & Furniture Official Website">
		<meta property="og:image" content="<?php echo base_url(); ?>assets/images/icon-arafahelectronics.png">

		<!-- Favicon -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/icon-arafahelectronics.png" type="image/x-icon" />
		<link rel="apple-touch-icon" href="<?php echo base_url(); ?>assets/images/icon-arafahelectronics.png">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-frontend/vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-frontend/vendor/fontawesome-free/css/all.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-frontend/vendor/animate/animate.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-frontend/vendor/simple-line-icons/css/simple-line-icons.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-frontend/vendor/owl.carousel/assets/owl.carousel.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-frontend/vendor/owl.carousel/assets/owl.theme.default.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-frontend/vendor/magnific-popup/magnific-popup.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-frontend/vendor/bootstrap-star-rating/css/star-rating.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-frontend/vendor/bootstrap-star-rating/themes/krajee-fas/theme.min.css">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-frontend/css/theme.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-frontend/css/theme-elements.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-frontend/css/theme-blog.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-frontend/css/theme-shop.css">
		
		<!-- Skin CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-frontend/css/skins/default.css"> 

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-frontend/css/custom.css">

		<!-- Head Libs -->
		<script src="<?php echo base_url(); ?>assets/template-frontend/vendor/modernizr/modernizr.min.js"></script>

		<!-- SIGNATURE -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/signature/css/jquery.signaturepad.css">

	</head>

	<style>
	/*OLD
	.placeButton{margin:0;
		padding:0;
		text-align:center;
		position:fixed;
		bottom:50px;
		right:20px;
		z-index: 10000;}
	.designButton{
  		width:75px; 
  		height:75px; 
  		border-radius: 50%; 
  		box-shadow: 0px 0px 20px grey;
	}*/
	/*WIDGET WHATSAPP*/
	.placeButton{margin:0;
		padding:0;
		text-align:center;
		position:fixed;
		bottom:20px;
		right:20px;
		z-index: 10000;}
	.designButtonProfile{
  		width:50px; 
  		height:50px; 
  		border-radius: 50%; 
	}
	.designButtonBackground{font-size:17px;
		color:#f0e2dd;
		background:#38bd4c;
		padding:5px;
		margin:0;
		overflow:hidden;
		border-radius:35px;
		text-decoration:none;
  		box-shadow: 0px 0px 20px grey;
	}

	/*SIGNATURE*/
	/*#btnSaveSign {
		color: #fff;
		background: #f99a0b;
		padding: 5px;
		border: none;
		border-radius: 5px;
		font-size: 20px;
		margin-top: 10px;
	}*/
	#signArea{
		width:304px;
	}

	</style>
	
	<body>

		<div class="body">

			<header id="header" class="header-effect-shrink" data-plugin-options="{'stickyEnabled': true, 'stickyEffect': 'shrink', 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyChangeLogo': true, 'stickyStartAt': 30, 'stickyHeaderContainerHeight': 70}">
				<div class="header-body border-top-0">
					<div class="header-container container-fluid px-lg-4">
						<div class="header-row">
							<div class="header-column flex-grow-0"> <!-- header-column-border-right = garis -->
								<div class="header-row pr-4">
									<div class="header-logo">
										<a href="<?php echo base_url(); ?>main">
											<img alt="Arafah" height="50" data-sticky-height="30" src="<?php echo base_url(); ?>assets/images/logo-arafahelectronics.png">
										</a>
									</div>
								</div>
							</div>
							<div class="header-column">
								<div class="header-row">
									<div class="header-nav header-nav-links justify-content-center">
										<div class="header-nav-main header-nav-main-square header-nav-main-effect-2 header-nav-main-sub-effect-1">
											<nav class="collapse header-mobile-border-top">
												<ul class="nav nav-pills" id="mainNav">

													<?php foreach ($data_categories as $categories) { 
														if ($categories['id_category_sub']=="" && $categories['id_categories']=='1') { ?>
																
																<li class="dropdown">
																	<a class="dropdown-item" href="<?php echo base_url(); ?>paket">
																		<?php echo $categories['name']; ?> <span class="tip tip-dark">hot</span>
																	</a>
																</li>

														<?php
														} else if ($categories['id_category_sub']=="" && $categories['id_categories']=='7') { ?>
																
																<li class="dropdown">
																	<a class="dropdown-item" href="<?php echo base_url(); ?>bangunan">
																		<?php echo $categories['name']; ?>
																	</a>
																</li>

														<?php
														} else if ($categories['id_category_sub']=="") { ?>

															<li class="dropdown">
																<a class="dropdown-item" href="<?php echo base_url().'category/view/'.$categories['id_categories']; ?>">
																	<?php echo $categories['name']; ?>
																</a>
															</li>

														<?php 
														} else {?>

															<li class="dropdown">
														
																<a class="dropdown-item dropdown-toggle" href="<?php echo base_url().'category/view/'.$categories['id_categories']; ?>">
																	<?php echo $categories['name']; ?>
																</a>
																
																<ul class="dropdown-menu">
																	<?php
																	foreach ($data_category_sub as $category_sub) {
																		if ($categories['id_categories'] == $category_sub['id_categories']) {
																	?>
																	<li>
																		<a class="dropdown-item" href="<?php echo base_url(); ?>category_sub/view/<?php echo $category_sub['id_category_sub'] ?>">
																			<?php echo $category_sub['name'] ?>
																		</a>
																	</li>
																	<?php } } ?>

																</ul>
															</li>

													<?php } 
													}; ?>

													<?php if ($this->session->userdata('member_id') == '') { ?>
													<!-- LOGIN -->
													<!-- <li class="dropdown">
														<a class="dropdown-item" href="<?php //echo base_url() ?>login">
															Login
														</a>
													</li> -->
													<?php } else { ?>

													<!-- LOGIN -->
													<li class="dropdown">
														<a class="dropdown-item" href="<?php echo base_url() ?>user/transaction">
															<?php echo $this->session->userdata('member_name') ?> <span class="tip tip-dark">Transaksi Saya</span>
														</a>
													</li>

													<!-- LOGIN -->
													<li class="dropdown">
														<a class="dropdown-item" href="<?php echo base_url() ?>login/proses_logout">
															Logout
														</a>
													</li>
													<?php } ?>

												</ul>
											</nav>
										</div>
									</div>
								</div>
							</div>
							<div class="header-column flex-grow-0 justify-content-center"> <!-- header-column-border-left = garis -->
								<div class="header-row pl-4 justify-content-end">
									<!-- <ul class="header-social-icons social-icons d-none d-sm-block social-icons-clean m-0">
										<li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
										<li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
										<li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li>
									</ul> -->
									<button class="btn header-btn-collapse-nav ml-0 ml-sm-3" data-toggle="collapse" data-target=".header-nav-main nav">
										<i class="fas fa-bars"></i>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>
