
			<div role="main" class="main shop">

				<section class="page-header page-header-modern bg-color-light page-header-sm" style="margin-top: -2rem">
					<div class="container">
						<div class="row">

							<div class="col-md-12 align-self-center order-1">

								<ul class="breadcrumb d-block text-center">
									<li><a href="<?php echo base_url(); ?>main">Beranda</a></li>
									<li class="active">Syarat & Ketentuan</li>
								</ul>
							</div>
						</div>
					</div>
				</section>

				<div class="container" style="margin-top: -3rem">

					<div class="row">
						<div class="col-lg-12">

							<!-- <div class="card border-0 border-radius-1 bg-color-primary">
								<div class="card-body">
									<h4 class="card-title mb-1 text-4 font-weight-bold text-light">No. Faktur : <?php //echo $no_order ?></h4>
									<p class="card-text text-light">Status : 
										<?php //if ($id_order_status == '1') {
											//echo $os_name;
										/*} else if ($id_order_status == '2' || $id_order_status == '3') {
											echo 'Sedang diproses oleh penjual';
										} else if ($id_order_status == '4' || $id_order_status == '5' || $id_order_status == '7') {
											echo 'Proses pembayaran';
										} else if ($id_order_status == '6') {
											echo 'Transaksi BERHASIL. Barang dalam proses pengiriman';
										} else if ($id_order_status == '8') {
											echo $os_name;
										}*/ ?>
									</p>
								</div>
							</div> 
							<br>-->
							<div class="accordion accordion-modern" id="accordion">
								
								<div class="card card-default">
									<div class="card-header">
										<h4 class="card-title m-0">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse1">
												PERJANJIAN SEWA-BELI
											</a>
										</h4>
									</div>
									<div id="collapse1" class="collapse show">
										<div class="card-body">
											
											<p class="text-justify">

											<strong>1.	PIHAK PERTAMA</strong> dan <strong>PIHAK KEDUA</strong> telah sepakat mengikat diri dalam perjanjian sewa-beli dalam hal ini <strong>ARAFAH JAYA LTD</strong> dengan Merk Dagang <strong>ARAFAH SEWA & BELI</strong> yang selanjutnya disebut <strong>PIHAK PERTAMA</strong>. Sebagai Penjual Sewa-Beli dengan <strong>PIHAK KEDUA</strong> yang selanjutnya disebut sebagai Pembeli Sewa-Beli sesuai data identitas yang tercantum dalam pengajuan formulir aplikasi sewa-beli ini.<br><br>
											
											<strong>2.	PEMILIKAN BARANG</strong><br>
											a.	Serah Terima Barang<br>
											PIHAK KEDUA (Pembeli Sewa-Beli) dan / atau pihak lain yang ditunjuk oleh pihak kedua yang tercantum dalam formulir ini telah menerima barang yang di SEWA-BELI sesuai data, merk dan type barang yang tercatat di formulir sewa-beli ini. Selanjutnya PIHAK KEDUA dan pihak yang ditunjuk dianggap telah memeriksa dan menerima barang yang dikirim oleh PIHAK PERTAMA.<br>
											b.	Hak Milik Barang yang di Sewa-Beli akan beralih kepada PIHAK KEDUA setelah PIHAK KEDUA melunasi keseluruhan angsurannya kepada PIHAK PERTAMA.<br>
											c.	Selama masih dalam angsuran, kepemilikan barang tetap berada pada PIHAK PERTAMA, maka PIHAK KEDUA bertanggung jawab penuh atas :<br>
											-	Pemeliharaan terhadap keutuhan & kondisi barang<br>
											-	Kerusakan atau kehilangan barang<br>
											-	Bila terjadi kerusakan dan atau kehilangan terhadap barang yang di Sewa-Beli.<br>
											Pihak Kedua tetap berkewajiban membayar lunas seluruh sisa angsuran atau sisa harga Sewa-Beli kepada Pihak Pertama.<br>
											d.	Selama masih dalam masa angsuran, PIHAK KEDUA dilarang keras untuk menjual, menggadaikan, memindah-alamatkan dan / atau perbuatan melanggar hokum lainnya yang bertujuan untuk memindah-tangankan barang, sebab hal mana dapat dikenakan sanksi Undang-Undang Hukum Pidana tentang PENGGELAPAN.<br><br>

											<strong>3.	PEMBAYARAN ANGSURAN</strong><br>
											a.	Pembayaran Uang Muka atau Angsuran Pertama sebagaimana dalam perjanjian Sewa-Beli ini, harus dilakukan PIHAK KEDUA pada saat pengajuan Sewa-Beli barang dari PIHAK PERTAMA dan sisa sewa yang terhutang harus dibayarkan pada setiap bulan oleh PIHAK KEDUA selambat-lambatnya pada yang Jatuh Tempo yang tercantum dalam informasi taighan yang diberikan pada PIHAK KEDUA.<br>
											b.	Setiap pembayaran yang dilakukan oleh PIHAK KEDUA baik untuk uang muka/DP, Angsuran kedua dan seterusnya dianggap sah bila dapat menunjukkan bukti/struk pembayaran dari PIHAK PERTAMA atau pihak yang ditunjuk. <br><br>

											<strong>4.	DENDA DAN BIAYA PENAGIHAN</strong><br>
											a.	PIHAK PERTAMA akan menghubungi PIHAK KEDUA melalui telepon apabila terjadi keterlambatan pembayaran Angsuran Sewa-Beli dari tanggal jatuh tempo yang telah ditentukan.<br>
											b.	Atas setiap keterlambatan pembayaran sewa tersebut PIHAK KEDUA dikenakan penalti/Denda sebesar <?php echo $official_country_denda.' '.$official_country_kurs ?> per bulan.<br><br>

											<strong>5.	PEMBATALAN PERJANJIAN</strong><br>
											a.	Dengan tidak dibayarnya angsuran sewa yang disebut dalam Perjanjuan Sewa-Beli ini, maka cukup dengan lewat jatuh tempo pembayaran saja, maka PIHAK KEDUA telah memenuhi unsur untuk disyaratkan dalam keadaan LALAI atau WANPRESTASI.<br>
											b.	Tanpa melalui proses Pengadilan Negeri yang mana kedua belah pihak telah sepakat mengabaikan ketentuan pada Undang Undang, bila dalam keadaan Lalai atau Wanprestasi maka perjanjuan Sewa-Beli ini dapat mengakibatkan BATAL.<br>
											c.	Bila Perjanjian Sewa-Beli ini dinyatakan BATAL oleh PIHAK PERTAMA karena PIHAK KEDUA Lalai atau melakukan Wanprestasi, maka tanpa melalui proses pengadilan negeri, PIHAK KEDUA memberikan keasa dan Hak Subtitusi kepada PIHAK PERTAMA untuk memenuhi bangunan dan atau tempat tinggal untuk mengambil kembali Barang Sewa-Beli atau Barang Lain yang setara nilainya yang berada ditangan PIHAK KEDUA dan atau Penerima Barang, dimana dengan sendirinya uang yang telah dibayarkan dinyatakan hangun dan dianggap sebagai SEWA PAKAI BARANG.<br><br>

											<strong>6.	KETENTUAN-KETENTUAN LAIN</strong><br>
											a.	Keterlambatan PIHAK KEDUA dalam melaksanakan hak-haknya sebagaimana dalam perjanjian Sewa-Beli ini tidak dapat dianggap sebagai pencabutan terhadap hak-hak tersebut. Demikian juga setiap pelaksanaan sebagian hak-hak dalam perjanjian ini tidak dapat mengurangi hak-hak PIHAK PERTAMA untuk melaksanakan setiap hak-hak lainnya.<br>
											b.	Apabila terhadap penanda-tanganan dalam Perjanjian Sewa Beli ini lebih dari 1 (satu) orang dan atau terbentuk Badan Hukum maka setiap penandatanganan tersebut bertanggung jawab secara pribadi maupun bersama-sama kepada PIHAK PERTAMA untuk melaksanakan ketentuan-ketentuan dan syarat-syarat dalam Perjanjian ini.<br>
											c.	Untuk melancarkan segala proses kebutuhan yang berhubungan dengan kelayakan Sewa-Beli, maka PIHAK KEDUA memberi kuasa kepada PIHAK PERTAMA untuk memeriksa dan menggali berbagai informasi dari PIHAK KEDUA.<br>
											d.	Dengan ditanda tangani Formulir Pengajuan Sewa Beli oleh PIHAK KEDUA maka PIHAK KEDUA dianggap telah menyetujui terhadap Nama Barang, Merk, Tipe, Warna, Unit Barang, Uang Muka Sewa Angsuran per bulan, Nama Angsuran dan Nilai Total Sewa Beli serta Biaya Administrasi Pengajuan Sewa Beli yang tak terpisahkan dengan Perjanjian Sewa Beli ini.<br>
											e.	Dengan ditanda tanganinya baik formulir pengajuan sewa beli maupun Perjanjian Sewa Beli oleh PIHAK KEDUA, maka PIHAK KEDUA telah memahami dan mengerti secara jelas & rinci serta menyetujui semua bentuk syarat dan ketentuan-ketentuannya.
										
											Saya telah membaca, mengetahui dan memahami dengan benar-benar segala hal Surat Perjanjian Sewa Beli dan Formulir Perjanjian Sewa Beli ini, dan Penanda-tanganan ini saya lakukan dengan keadaan sadar dan sehat walafiat tanpa tekanan dan paksaan dari pihak lain.
											</p>
										</div>
									</div>
								</div>
								
							</div>

						</div>

					</div>

				</div>

			</div>