<?php
$this->db->select('*');
$this->db->from('u_contact_official');
$query = $this->db->get();
$data = $query->row();
$country_name = $data->country_name;
$country_code = $data->country_code;
?>

			<div class="clearfix"></div>
			<footer>
				<div class="container-fluid">
					<p class="copyright">&copy; 2019 <a href="<?php echo base_url(); ?>main" target="_blank">Arafah Electronics & Furniture</a>. All Rights Reserved.</p>
				</div>
			</footer>
		</div>
		<!-- END WRAPPER -->

		<!-- Javascript -->
		<script src="<?php echo base_url(); ?>assets/template-backend/assets/vendor/jquery/jquery.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-backend/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-backend/assets/vendor/pace/pace.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-backend/assets/vendor/datatables/js-main/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-backend/assets/vendor/datatables/js-bootstrap/dataTables.bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-backend/assets/vendor/datatables-colreorder/dataTables.colReorder.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-backend/assets/vendor/datatables-tabletools/js/dataTables.tableTools.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-backend/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-backend/assets/vendor/parsleyjs/js/parsley.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-backend/assets/vendor/select2/js/select2.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-backend/assets/vendor/summernote/summernote.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-backend/assets/scripts/klorofilpro-common.js"></script>
		<script src="<?php echo base_url(); ?>assets/template-other/jquery/jquery-chained.min.js"></script>
		
		<!-- CHART -->
		<script src="<?php echo base_url(); ?>assets/chart/code/highcharts.js"></script>
		<script src="<?php echo base_url(); ?>assets/chart/code/modules/exporting.js"></script>
		<script src="<?php echo base_url(); ?>assets/chart/code/modules/export-data.js"></script>
		<script src="<?php echo base_url(); ?>assets/chart/code/modules/data.js"></script>
		<script src="<?php echo base_url(); ?>assets/chart/code/modules/drilldown.js"></script>

		<script type="text/javascript">

		//DATA TABLE
		$(function()
		{
			// datatable column with reorder extension
			$('#datatable-column-reorder').dataTable(
			{
				pagingType: "full_numbers",
				sDom: "RC" +
					"t" +
					"<'row'<'col-sm-6'i><'col-sm-6'p>>",
				colReorder: true,
			});
			// datatable with column filter enabled
			var dtTable = $('#datatable-column-filter').DataTable(
			{ // use DataTable, not dataTable
				sDom: // redefine sDom without lengthChange and default search box
					"t" +
					"<'row'<'col-sm-6'i><'col-sm-6'p>>"
			});
			$('#datatable-column-filter thead').append('<tr class="row-filter"><th></th><th></th><th></th><th></th><th></th></tr>');
			$('#datatable-column-filter thead .row-filter th').each(function()
			{
				$(this).html('<input type="text" class="form-control input-sm" placeholder="Search...">');
			});
			$('#datatable-column-filter .row-filter input').on('keyup change', function()
			{
				dtTable
					.column($(this).parent().index() + ':visible')
					.search(this.value)
					.draw();
			});
			// datatable with paging options and live search
			$('#featured-datatable').dataTable(
			{
				sDom: "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
			});
			// datatable with export feature
			var exportTable = $('#datatable-data-export').DataTable(
			{
				sDom: "T<'clearfix'>" +
					"<'row'<'col-sm-6'l><'col-sm-6'f>r>" +
					"t" +
					"<'row'<'col-sm-6'i><'col-sm-6'p>>",
				"tableTools":
				{
					"sSwfPath": "<?php echo base_url(); ?>assets/template-backend/assets/vendor/datatables-tabletools/swf/copy_csv_xls_pdf.swf"
				}
			});
			// datatable with scrolling
			$('#datatable-basic-scrolling').dataTable(
			{
				scrollY: "300px",
				scrollCollapse: true,
				paging: false
			});
		});

		//FORM
		$(function()
		{
			// validation needs name of the element
			$('#food').multiselect();
			// initialize after multiselect
			$('#basic-form').parsley();
		});

		//SELECT2
		$(function()
		{
			$('.select-basic').select2();
			$('.select-multiple-basic').select2();
			$('#select-placeholder-single').select2(
			{
				placeholder: 'Select a state',
				allowClear: true
			});
			$('#select-placeholder-multiple').select2(
			{
				placeholder: 'Select a state'
			});
			$('#select-tag').select2(
			{
				tags: true
			});
			$('#select-tag-token').select2(
			{
				tags: true,
				tokenSeparators: [',', ' ']
			});
		});

		//TEXT EDITOR
		$(function()
		{
			// summernote editor
			/*$('.summernote_specification').summernote(
			{
				height: 300,
				focus: false,
				onpaste: function()
				{
					alert('You have pasted something to the editor');
				}
			});*/
			// summernote editor
			$('.summernote_description').summernote(
			{
				height: 300,
				focus: false,
				onpaste: function()
				{
					alert('You have pasted something to the editor');
				}
			});
		});

		//DROPDOWN BREAKDOWN
		$(document).ready(function() {
            $("#sub_category").chained("#category");
        });

		//FORM UPLOAD FILE (SLIDE | PAKET)
		var uploadField200 = document.getElementById("upload-pictures-300");
		uploadField200.onchange = function() {
		    if(this.files[0].size > 307200){ //1024 * 300
		       alert("File terlalu besar! (Maks 300 KB)");
		       this.value = "";
		    } else {
		    	//PREVIEW IMAGE
		    	var reader200 = new FileReader();

			    reader200.onload = function (e) {
			        // get loaded data and render thumbnail.
			        document.getElementById("preview-image").src = e.target.result;
			    };

			    // read the image file as a data URL.
			    reader200.readAsDataURL(this.files[0]);
		    };
		};

		</script>

		<script type="text/javascript">

		//FORM UPLOAD FILE (PRODUCT | STAFF)
		var uploadField100 = document.getElementById("upload-pictures-150");
		uploadField100.onchange = function() {
		    if(this.files[0].size > 153600){ //1024 * 150
		       alert("File terlalu besar! (Maks 150 KB)");
		       this.value = "";
		    } else {
		    	//PREVIEW IMAGE
		    	var reader100 = new FileReader();

			    reader100.onload = function (e) {
			        // get loaded data and render thumbnail.
			        document.getElementById("preview-image").src = e.target.result;
			    };

			    // read the image file as a data URL.
			    reader100.readAsDataURL(this.files[0]);
		    };
		};
		</script>

		<script type="text/javascript">

		//STATISTIC WEB
		Highcharts.chart('statistik-web', {
		    chart: {
		        type: 'line'
		    },
		    title: {
		        text: 'Statistik Viewer Web Per Bulan Tahun 2019'
		    },
		    subtitle: {
		        text: 'Source: arafah.<?php echo $country_code ?>'
		    },
		    xAxis: {
		        categories: <?php echo json_encode($data_statistic_web_view_bulan) ?>
		    },
		    yAxis: {
		        title: {
		            text: 'Viewer'
		        }
		    },
		    plotOptions: {
		        line: {
		            dataLabels: {
		                enabled: true
		            },
		            enableMouseTracking: false
		        }
		    },
		    series: [{
		        name: 'arafah.<?php echo $country_code ?>',
		        data: <?php echo json_encode($data_statistic_web_view) ?>
		    }]
		});

		//STATISTIC PRODUCT
		Highcharts.chart('statistik-produk', {
		    chart: {
		        type: 'bar'
		    },
		    title: {
		        text: 'Statistik Viewer Produk Terbanyak'
		    },
		    subtitle: {
		        text: 'Source: arafah.<?php echo $country_code ?>'
		    },
		    xAxis: {
		        type: 'category'
		    },
		    yAxis: {
		        title: {
		            text: 'Total Viewer'
		        }

		    },
		    legend: {
		        enabled: false
		    },
		    plotOptions: {
		        series: {
		            borderWidth: 0,
		            dataLabels: {
		                enabled: true,
		                format: '{point.y}'
		            }
		        }
		    },

		    tooltip: {
		        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
		        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total view<br/>'
		    },

		    series: [
		        {
		            name: "Product",
		            colorByPoint: true,
		            data: <?php echo json_encode($data_statistic_product_view) ?>
		        }
		    ]
		});

		//STATISTIC SLIDE
		Highcharts.chart('statistik-slide', {
		    chart: {
		        type: 'bar'
		    },
		    title: {
		        text: 'Statistik Viewer Slide Terbanyak'
		    },
		    subtitle: {
		        text: 'Source: arafah.<?php echo $country_code ?>'
		    },
		    xAxis: {
		        type: 'category'
		    },
		    yAxis: {
		        title: {
		            text: 'Total Viewer'
		        }

		    },
		    legend: {
		        enabled: false
		    },
		    plotOptions: {
		        series: {
		            borderWidth: 0,
		            dataLabels: {
		                enabled: true,
		                format: '{point.y}'
		            }
		        }
		    },

		    tooltip: {
		        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
		        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total view<br/>'
		    },

		    series: [
		        {
		            name: "Slide",
		            colorByPoint: true,
		            data: <?php echo json_encode($data_statistic_slide_view) ?>
		        }
		    ]
		});

		//STATISTIK LOCATION
		Highcharts.chart('statistik-location', {
		    chart: {
		        plotBackgroundColor: null,
		        plotBorderWidth: null,
		        plotShadow: false,
		        type: 'pie'
		    },
		    title: {
		        text: 'Statistik Viewer Website Lokasi'
		    },
		    tooltip: {
		        pointFormat: '{series.name}: <b>{point.y}</b>'
		    },
		    plotOptions: {
		        pie: {
		            allowPointSelect: true,
		            cursor: 'pointer',
		            dataLabels: {
		                enabled: true,
		                format: '<b>{point.name}</b>: {point.percentage:.1f} %'
		            }
		        }
		    },
		    series: [{
		        name: 'Total',
		        colorByPoint: true,
		        data: <?php echo json_encode($data_statistic_web_view_location) ?>
		    }]
		});

		</script>


	</body>
</html>