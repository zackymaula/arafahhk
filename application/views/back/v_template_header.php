<?php
if ($this->session->userdata('user_id') == '') {
  redirect('sp-admin');
}
?>

<!doctype html>
<html lang="en">
	<head>
		<title>Administrator | Arafah Electronics & Furnituree</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
		<!-- VENDOR CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/vendor/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/vendor/themify-icons/css/themify-icons.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/vendor/pace/themes/orange/pace-theme-minimal.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/vendor/datatables/css-main/jquery.dataTables.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/vendor/datatables/css-bootstrap/dataTables.bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/vendor/datatables-tabletools/css/dataTables.tableTools.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/vendor/parsleyjs/css/parsley.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/vendor/select2/css/select2.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/vendor/summernote/summernote.css">
		<!-- MAIN CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/css/main.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/css/skins/sidebar-nav-darkgray.css" type="text/css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/css/skins/navbar3.css" type="text/css">
		<!-- FOR DEMO PURPOSES ONLY. You should/may remove this in your project -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/css/demo.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/demo-panel/style-switcher.css">
		<!-- ICONS -->
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>assets/template-backend/assets/img/apple-icon.png">
		<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url(); ?>assets/images/icon-arafahelectronics.png">
	</head>
	<body>
		<!-- WRAPPER -->
		<div id="wrapper">
			<!-- NAVBAR -->
			<nav class="navbar navbar-default navbar-fixed-top">
				<div class="brand">
					<a href="<?php echo base_url(); ?>admin/main">
						<img src="<?php echo base_url(); ?>assets/images/logo-arafah-white.png" alt="Arafah Logo" class="img-responsive logo">
					</a>
				</div>
				<div class="container-fluid">
					<div id="tour-fullwidth" class="navbar-btn">
						<button type="button" class="btn-toggle-fullwidth"><i class="ti-arrow-circle-left"></i></button>
					</div>
					<div id="navbar-menu">
						<ul class="nav navbar-nav navbar-right">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<img src="<?php echo base_url(); ?>assets/images/icon-arafahelectronics-white.png" alt="Avatar">
									<span><?php echo $this->session->userdata('user_name') ?></span>
								</a>
								<ul class="dropdown-menu logged-user-menu">
									<li><a href="<?php echo base_url(); ?>admin/login/proses_logout"><i class="ti-power-off"></i> <span>Logout</span></a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</nav>
			<!-- END NAVBAR -->
			<!-- LEFT SIDEBAR -->
			<div id="sidebar-nav" class="sidebar">
				<nav>
					<ul class="nav" id="sidebar-nav-menu">

						<?php if ($this->session->userdata('user_role') == '1') { ?>


						<li class="menu-group">Main</li>
						<li><a href="<?php echo base_url(); ?>admin/main"><i class="ti-dashboard"></i> <span class="title">Dashboard</span></a></li>

						<li class="menu-group">Transaction</li>
						<li><a href="<?php echo base_url(); ?>admin/order"><i class="ti-shopping-cart"></i> <span class="title">Order</span></a></li>

						<li class="menu-group">Contents</li>
						<li><a href="<?php echo base_url(); ?>admin/product"><i class="ti-layers-alt"></i> <span class="title">Product</span></a></li>
						<li><a href="<?php echo base_url(); ?>admin/promo"><i class="ti-announcement"></i> <span class="title">Promo</span></a></li>
						<li><a href="<?php echo base_url(); ?>admin/slide"><i class="ti-bookmark-alt"></i> <span class="title">Slide</span></a></li>
						<li><a href="<?php echo base_url(); ?>admin/paket"><i class="ti-credit-card"></i> <span class="title">Paket Arafah</span></a></li>
						<li><a href="<?php echo base_url(); ?>admin/testimoni"><i class="ti-comments"></i> <span class="title">Testimoni</span></a></li>
						<li class="panel">
							<a href="#" data-toggle="collapse" data-target="#submenuCategories" data-parent="#sidebar-nav-menu" class="collapsed"><i class="ti-layout-grid2"></i> <span class="title">Categories</span><i class="icon-submenu ti-angle-left"></i></a>
							<div id="submenuCategories" class="collapse">
								<ul class="submenu">
									<li><a href="<?php echo base_url(); ?>admin/category">Category</a></li>
									<li><a href="<?php echo base_url(); ?>admin/category/sub">Sub Category</a></li>
								</ul>
							</div>
						</li>

						<li class="menu-group">Others</li>
						<li class="panel">
							<a href="#" data-toggle="collapse" data-target="#submenuUsers" data-parent="#sidebar-nav-menu" class="collapsed"><i class="ti-id-badge"></i> <span class="title">Users</span><i class="icon-submenu ti-angle-left"></i></a>
							<div id="submenuUsers" class="collapse">
								<ul class="submenu">
									<li><a href="<?php echo base_url(); ?>admin/users">Account</a></li>
									<li><a href="<?php echo base_url(); ?>admin/users/staff">Marketing</a></li>
								</ul>
							</div>
						</li>
						<!-- <li><a href="#"><i class="ti-email"></i> <span class="title">Contact Person</span></a></li> -->
						<?php } else if ($this->session->userdata('user_role') == '6') { ?>
						
						<li class="menu-group">Contents</li>
						<li><a href="<?php echo base_url(); ?>admin/product"><i class="ti-layers-alt"></i> <span class="title">Product</span></a></li>
						<li><a href="<?php echo base_url(); ?>admin/promo"><i class="ti-announcement"></i> <span class="title">Promo</span></a></li>
						<li><a href="<?php echo base_url(); ?>admin/slide"><i class="ti-bookmark-alt"></i> <span class="title">Slide</span></a></li>
						<?php } ?>
					</ul>
					<button type="button" class="btn-toggle-minified" title="Toggle Minified Menu"><i class="ti-arrows-horizontal"></i></button>
				</nav>
			</div>
			<!-- END LEFT SIDEBAR -->