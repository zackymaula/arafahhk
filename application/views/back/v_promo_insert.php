
			<!-- MAIN -->
			<div class="main">
				<!-- MAIN CONTENT -->
				<div class="main-content">
					<div class="content-heading clearfix">
						<div class="heading-left">
							<h1 class="page-title">Promo Insert</h1>
							<p class="page-subtitle">Insert a Promo</p>
						</div>
						<ul class="breadcrumb">
							<li><a href="<?php echo base_url(); ?>admin/main"><i class="fa fa-home"></i> Dashboad</a></li>
							 <li><a href="<?php echo base_url(); ?>admin/promo">Promo</a></li>
							<li class="active">Promo Insert</li>
						</ul>
					</div>
					<div class="container-fluid">

						<div class="row">
							<div class="col-md-12">
								<!-- SUBMIT PRODUCTS -->
								<div class="panel">
									<div class="panel-heading">
										<h3 class="panel-title">Submit a Promo</h3>
									</div>
									<div class="panel-body">
										<form id="basic-form" class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>admin/promo/insert_do" enctype="multipart/form-data">

												<div class="form-group">
													<label for="preview-image" class="col-sm-3 control-label"></label>
													<div class="col-sm-9">
														<img src="<?php echo base_url().'assets/images/image-empty.png'; ?>" id="preview-image" style="width: 500px;" class="w3-border w3-padding" alt="Image">
													</div>
												</div>

												<div class="form-group">
													<label for="upload-pictures-300" class="col-sm-3 control-label">Pictures</label>
													<div class="col-md-9">
														<input type="file" id="upload-pictures-300" name="post_file" accept=".jpg,.jpeg,.png" required>
														<p class="help-block">
															<em>Valid file type: .jpg, .jpeg, .png. File size max: 300 KB. File dimension: 500x500 pixel</em>
														</p>
													</div>
												</div>


												<div class="form-group">
													<label for="promo-name" class="col-sm-3 control-label">Name</label>
													<div class="col-sm-9">
														<input type="text" class="form-control" id="promo-name" name="post_name" placeholder="Name" required>
													</div>
												</div>

												<div class="form-group">
													<label for="promo-price" class="col-sm-3 control-label">Price</label>
													<div class="col-sm-9">
														<input type="text" class="form-control" id="promo-price" name="post_price" placeholder="Price" required>
													</div>
												</div>

												<div class="form-group">
													<label for="promo-description" class="col-sm-3 control-label">Description</label>
													<div class="col-sm-9">
														<textarea class="summernote_description" id="promo-description" name="post_description">
														</textarea>
													</div>
												</div>

												<div class="form-group">
													<div class="col-sm-offset-3 col-sm-9">
														<button type="submit" class="btn btn-primary btn-block" onclick="return confirm('Apakah data sudah benar semua?')">Insert Promo</button>
													</div>
												</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END MAIN CONTENT -->
			</div>
			<!-- END MAIN -->
			