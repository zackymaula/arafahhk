<?php
$this->db->select('*');
$this->db->from('u_contact_official');
$query = $this->db->get();
$data = $query->row();
$country_name = $data->country_name;
$country_code = $data->country_code;
?>

			<div class="main">
				<!-- MAIN CONTENT -->
				<div class="main-content">
					<div class="content-heading clearfix">
						<div class="heading-left">
							<h1 class="page-title">Marketing</h1>
							<p class="page-subtitle">List Marketing</p>
						</div>
						<ul class="breadcrumb">
							<li><a href="<?php echo base_url(); ?>admin/main"><i class="fa fa-home"></i> Dashboard</a></li>
							<li><a href="#">Users</a></li>
							<li class="active">Marketing</li>
						</ul>
					</div>
					<div class="container-fluid">
						<!-- FEATURED DATATABLE -->
						<p class="demo-button">
							<a href="<?php echo base_url(); ?>admin/users/staff_insert" type="button" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="right" title="Insert"><i class="fa fa-plus-square"></i>
								<span class="sr-only">Insert</span>
							</a>
						</p>
						<div class="table-responsive">
							<table id="featured-datatable" class="table table-striped table-hover">
								<thead>
									<tr>
										<th>No</th>
										<th>Photo</th>
										<th>Name</th>
										<th>URL</th>
										<th>Whatsapp</th>
										<th>Option</th>
									</tr>
								</thead>
								<tbody>
									<?php $no = 1;
									foreach ($data_staff as $data_staff) { ?>
									<tr>
										<td><?php echo $no ?></td>
										<td><img src="<?php echo base_url().$data_staff['photo_file_path'].$data_staff['photo_file_name']; ?>" style="width: 70px;" class="w3-border w3-padding" alt="Image"></td>
										<td><?php echo $data_staff['name']; ?></td>
										<td>http://arafah.<?php echo $country_code ?>/me/<?php echo $data_staff['url']; ?></td>
										<td>+<?php echo $data_staff['contact_person']; ?></td>
										<td>
											<div class="btn-group">
												<a href="#" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="Detail">
													<span class="sr-only">Detail</span><i class="fa fa-info-circle"></i></a>
												<a href="<?php echo base_url(); ?>admin/users/staff_update/<?php echo $data_staff['id_staff'] ?>" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Edit">
													<span class="sr-only">Edit</span><i class="fa fa-pencil"></i></a>
												<a href="<?php echo base_url(); ?>admin/users/staff_delete/<?php echo $data_staff['id_staff'] ?>" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Remove" onclick="return confirm('Anda yakin untuk menghapus data?')">
													<span class="sr-only">Remove</span><i class="fa fa-remove"></i></a>
											</div>
										</td>
									</tr>
									<?php $no++; }; ?>
								</tbody>
							</table>
						</div>
						<!-- END FEATURED DATATABLE -->
					</div>
				</div>
				<!-- END MAIN CONTENT -->
			</div>
			<!-- END MAIN -->