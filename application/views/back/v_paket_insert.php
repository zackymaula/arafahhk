
			<!-- MAIN -->
			<div class="main">
				<!-- MAIN CONTENT -->
				<div class="main-content">
					<div class="content-heading clearfix">
						<div class="heading-left">
							<h1 class="page-title">Paket Insert</h1>
							<p class="page-subtitle">Insert a Paket</p>
						</div>
						<ul class="breadcrumb">
							<li><a href="<?php echo base_url(); ?>admin/main"><i class="fa fa-home"></i> Dashboad</a></li>
							 <li><a href="<?php echo base_url(); ?>admin/paket">Paket</a></li>
							<li class="active">Paket Insert</li>
						</ul>
					</div>
					<div class="container-fluid">

						<div class="row">
							<div class="col-md-12">
								<!-- SUBMIT PRODUCTS -->
								<div class="panel">
									<div class="panel-heading">
										<h3 class="panel-title">Submit a Paket</h3>
									</div>
									<div class="panel-body">
										<form id="basic-form" class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>admin/paket/insert_do" enctype="multipart/form-data">

												<div class="form-group">
													<label for="preview-image" class="col-sm-3 control-label"></label>
													<div class="col-sm-9">
														<img src="<?php echo base_url().'assets/images/image-empty.png'; ?>" id="preview-image" style="width: 500px;" class="w3-border w3-padding" alt="Image">
													</div>
												</div>

												<div class="form-group">
													<label for="upload-pictures-300" class="col-sm-3 control-label">Pictures</label>
													<div class="col-md-9">
														<input type="file" id="upload-pictures-300" name="post_file" accept=".jpg,.jpeg,.png" required>
														<p class="help-block">
															<em>Valid file type: .jpg, .jpeg, .png. File size max: 300 KB.</em>
														</p>
													</div>
												</div>

												<div class="form-group">
													<div class="col-sm-offset-3 col-sm-9">
														<button type="submit" class="btn btn-primary btn-block" onclick="return confirm('Apakah data sudah benar semua?')">Insert Paket</button>
													</div>
												</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END MAIN CONTENT -->
			</div>
			<!-- END MAIN -->
			