			<div class="main">
				<!-- MAIN CONTENT -->
				<div class="main-content">
					<div class="content-heading clearfix">
						<div class="heading-left">
							<h1 class="page-title">Order</h1>
							<p class="page-subtitle">List Order</p>
						</div>
						<ul class="breadcrumb">
							<li><a href="<?php echo base_url(); ?>admin/main"><i class="fa fa-home"></i> Dashboard</a></li>
							<li class="active">Order</li>
						</ul>
					</div>
					<div class="container-fluid">
						<!-- FEATURED DATATABLE -->
						
						<div class="table-responsive">
							<table id="featured-datatable" class="table table-striped table-hover">
								<thead>
									<tr>
										<th>#</th>
										<th>No Faktur</th>
										<th>Nomor ID</th>
										<th>Nama Member</th>
										<th>Proses</th>
										<th>Tanggal</th>
										<th>Pilihan</th>
									</tr>
								</thead>
								<tbody>
									<?php $no = 1;
									foreach ($data_order as $data_order) { ?>
									<tr>
										<td><?php echo $no ?></td>
										<td><?php echo $data_order['no_order']; ?></td>
										<td><?php echo $data_order['no_member']; ?></td>
										<td><?php echo $data_order['m_nama']; ?></td>
										<td><?php echo $data_order['os_name']; ?></td>
										<td><?php echo $data_order['tgl_order']; ?></td>
										<td>
											<div class="btn-group">
												<a href="<?php echo base_url(); ?>admin/order/detail/<?php echo $data_order['id_orders'] ?>" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="Detail">
													<span class="sr-only">Detail</span><i class="fa fa-info-circle"></i></a>
											</div>
										</td>
									</tr>
									<?php $no++; }; ?>
								</tbody>
							</table>
						</div>
						<!-- END FEATURED DATATABLE -->
					</div>
				</div>
				<!-- END MAIN CONTENT -->
			</div>
			<!-- END MAIN -->