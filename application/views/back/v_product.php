			<div class="main">
				<!-- MAIN CONTENT -->
				<div class="main-content">
					<div class="content-heading clearfix">
						<div class="heading-left">
							<h1 class="page-title">Product</h1>
							<p class="page-subtitle">List product.</p>
						</div>
						<ul class="breadcrumb">
							<li><a href="<?php echo base_url(); ?>admin/main"><i class="fa fa-home"></i> Dashboard</a></li>
							<li class="active">Product</li>
						</ul>
					</div>
					<div class="container-fluid">
						<!-- FEATURED DATATABLE -->
						<p class="demo-button">
							<a href="<?php echo base_url(); ?>admin/product/insert" type="button" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="right" title="Insert"><i class="fa fa-plus-square"></i>
								<span class="sr-only">Insert</span>
							</a>
						</p>
						<div class="table-responsive">
							<table id="featured-datatable" class="table table-striped table-hover">
								<thead>
									<tr>
										<th>No</th>
										<th>Category</th>
										<th>Category Sub</th>
										<th>Name</th>
										<th>Price</th>
										<th>Option</th>
									</tr>
								</thead>
								<tbody>
									<?php $no = 1;
									foreach ($data_product as $data_product) { ?>
									<tr>
										<td><?php echo $no ?></td>
										<td><?php echo $data_product['c_name']; ?></td>
										<td><?php echo $data_product['cs_name']; ?></td>
										<td><?php echo $data_product['product_name']; ?></td>
										<td><?php echo $data_product['price_regular']; ?></td>
										<td>
											<div class="btn-group">
												<a href="<?php echo base_url(); ?>admin/product/detail/<?php echo $data_product['id_products'] ?>" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="Detail">
													<span class="sr-only">Detail</span><i class="fa fa-info-circle"></i></a>
												<a href="<?php echo base_url(); ?>admin/product/update/<?php echo $data_product['id_products'] ?>" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Edit">
													<span class="sr-only">Edit</span><i class="fa fa-pencil"></i></a>
												<a href="<?php echo base_url(); ?>admin/product/delete/<?php echo $data_product['id_products'] ?>" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Remove" onclick="return confirm('Anda yakin untuk menghapus data?')">
													<span class="sr-only">Remove</span><i class="fa fa-remove"></i></a>
											</div>
										</td>
									</tr>
									<?php $no++; }; ?>
								</tbody>
							</table>
						</div>
						<!-- END FEATURED DATATABLE -->
					</div>
				</div>
				<!-- END MAIN CONTENT -->
			</div>
			<!-- END MAIN -->