
			<!-- MAIN -->
			<div class="main">
				<!-- MAIN CONTENT -->
				<div class="main-content">
					<div class="content-heading clearfix">
						<div class="heading-left">
							<h1 class="page-title">Product Detail</h1>
							<p class="page-subtitle">Detail a Product</p>
						</div>
						<ul class="breadcrumb">
							<li><a href="<?php echo base_url(); ?>admin/main"><i class="fa fa-home"></i> Dashboad</a></li>
							 <li><a href="<?php echo base_url(); ?>admin/product">Product</a></li>
							<li class="active">Product Detail</li>
						</ul>
					</div>
					<div class="container-fluid">

						<div class="row">
							<div class="col-md-12">
								<!-- SUBMIT PRODUCTS -->
								<div class="panel">
									<div class="panel-heading">
										<h3 class="panel-title">Detail a Product</h3>
									</div>
									<div class="panel-body">

												<input type="hidden" name="post_id_products" value="<?php echo $id_products ?>" />

												<div class="form-group">
													<label for="product-name" class="col-sm-3 control-label"></label>
													<div class="col-sm-9">
													<?php foreach ($data_product_file as $data_product_file) { ?>
														<img src="<?php echo base_url().$data_product_file['file_path'].$data_product_file['file_name']; ?>" style="width: 300px;" class="w3-border w3-padding" alt="Image">
													<?php }; ?>
													</div>
												</div>

												<div class="form-group">
													<label for="product-name" class="col-sm-3 control-label">Category</label>
													<div class="col-sm-9">
														<input type="text" class="form-control" id="category" name="post_category" value="<?php echo $c_name ?>" placeholder="-" disabled>
													</div>
												</div>
												<div class="form-group">
													<label for="product-name" class="col-sm-3 control-label">Sub Category</label>
													<div class="col-sm-9">
														<input type="text" class="form-control" id="sub_categorye" name="post_sub_category" value="<?php echo $cs_name ?>" placeholder="-" disabled>
													</div>
												</div>
												<div class="form-group">
													<label for="product-name" class="col-sm-3 control-label">Name</label>
													<div class="col-sm-9">
														<input type="text" class="form-control" id="product-name" name="post_name" value="<?php echo $product_name ?>" placeholder="-" disabled>
													</div>
												</div>
												<div class="form-group">
													<label for="product-price" class="col-sm-3 control-label">Price</label>
													<div class="col-sm-9">
														<input type="text" class="form-control" id="product-price" name="post_price" value="<?php echo $price_regular ?>" placeholder="-" disabled>
													</div>
												</div>
												<div class="form-group">
													<label for="product-price-discount" class="col-sm-3 control-label">Price Discount</label>
													<div class="col-sm-9">
														<input type="text" class="form-control" id="product-price-discount" name="post_price_discount" value="<?php echo $price_discount ?>" placeholder="-" disabled>
													</div>
												</div>

												<!-- <div class="form-group">
													<label for="product-tag" class="col-sm-3 control-label">Tag</label>
													<div class="col-sm-9">
														<select id="select-tag-token" name="post_tag[]" multiple="multiple" style="width: 100%;" required>

														</select>
														<p class="help-block">
															<em>Fill more than one</em>
														</p>
													</div>
												</div> -->

												<!-- <div class="form-group">
													<label for="product-specification" class="col-sm-3 control-label">Specification</label>
													<div class="col-sm-9">
														<textarea class="summernote_specification" id="product-specification" name="post_specification" required>
														</textarea>
													</div>
												</div> -->
												<div class="form-group">
													<label for="product-description" class="col-sm-3 control-label">Description</label>
													<div class="col-sm-9">
														<textarea class="form-control" placeholder="textarea" rows="4" disabled><?php echo $description ?></textarea>
													</div>
												</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END MAIN CONTENT -->
			</div>
			<!-- END MAIN -->
			