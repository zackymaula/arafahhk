
			<!-- MAIN -->
			<div class="main">
				<!-- MAIN CONTENT -->
				<div class="main-content">
					<div class="container">
						<div class="content-heading clearfix">
							<div class="heading-left">
								<h1 class="page-title">List Order</h1>
							</div>
							<ul class="breadcrumb">
								<li><a href="<?php echo base_url(); ?>marketing/main"><i class="fa fa-home"></i> Dashboad</a></li>
								<li class="active">List Order</li>
								<!-- <li><a href="#">Pages</a></li> -->
								<!-- <li class="active">Blank Page</li> -->
							</ul>
						</div>
						<!-- <div class="container-fluid">
							asdasdas
						</div> -->
						<!-- <div class="row">
							<div class="col-md-12">
								<div class="panel">
									<div class="panel-heading">
										<h3 class="panel-title">-</h3>
									</div>
									<div class="panel-body">
										-
									</div>
								</div>
							</div>
						</div> -->
						<div class="table-responsive">
							<table id="featured-datatable" class="table table-striped table-hover">
								<thead>
									<tr>
										<th>#</th>
										<th>No Faktur</th>
										<th>No Member</th>
										<th>Nama</th>
										<th>Proses</th>
										<th>Tanggal</th>
										<th>Pilihan</th>
									</tr>
								</thead>
								<tbody>
									<?php $no = 1;
									foreach ($data_order as $data_order) { ?>
									<tr>
										<td><?php echo $no ?></td>
										<td><?php echo $data_order['no_order']; ?></td>
										<td><?php echo $data_order['no_member']; ?></td>
										<td><?php echo $data_order['m_nama']; ?></td>
										<td><?php echo $data_order['os_name']; ?></td>
										<td><?php echo $data_order['tgl_order']; ?></td>
										<td>
											<?php if ($data_order['id_order_status']=='1') { ?>
											<div class="row">
												<div class="col-md-4">
													<a href="<?php echo base_url().'marketing/order/detail/'.$data_order['id_orders'] ?>" type="button" class="btn btn-info btn-xs btn-block">Detail</a>
												</div>
												<div class="col-md-4">
													<a href="<?php echo base_url().'marketing/order/copy_link/'.$data_order['no_order_generate'] ?>" type="button" class="btn btn-reddit btn-xs btn-block">Copy link</a>
												</div>
												<div class="col-md-4">
													<a href="<?php echo base_url().'marketing/order/update/'.$data_order['id_orders'] ?>" type="button" class="btn btn-danger btn-xs btn-block">Edit</a>
												</div>
											</div>
											<?php } else if ($data_order['id_order_status']=='2') { ?>
											<div class="row">
												<div class="col-md-4">
													<a href="<?php echo base_url().'marketing/order/detail/'.$data_order['id_orders'] ?>" type="button" class="btn btn-info btn-xs btn-block">Detail</a>
												</div>
												<div class="col-md-4">
													<a href="<?php echo base_url().'marketing/order/confirm_marketing/'.$data_order['id_orders'] ?>" type="button" class="btn btn-success btn-xs btn-block">Setujui</a>
												</div>
												<div class="col-md-4"></div>
											</div>
											<?php } else if ($data_order['id_order_status']=='3') { ?>
											<div class="row">
												<div class="col-md-4">
													<a href="<?php echo base_url().'marketing/order/detail/'.$data_order['id_orders'] ?>" type="button" class="btn btn-info btn-xs btn-block">Detail</a>
												</div>
												<div class="col-md-4"></div>
												<div class="col-md-4"></div>
											</div>
											<?php } else if ($data_order['id_order_status']=='4') { ?>
											<div class="row">
												<div class="col-md-4">
													<a href="<?php echo base_url().'marketing/order/detail/'.$data_order['id_orders'] ?>" type="button" class="btn btn-info btn-xs btn-block">Detail</a>
												</div>
												<div class="col-md-4">
													<!-- <a href="<?php //echo base_url().'marketing/generate/index/'.$data_order['id_orders'] ?>" type="button" class="btn btn-facebook btn-xs btn-block">Generate QRCode</a> -->
												</div>
												<div class="col-md-4"></div>
											</div>
											<?php } else if ($data_order['id_order_status']=='6') { ?>
											<div class="row">
												<div class="col-md-4">
													<a href="<?php echo base_url().'marketing/order/detail/'.$data_order['id_orders'] ?>" type="button" class="btn btn-info btn-xs btn-block">Detail</a>
												</div>
												<div class="col-md-4"></div>
												<div class="col-md-4"></div>
											</div>
											<?php } ?>
											
										</td>
									</tr>
									<?php $no++; }; ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- END MAIN CONTENT -->
			</div>
			<!-- END MAIN -->