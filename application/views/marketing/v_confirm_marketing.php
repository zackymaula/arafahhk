			
			<!-- MAIN -->
			<div class="main">
				<!-- MAIN CONTENT -->
				<div class="main-content">
					<div class="container">
						<div class="content-heading clearfix">
							<div class="heading-left">
								<h1 class="page-title">Persetujuan Marketing</h1>
							</div>
							<ul class="breadcrumb">
								<li><a href="<?php echo base_url(); ?>marketing/main"><i class="fa fa-home"></i> Dashboad</a></li>
								<li><a href="<?php echo base_url(); ?>marketing/order">List Order</a></li>
								<li class="active">Persetujuan Marketing</li>
								<!-- <li><a href="#">Pages</a></li> -->
								<!-- <li class="active">Blank Page</li> -->
							</ul>
						</div>
						<!-- <div class="container-fluid">
							asdasdas
						</div> -->
						<div class="row">
							<div class="col-md-12">
								<div class="panel">
									<div class="panel-heading">
										
									<button class="btn btn-primary btn-block">
										<h4 class="text-left">No. Order : <?php echo $no_order ?></h4>
										<h5 class="text-left">Status : <?php echo $os_name ?></h5>
									</button>
									</div>
									<div class="panel-body">
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<label class="control-label" for="ksm-tanda-tangan"><b>Tanda Tangan</b></label>
													<!-- <textarea class="form-control" id="tanda-tangan" name="tanda-tangan" placeholder="" rows="4"></textarea> --> <!-- required> -->
													<div id="signArea" >
														<div class="sig sigWrapper" style="height:auto;">
															<div class="typed"></div>
															<canvas class="sign-pad" id="sign-pad" width="300" height="100"></canvas>
														</div>
													</div>
													<p class="help-block">
														<form action="<?php echo base_url().'marketing/order/confirm_marketing/'.$id_orders; ?>">
															<em>Jika ada kekeliruan tanda tangan mohon refresh ulang halaman  </em>
														    <button type="submit" class="mb-1 mt-1 mr-1 btn-xs btn btn-success"><i class="fa fa-refresh"></i> Tekan disini untuk Refresh</button>
														</form>
													</p>
												</div>
											</div>
											<div class="col-md-12">
												<form id="basic-form" class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>marketing/order/insert_data_sign_marketing" enctype="multipart/form-data">
													<input type="hidden" name="post_id_order" value="<?php echo $id_orders ?>" />
													<input type="hidden" name="post_id_sign" value="<?php echo $id_order_sign ?>" />
													<button type="submit" id="btnSaveSign" class="btn btn-primary btn-block btn-modern text-uppercase mt-5 mb-5 mb-lg-0" onclick="return confirm('Apakah data sudah benar semua dan ingin mengkonfirmasinya?')">Konfirmasi</button>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>


					</div>
				</div>
				<!-- END MAIN CONTENT -->
			</div>
			<!-- END MAIN -->