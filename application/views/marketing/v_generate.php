<?php
$this->db->select('*');
$this->db->from('u_contact_official');
$query = $this->db->get();
$data = $query->row();
$country_kurs = $data->country_kurs;
?>

			<!-- MAIN -->
			<div class="main">
				<!-- MAIN CONTENT -->
				<div class="main-content">
					<div class="container">
						<div class="content-heading clearfix">
							<div class="heading-left">
								<h1 class="page-title">Generate QRcode TNG</h1>
							</div>
							<ul class="breadcrumb">
								<li><a href="<?php echo base_url(); ?>marketing/main"><i class="fa fa-home"></i> Dashboad</a></li>
								<li><a href="<?php echo base_url(); ?>marketing/order">List Order</a></li>
								<li class="active">Generate QRcode TNG</li>
							</ul>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="panel">
									<div class="panel-heading">
										<h3 class="panel-title">Detail Transaksi</h3>
									</div>
									<div class="panel-body">
										<img src="<?php echo base_url().'assets/images/logo-arafahelectronics.png'; ?>" style="width: 100px; margin:auto; display:block;" class="w3-border w3-padding" alt="Image">
										<br>
										<img src="<?php echo base_url().'assets/qrcode/images/qrc-190910213756.png'; ?>" style="width: 300px; margin:auto; display:block;" class="w3-border w3-padding" alt="Image">
										<div class="profile-info">
											<!-- <h4 class="heading">Informasi</h4> -->
											<ul class="list-unstyled list-justify">
												<li>No Order
													<span><?php echo $no_order ?></span>
												</li>
												<li>No Member
													<span><?php echo $no_member ?></span>
												</li>
												<li>Nama
													<span><?php echo $m_nama ?></span>
												</li>
												<li>Total
													<span><?php echo $grand_total ?></span>
												</li>
												<li>Status Transaksi
													<span><?php echo $status ?></span>
												</li>
											</ul>
										</div>
										<!-- <form id="basic-form" class="form-horizontal" role="form" method="post" action="<?php //echo base_url(); ?>generate/gettransactioninfo" enctype="multipart/form-data">

											<input type="hidden" name="post_order_no" value="<?php //echo $data_order_no ?>" />
											<div class="col-sm-12">
												<button type="submit" class="btn btn-primary btn-block">Refresh Informasi Transaksi</button>
											</div>
										</form> -->

									</div>
							</div>

						</div>
					</div>
				</div>
				<!-- END MAIN CONTENT -->
			</div>
			<!-- END MAIN -->