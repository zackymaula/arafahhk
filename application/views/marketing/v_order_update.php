<?php
$this->db->select('*');
$this->db->from('u_contact_official');
$query = $this->db->get();
$data = $query->row();
$country_name = $data->country_name;
$country_code = $data->country_code;
$country_kurs = $data->country_kurs;
?>


			<!-- MAIN -->
			<div class="main">
				<!-- MAIN CONTENT -->
				<div class="main-content">
					<div class="container">
						<div class="content-heading clearfix">
							<div class="heading-left">
								<h1 class="page-title">Update Order</h1>
							</div>
							<ul class="breadcrumb">
								<li><a href="<?php echo base_url(); ?>marketing/main"><i class="fa fa-home"></i> Dashboad</a></li>
								<li class="active">Update Order</li>
								<!-- <li><a href="#">Pages</a></li> -->
							</ul>
						</div>
						<!-- <div class="container-fluid">
							asdasdas
						</div> -->
						<!-- <div class="row">
							<div class="col-md-12">
								<div class="panel">
									<div class="panel-heading">
										<h3 class="panel-title">-</h3>
									</div>
									<div class="panel-body">
										-
									</div>
								</div>
							</div>
						</div> -->
						<form id="basic-form form" class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>marketing/order/update_do" enctype="multipart/form-data" data-parsley-validate novalidate>
							
							<input type="hidden" name="post_id_orders" value="<?php echo $id_orders ?>" />
							<input type="hidden" name="post_id_members" value="<?php echo $id_members ?>" />
							<input type="hidden" name="post_id_order_receiver" value="<?php echo $id_order_receiver ?>" />
							<input type="hidden" name="post_file_id_card" value="<?php echo $m_file_id_card ?>" />
							<input type="hidden" name="post_file_selfie" value="<?php echo $m_file_selfie ?>" />
							<input type="hidden" name="post_file_lain" value="<?php echo $m_file_lain ?>" />

						<div class="row">
							<div id="demo-wizard" class="wizard">
								<div class="steps-container">
									<ul class="steps">
										<li data-step="1" class="active">
											<span class="badge badge-info">1</span>Data Pribadi
											<span class="chevron"></span>
										</li>
										<li data-step="2">
											<span class="badge">2</span>Data Pekerjaan
											<span class="chevron"></span>
										</li>
										<li data-step="3">
											<span class="badge">3</span>Data Medsos
											<span class="chevron"></span>
										</li>
										<li data-step="4">
											<span class="badge">4</span>Data Penerima Barang
											<span class="chevron"></span>
										</li>
										<li data-step="5">
											<span class="badge">5</span>Data Barang yang Dipesan
											<span class="chevron"></span>
										</li>
										<li data-step="6">
											<span class="badge">6</span>Upload Dokumen
											<span class="chevron"></span>
										</li>
										<li data-step="7" class="last">
											<span class="badge">7</span>Konfirmasi Update
										</li>
									</ul>
								</div>
								<div class="step-content">
								
									<div class="step-pane active" data-step="1">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label" for="pribadi-nama">Nama Lengkap</label>
													<input type="text" id="pribadi-nama" name="pribadi-nama" value="<?php echo $m_nama ?>" placeholder="" class="form-control"> <!-- required> -->
												</div>
												<div class="form-group">
													<label class="control-label" for="pribadi-lahir-tempat">Tempat Lahir</label>
													<input type="text" id="pribadi-lahir-tempat" name="pribadi-lahir-tempat" value="<?php echo $m_lahir_tmpt ?>" placeholder="" class="form-control"> <!-- required> -->
												</div>
												<div class="form-group">
													<label class="control-label" for="pribadi-lahir-tgl">Tanggal Lahir</label>
													<div class="input-group date" data-date-autoclose="true" data-provide="datepicker" data-date-autoclose="true" data-date-format="dd/mm/yyyy">
														<input type="text" id="pribadi-lahir-tgl" name="pribadi-lahir-tgl" value="<?php echo $m_lahir_tgl ?>" class="form-control" readonly="">
														<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label" for="pribadi-status-nikah">Status Nikah</label>
													<br>
													<label class="fancy-radio custom-color-blue">
														<input name="pribadi-status-nikah" value="Sudah Nikah" type="radio" <?php if( $m_status_nikah=='Sudah Nikah'){echo "checked"; } ?> >
														<span><i></i>Sudah Nikah</span>
													</label>
													<label class="fancy-radio custom-color-blue">
														<input name="pribadi-status-nikah" value="Belum Nikah" type="radio" <?php if( $m_status_nikah=='Belum Nikah'){echo "checked"; } ?> >
														<span><i></i>Belum Nikah</span>
													</label>
												</div>
												<div class="form-group">
													<label class="control-label" for="pribadi-alamat">Alamat Lengkap Indonesia</label>
													<textarea class="form-control" id="pribadi-alamat" name="pribadi-alamat" placeholder="" rows="4"><?php echo $m_alamat_indo ?></textarea> <!-- required> -->
												</div>
												<div class="form-group">
													<label class="control-label" for="pribadi-nama-ayah">Nama Ayah</label>
													<input type="text" id="pribadi-nama-ayah" name="pribadi-nama-ayah" value="<?php echo $m_nama_ayah ?>"  placeholder="" class="form-control"> <!-- required> -->
												</div>
												<div class="form-group">
													<label class="control-label" for="pribadi-nama-ibu">Nama Ibu Kandung</label>
													<input type="text" id="pribadi-nama-ibu" name="pribadi-nama-ibu" value="<?php echo $m_nama_ibu ?>"  placeholder="" class="form-control"> <!-- required> -->
												</div>
												<div class="form-group">
													<label class="control-label" for="pribadi-no-hp-indo">Nomor HP Indonesia</label>
													<input type="text" id="pribadi-no-hp-indo" name="pribadi-no-hp-indo" value="<?php echo $m_hp_indo ?>"  placeholder="" class="form-control"> <!-- required> -->
												</div>
												<div class="form-group">
													<label class="control-label" for="pribadi-no-hp-luar">Nomor HP <?php echo $country_name ?></label>
													<input type="text" id="pribadi-no-hp-luar" name="pribadi-no-hp-luar" value="<?php echo $m_hp_luar ?>"  placeholder="" class="form-control"> <!-- required> -->
												</div>
												<div class="form-group">
													<label class="control-label" for="pribadi-status-rumah">Status Rumah</label>
													<br>
													<label class="fancy-radio custom-color-blue">
														<input name="pribadi-status-rumah" value="Sendiri" type="radio" <?php if( $m_status_rumah=='Sendiri'){echo "checked"; } ?>>
														<span><i></i>Sendiri</span>
													</label>
													<label class="fancy-radio custom-color-blue">
														<input name="pribadi-status-rumah" value="Orang Tua" type="radio" <?php if( $m_status_rumah=='Orang Tua'){echo "checked"; } ?>>
														<span><i></i>Orang Tua</span>
													</label>
													<label class="fancy-radio custom-color-blue">
														<input name="pribadi-status-rumah" value="Saudara" type="radio" <?php if( $m_status_rumah=='Saudara'){echo "checked"; } ?>>
														<span><i></i>Saudara</span>
													</label>
													<label class="fancy-radio custom-color-blue">
														<input name="pribadi-status-rumah" value="Kontrak" type="radio" <?php if( $m_status_rumah=='Kontrak'){echo "checked"; } ?>>
														<span><i></i>Kontrak</span>
													</label>
												</div>
											</div>
										</div>
									</div>
								
									<div class="step-pane active" data-step="2">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label" for="pekerjaan">Pekerjaan</label>
													<br>
													<label class="fancy-radio custom-color-blue">
														<input name="pekerjaan" value="Domestic" type="radio" <?php if( $m_pekerjaan=='Domestic'){echo "checked"; } ?> >
														<span><i></i>Domestic</span>
													</label>
													<label class="fancy-radio custom-color-blue">
														<input name="pekerjaan" value="Nurse" type="radio" <?php if( $m_pekerjaan=='Nurse'){echo "checked"; } ?> >
														<span><i></i>Nurse</span>
													</label>
													<label class="fancy-radio custom-color-blue">
														<input name="pekerjaan" value="Pabrik" type="radio" <?php if( $m_pekerjaan=='Pabrik'){echo "checked"; } ?> >
														<span><i></i>Pabrik</span>
													</label>
													<label class="fancy-radio custom-color-blue">
														<input name="pekerjaan" value="Lain-lain" type="radio" <?php if( $m_pekerjaan=='Lain-lain'){echo "checked"; } ?> >
														<span><i></i>Lain-lain</span>
													</label>
												</div>
												<div class="form-group">
													<label class="control-label" for="pekerjaan-no-id-negara">Nomor ID <?php echo $country_name ?></label>
													<input type="text" id="pekerjaan-no-id-negara" name="pekerjaan-no-id-negara"  value="<?php echo $m_pekerjaan_no_id_luar ?>" placeholder="" class="form-control"> <!-- required> -->
												</div>
												<div class="form-group">
													<label class="control-label" for="pekerjaan-masa-berlaku">Masa Berlaku Kontrak</label>
													<input type="text" id="pekerjaan-masa-berlaku" name="pekerjaan-masa-berlaku" value="<?php echo $m_pekerjaan_masa_berlaku ?>" placeholder="" class="form-control"> <!-- required> -->
												</div>
												<div class="form-group">
													<label class="control-label" for="pekerjaan-alamat">Alamat Kerja</label>
													<textarea class="form-control" id="pekerjaan-alamat" name="pekerjaan-alamat" placeholder="" rows="4"><?php echo $m_pekerjaan_alamat ?></textarea> <!-- required> -->
												</div>
												<div class="form-group">
													<label class="control-label" for="pekerjaan-teman-nama">Nama Teman Dekat</label>
													<input type="text" id="pekerjaan-teman-nama" name="pekerjaan-teman-nama" value="<?php echo $m_pekerjaan_teman_nama ?>" placeholder="" class="form-control"> <!-- required> -->
												</div>
												<div class="form-group">
													<label class="control-label" for="pekerjaan-teman-no-hp">Nomor HP Teman Dekat</label>
													<input type="text" id="pekerjaan-teman-no-hp" name="pekerjaan-teman-no-hp" value="<?php echo $m_pekerjaan_teman_hp ?>" placeholder="" class="form-control"> <!-- required> -->
												</div>

											</div>
										</div>
									</div>
								
									<div class="step-pane active" data-step="3">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label" for="medsos-line">LINE ID</label>
													<input type="text" id="medsos-line" name="medsos-line" value="<?php echo $m_medsos_line ?>" placeholder="" class="form-control"> <!-- required> -->
												</div>
												<div class="form-group">
													<label class="control-label" for="medsos-fb">Facebook ID</label>
													<input type="text" id="medsos-fb" name="medsos-fb" value="<?php echo $m_medsos_fb ?>" placeholder="" class="form-control"> <!-- required> -->
												</div>
												<div class="form-group">
													<label class="control-label" for="medsos-ig">Instagram ID</label>
													<input type="text" id="medsos-ig" name="medsos-ig" value="<?php echo $m_medsos_ig ?>" placeholder="" class="form-control"> <!-- required> -->
												</div>
												<div class="form-group">
													<label class="control-label" for="medsos-wa">Whatsapp ID</label>
													<input type="text" id="medsos-wa" name="medsos-wa" value="<?php echo $m_medsos_wa ?>" placeholder="" class="form-control"> <!-- required> -->
												</div>
												
											</div>
										</div>
									</div>
								
									<div class="step-pane active" data-step="4">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label" for="penerima-nama">Nama Lengkap</label>
													<input type="text" id="penerima-nama" name="penerima-nama" value="<?php echo $or_nama ?>" placeholder="" class="form-control"> <!-- required> -->
												</div>
												<div class="form-group">
													<label class="control-label" for="penerima-no-id">Nomor ID</label>
													<input type="text" id="penerima-no-id" name="penerima-no-id" value="<?php echo $or_no_id ?>" placeholder="" class="form-control"> <!-- required> -->
												</div>
												<div class="form-group">
													<label class="control-label" for="penerima-hubungan">Hubungan dengan Pemohon</label>
													<br>
													<label class="fancy-radio custom-color-blue">
														<input name="penerima-hubungan" value="Sendiri" type="radio" <?php if( $or_hubungan=='Sendiri'){echo "checked"; } ?> >
														<span><i></i>Sendiri</span>
													</label>
													<label class="fancy-radio custom-color-blue">
														<input name="penerima-hubungan" value="Orang Tua" type="radio" <?php if( $or_hubungan=='Orang Tua'){echo "checked"; } ?> >
														<span><i></i>Orang Tua</span>
													</label>
													<label class="fancy-radio custom-color-blue">
														<input name="penerima-hubungan" value="Anak" type="radio" <?php if( $or_hubungan=='Anak'){echo "checked"; } ?> >
														<span><i></i>Anak</span>
													</label>
													<label class="fancy-radio custom-color-blue">
														<input name="penerima-hubungan" value="Kakek-Nenek" type="radio" <?php if( $or_hubungan=='Kakek-Nenek'){echo "checked"; } ?> >
														<span><i></i>Kakek-Nenek</span>
													</label>
												</div>
												<div class="form-group">
													<label class="control-label" for="penerima-alamat-negara">Alamat Lengkap</label>
													<br>
													<label class="fancy-radio custom-color-blue">
														<input name="penerima-alamat-negara" value="Indo" type="radio" <?php if( $or_alamat_negara=='Indo'){echo "checked"; } ?> >
														<span><i></i>Indo</span>
													</label>
													<label class="fancy-radio custom-color-blue">
														<input name="penerima-alamat-negara" value="Taiwan" type="radio" <?php if( $or_alamat_negara=='Taiwan'){echo "checked"; } ?> >
														<span><i></i>Taiwan</span>
													</label>
													<label class="fancy-radio custom-color-blue">
														<input name="penerima-alamat-negara" value="Hongkong" type="radio" <?php if( $or_alamat_negara=='Hongkong'){echo "checked"; } ?> >
														<span><i></i>Hongkong</span>
													</label>
													<label class="fancy-radio custom-color-blue">
														<input name="penerima-alamat-negara" value="Singapore" type="radio" <?php if( $or_alamat_negara=='Singapore'){echo "checked"; } ?> >
														<span><i></i>Singapore</span>
													</label>
													<br>
													<textarea class="form-control" id="penerima-alamat-detail" name="penerima-alamat-detail" placeholder="" rows="4"><?php echo $or_alamat_detail ?></textarea> <!-- required> -->
												</div>
												<div class="form-group">
													<label class="control-label" for="penerima-no-hp">Nomor HP Penerima</label>
													<input type="text" id="penerima-no-hp" name="penerima-no-hp" value="<?php echo $or_hp ?>" placeholder="" class="form-control"> <!-- required> -->
												</div>
												<div class="form-group">
													<label class="control-label" for="penerima-no-hp-keluarga">Nomor HP Keluarga Indonesia</label>
													<input type="text" id="penerima-no-hp-keluarga" name="penerima-no-hp-keluarga" value="<?php echo $or_hp_keluarga ?>" placeholder="" class="form-control"> <!-- required> -->
												</div>
												
											</div>
										</div>
									</div>

									<div class="step-pane active" data-step="5">
										<div class="row">
											<div class="col-md-12">
												<div class="table-responsive">
													<table class="table table-bordered" id="crud_table">
														<thead>
															<tr>
																<th><b>Nama Barang</b></th>
																<th><b>Merk</b></th>
																<th><b>Type</b></th>
																<th><b>Warna</b></th>
																<th><b>Unit</b></th>
																<th><b>Uang Muka</b></th>
																<th><b>Cicilan/bulan</b></th>
																<th><b>Tempo (bulan)</b></th>
																<th><b>#</b></th>
															</tr>
														</thead>
														<tbody>
															<?php $no = 1;
															foreach ($data_list_order_barang as $list_barang) { ?>
															<tr id="row<?php echo $no ?>">
																<td class="item_nama"><input type="text" name='item_nama[]' value="<?php echo $list_barang['oi_nama']; ?>" style="width:200px;" placeholder="-" class="form-control"></td>
																<td class="item_merk"><input type="text" name='item_merk[]' value="<?php echo $list_barang['oi_merk']; ?>" style="width:100px;" placeholder="-" class="form-control"></td>
																<td class="item_type"><input type="text" name='item_type[]' value="<?php echo $list_barang['oi_type']; ?>" style="width:100px;" placeholder="-" class="form-control"></td>
																<td class="item_warna"><input type="text" name='item_warna[]' value="<?php echo $list_barang['oi_warna']; ?>" style="width:100px;" placeholder="-" class="form-control"></td>
																<td class="item_unit"><input type="text" name='item_unit[]' value="<?php echo $list_barang['oi_unit']; ?>" style="width:100px;" placeholder="-" class="form-control"></td>
																<td class="item_uang_muka"><input type="text" name='item_uang_muka[]' value="<?php echo $list_barang['oi_uang_muka']; ?>" style="width:100px;" placeholder="-" class="form-control"></td>
																<td class="item_cicilan"><div class="input-group"><input type="number" name="item_cicilan[]" value="<?php echo $list_barang['oi_cicilan']; ?>" style="width:100px;" min="1" placeholder="0" class="form-control"><span class="input-group-addon"><?php echo $country_kurs ?></span></div></td>
																<td class="item_tempo"><div class="input-group"><input type="number" name="item_tempo[]" value="<?php echo $list_barang['oi_tempo']; ?>" style="width:100px;" min="1" placeholder="0" class="form-control"><span class="input-group-addon">Bulan</span></div></td>
																<td>
																<?php if ($no >= 2) {
																	?>
																	<button type='button' name='remove' data-row='row<?php echo $no ?>' class='btn btn-danger btn-xs remove'>-</button>
																	<?php
																}
																?>
																</td>
															</tr>
															<?php $no++; }; ?>
														</tbody>
													</table>
												</div>
												
											    <div align="right">
											    	<button type="button" name="add" id="add" class="btn btn-success btn-xs">+</button>
											    </div>

												<div class="form-group">
													<label for="qrc-amount" class="col-sm-1 control-label"><b>Biaya</b></label>
													<div class="col-sm-5">
														<div class="input-group">
															<input type="number" id="order-biaya" name="order-biaya" value="<?php echo $biaya_order ?>" min="1" placeholder="0" class="form-control">
															<span class="input-group-addon"><?php echo $country_kurs ?></span>
														</div>
													</div>
												</div>

											</div>
										</div>
									</div>
								
									<div class="step-pane active" data-step="6">
										<div class="row">
											<div class="col-md-6">
												<div class="panel">
													<div class="panel-heading">
														<h3 class="panel-title">Upload ID card</h3>
													</div>
													<div class="panel-body">
														<div class="row">
															<div class="col-xs-6">
																<?php if ($m_file_id_card=='') {
																	?><img src="<?php echo base_url().'assets/images/image-empty.png'; ?>" id="preview-image1" style="width: 100%; margin-left: auto; margin-right: auto; display: block;" class="w3-border w3-padding" alt="Image"><?php
																} else {
																	?><img src="<?php echo base_url().$m_file_id_card; ?>" id="preview-image1" style="width: 100%; margin-left: auto; margin-right: auto; display: block;" class="w3-border w3-padding" alt="Image"><?php
																}
																?>
															</div>
															<div class="col-xs-6">
																<img src="<?php echo base_url().'assets/images/contoh-id-card.jpg'; ?>" style="width: 100%; margin-left: auto; margin-right: auto; display: block;" class="w3-border w3-padding" alt="Image">
																<p class="help-block">
																	<em>Contoh</em>
																</p>
															</div>
														</div>
														<div class="form-group">
															<!-- <label class="control-label" for="upload-pictures1"><b>Upload ID card</b></label><br> -->
															<input type="file" id="upload-pictures1" name="file-id-card" accept=".jpg,.jpeg,.png" class="form-control"> <!-- required> -->
															<p class="help-block">
																<em>Valid file type: .jpg, .jpeg, .png. File size max: 500 KB.</em>
															</p>
														</div>
													</div>
												</div>

												<div class="panel">
													<div class="panel-heading">
														<h3 class="panel-title">Upload selfie dengan ID dan pose <?php echo $m_selfie_pilihan ?></h3>
													</div>
													<div class="panel-body">
														<div class="row">
															<div class="col-xs-6">
																<?php if ($m_file_selfie=='') {
																	?><img src="<?php echo base_url().'assets/images/image-empty.png'; ?>" id="preview-image2" style="width: 100%; margin-left: auto; margin-right: auto; display: block;" class="w3-border w3-padding" alt="Image"><?php
																} else {
																	?><img src="<?php echo base_url().$m_file_selfie; ?>" id="preview-image2" style="width: 100%; margin-left: auto; margin-right: auto; display: block;" class="w3-border w3-padding" alt="Image"><?php
																}
																?>
															</div>
															<div class="col-xs-6">
																<img src="<?php echo base_url().'assets/images/contoh-selfie-id-card.jpg'; ?>" style="width: 100%; margin-left: auto; margin-right: auto; display: block;" class="w3-border w3-padding" alt="Image">
																<p class="help-block">
																	<em>Contoh</em>
																</p>
															</div>
														</div>
														<div class="form-group">
															<!-- <label class="control-label" for="upload-pictures1"><b>Upload ID card</b></label><br> -->
															<input type="file" id="upload-pictures2" name="file-selfie" accept=".jpg,.jpeg,.png" class="form-control"> <!-- required> -->
															<p class="help-block">
																<em>Valid file type: .jpg, .jpeg, .png. File size max: 500 KB.</em>
															</p>
														</div>
													</div>
												</div>

												<div class="panel">
													<div class="panel-heading">
														<h3 class="panel-title">Upload dokumen lainnya</h3>
													</div>
													<div class="panel-body">
														<div class="row">
															<div class="col-md-6">
																<?php if ($m_file_lain=='') {
																	?><img src="<?php echo base_url().'assets/images/image-empty.png'; ?>" id="preview-image3" style="width: 100%; margin-left: auto; margin-right: auto; display: block;" class="w3-border w3-padding" alt="Image"><?php
																} else {
																	?><img src="<?php echo base_url().$m_file_lain; ?>" id="preview-image3" style="width: 100%; margin-left: auto; margin-right: auto; display: block;" class="w3-border w3-padding" alt="Image"><?php
																}
																?>
															</div>
														</div>
														<div class="form-group">
															<!-- <label class="control-label" for="upload-pictures1"><b>Upload ID card</b></label><br> -->
															<input type="file" id="upload-pictures3" name="file-lain" accept=".jpg,.jpeg,.png" class="form-control"> <!-- required> -->
															<p class="help-block">
																<em>Valid file type: .jpg, .jpeg, .png. File size max: 500 KB.</em>
															</p>
														</div>
													</div>
												</div>
												
											</div>
										</div>
									</div>

									<div class="step-pane" data-step="7">
										<p class="lead"><i class="fa fa-check-circle text-success"></i> Pengisian data dan pemesanan barang sudah benar dan sesuai semua</p>
									</div>
								</div>
								<div class="actions">
									<button type="button" class="btn btn-default btn-prev"><i class="fa fa-arrow-left"></i> Kembali</button>
									<button type="button" class="btn btn-primary btn-next" id="btn-next">Lanjut <i class="fa fa-arrow-right"></i></button>
									<button type="submit" class="btn btn-primary" id="btn-submit" onclick="return confirm('Apakah Anda yakin?')">Simpan Perubahan <i class="fa fa-check-circle"></i></button>
								</div>
							</div>
						</div>
						</form>

					</div>
				</div>
				<!-- END MAIN CONTENT -->
			</div>
			<!-- END MAIN -->