<?php
$this->db->select('*');
$this->db->from('u_contact_official');
$query = $this->db->get();
$data = $query->row();
$country_name = $data->country_name;
?>

			<!-- MAIN -->
			<div class="main">
				<!-- MAIN CONTENT -->
				<div class="main-content">
					<div class="container">
						<div class="content-heading clearfix">
							<div class="heading-left">
								<h1 class="page-title">List Member</h1>
							</div>
							<ul class="breadcrumb">
								<li><a href="<?php echo base_url(); ?>marketing/main"><i class="fa fa-home"></i> Dashboad</a></li>
								<li class="active">List Member</li>
								<!-- <li><a href="#">Pages</a></li> -->
								<!-- <li class="active">Order Member</li> -->
							</ul>
						</div>
						<!-- <div class="container-fluid">
							asdasdas
						</div> -->
						<!-- <div class="row">
							<div class="col-md-12">
								<div class="panel">
									<div class="panel-heading">
										<h3 class="panel-title">-</h3>
									</div>
									<div class="panel-body">
										-
									</div>
								</div>
							</div>
						</div> -->

						<div class="table-responsive">
							<table id="featured-datatable" class="table table-striped table-hover">
								<thead>
									<tr>
										<th>#</th>
										<th>No Member</th>
										<th>Nama</th>
										<th>Nomor HP Indonesia</th>
										<th>Nomor HP <?php echo $country_name ?></th>
										<th>Pilihan</th>
									</tr>
								</thead>
								<tbody>
									<?php $no = 1;
									foreach ($data_member as $data_member) { ?>
									<tr>
										<td><?php echo $no ?></td>
										<td><?php echo $data_member['no_member']; ?></td>
										<td><?php echo $data_member['m_nama']; ?></td>
										<td><?php echo $data_member['m_hp_indo']; ?></td>
										<td><?php echo $data_member['m_hp_luar']; ?></td>
										<td>
											<a href="<?php echo base_url().'marketing/member/detail/'.$data_member['id_members'] ?>" type="button" class="btn btn-info btn-xs">Detail</a>
										</td>
									</tr>
									<?php $no++; }; ?>
								</tbody>
							</table>
						</div>
						
					</div>
				</div>
				<!-- END MAIN CONTENT -->
			</div>
			<!-- END MAIN -->