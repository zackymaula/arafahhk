<?php
if ($this->session->userdata('user_id') == '') {
  redirect('mktg');
}
?>

<!doctype html>
<html lang="en" class="fullscreen-bg">
	<head>
		<title>Marketing | Arafah Electronics & Furnituree</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
		<!-- VENDOR CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/vendor/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/vendor/themify-icons/css/themify-icons.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/vendor/pace/themes/orange/pace-theme-minimal.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/vendor/datatables/css-main/jquery.dataTables.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/vendor/datatables/css-bootstrap/dataTables.bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/vendor/datatables-tabletools/css/dataTables.tableTools.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/vendor/parsleyjs/css/parsley.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/vendor/select2/css/select2.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/vendor/summernote/summernote.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/vendor/sweetalert2/sweetalert2.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/vendor/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css">
		<!-- MAIN CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/css/main.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/css/skins/sidebar-nav-darkgray.css" type="text/css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/css/skins/navbar3.css" type="text/css">
		<!-- FOR DEMO PURPOSES ONLY. You should/may remove this in your project -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/assets/css/demo.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template-backend/demo-panel/style-switcher.css">
		<!-- ICONS -->
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>assets/template-backend/assets/img/apple-icon.png">
		<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url(); ?>assets/images/icon-arafahelectronics.png">
		
		<!-- SIGNATURE -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/signature/css/jquery.signaturepad.css">
	</head>

	<style>
	#signArea{
		width:304px;
	}

	</style>

	<body class="layout-topnav">
		<!-- WRAPPER -->
		<div id="wrapper">

			<!-- NAVBAR -->
			<nav class="navbar navbar-default navbar-fixed-top">
				<!-- top bar -->
				<div class="top-bar clearfix">
					<div class="container-bar">
						<div class="brand" style="height:45px;">
							<a href="<?php echo base_url(); ?>marketing/main">
								<img src="<?php echo base_url(); ?>assets/images/logo-arafah-white.png" style="height:23px;" alt="Arafah Logo" class="img-responsive logo">
							</a>
						</div>
					</div>
				</div>
				<!-- end top bar -->
				<!-- main navigation -->
				<div id="navbar-menu" class="bottom-bar clearfix">
					<div class="navbar-header">
						<div class="brand visible-xs">
							<a href="<?php echo base_url(); ?>marketing/main">
								<img src="<?php echo base_url(); ?>assets/images/logo-arafahelectronics.png" style="height:25px;" alt="Arafah Logo" class="img-responsive logo">
							</a>
						</div>
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav">
							<i class="ti-menu"></i>
						</button>
					</div>
					<div class="navbar-collapse collapse" id="main-nav">
						<ul class="nav navbar-nav">
							<li><a href="<?php echo base_url(); ?>marketing/main"><i class="ti-dashboard"></i> <span>Dashboard</span></a></li>
							<li><a href="<?php echo base_url(); ?>marketing/order"><i class="fa fa-shopping-bag"></i> <span>List Order</span></a></li>
							<li class="dropdown">
								<a href="#" data-toggle="dropdown"><i class="ti-shopping-cart"></i> <span>Insert Order</span> <i class="ti-angle-down icon-submenu"></i></a>
								<ul class="dropdown-menu">
									<li>
										<a href="<?php echo base_url(); ?>marketing/order/member">Member</a>
									</li>
									<li>
										<a href="<?php echo base_url(); ?>marketing/order/non_member">Non Member</a>
									</li>
								</ul>
							</li>
							<li><a href="<?php echo base_url(); ?>marketing/member"><i class="ti-user"></i> <span>List Member</span></a></li>
							<li><a href="<?php echo base_url(); ?>marketing/cicilan"><i class="ti-map"></i> <span>Cicilan</span></a></li>
							<!-- <li class="dropdown">
								<a href="index.html" class="active" data-toggle="dropdown"><i class="ti-layout"></i> <span>Contents</span> <i class="ti-angle-down icon-submenu"></i></a>
								<ul class="dropdown-menu">
									<li><a href="<?php echo base_url(); ?>admin/product"><i class="ti-layers-alt"></i><span>Product</span></a></li>
									<li><a href="<?php echo base_url(); ?>admin/slide"><i class="ti-announcement"></i><span>Slide</span></a></li>
									<li><a href="<?php echo base_url(); ?>admin/paket"><i class="ti-credit-card"></i><span>Paket Arafah</span></a></li>
									<li class="dropdown dropdown-sub">
										<a href="#" data-toggle="dropdown"><i class="ti-layout-grid2"></i>Categories <i class="icon-submenu ti-angle-right"></i></a>
										<ul class="dropdown-menu">
											<li><a href="<?php echo base_url(); ?>admin/category">Category</a></li>
											<li><a href="<?php echo base_url(); ?>admin/category/sub">Sub Category</a></li>
										</ul>
									</li>
								</ul>
							</li> -->
						</ul>
						<ul class="nav navbar-nav navbar-right">
							<li><a href="<?php echo base_url(); ?>marketing/login/proses_logout"><i class="ti-power-off"></i> <span>Logout</span></a></li>
							<!-- <li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<img src="<?php echo base_url(); ?>assets/images/icon-arafahelectronics.png" alt="Avatar">
									<span><?php //echo $this->session->userdata('user_name') ?></span> <i class="ti-angle-down icon-submenu"></i></a>
								<ul class="dropdown-menu logged-user-menu">
									<li><a href="<?php echo base_url(); ?>qrc/login/proses_logout"><i class="ti-power-off"></i> <span>Logout</span></a></li>
								</ul>
							</li> -->
						</ul>
					</div>
				</div>
				<!-- end main navigation -->
			</nav>
			<!-- END NAVBAR -->