<?php
$this->db->select('*');
$this->db->from('u_contact_official');
$query = $this->db->get();
$data = $query->row();
$country_kurs = $data->country_kurs;
?>

			<!-- MAIN -->
			<div class="main">
				<!-- MAIN CONTENT -->
				<div class="main-content">
					<div class="container">
						<div class="content-heading clearfix">
							<div class="heading-left">
								<h1 class="page-title">List Cicilan</h1>
							</div>
							<ul class="breadcrumb">
								<li><a href="<?php echo base_url(); ?>marketing/main"><i class="fa fa-home"></i> Dashboad</a></li>
								<li class="active">List Cicilan</li>
								<!-- <li><a href="#">Pages</a></li> -->
								<!-- <li class="active">Order Member</li> -->
							</ul>
						</div>
						<!-- <div class="container-fluid">
							asdasdas
						</div> -->
						<!-- <div class="row">
							<div class="col-md-12">
								<div class="panel">
									<div class="panel-heading">
										<h3 class="panel-title">-</h3>
									</div>
									<div class="panel-body">
										-
									</div>
								</div>
							</div>
						</div> -->

						<div class="table-responsive">
							<table id="featured-datatable" class="table table-striped table-hover">
								<thead>
									<tr>
										<th>#</th>
										<th>No Faktur</th>
										<th>No Member</th>
										<th>Nama Member</th>
										<th>Cicilan/bulan</th>
										<th>Proses Cicilan<h5 style="width:150px;"></h5></th>
										<th>Pilihan</th>
									</tr>
								</thead>
								<tbody>
									<?php $no = 1;
									foreach ($data_cicilan as $data_cicilan) { ?>
									<tr>
										<td><?php echo $no ?></td>
										<td><?php echo $data_cicilan['no_order']; ?></td>
										<td><?php echo $data_cicilan['no_member']; ?></td>
										<td><?php echo $data_cicilan['m_nama']; ?></td>
										<td><?php echo $data_cicilan['order_cicilan_total'].' '.$country_kurs; ?></td>
										<td>
											<div class="progress">
												<div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $data_cicilan['persen_cicilan']; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $data_cicilan['persen_cicilan']; ?>%;">
													<?php echo $data_cicilan['total_cicilan']; ?>/<?php echo $data_cicilan['order_tempo']; ?>
												</div>
											</div>
										</td>

										<td>
											<a href="#<?php //echo base_url().'marketing/member/detail/'.$data_member['id_members'] ?>" type="button" class="btn btn-success btn-xs">Bayar</a>
										</td>
									</tr>
									<?php $no++; }; ?>

								</tbody>
							</table>
						</div>
						
					</div>
				</div>
				<!-- END MAIN CONTENT -->
			</div>
			<!-- END MAIN -->