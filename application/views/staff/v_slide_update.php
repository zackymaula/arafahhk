<!-- MAIN -->
			<div class="main">
				<!-- MAIN CONTENT -->
				<div class="main-content">
					<div class="container">
						<div class="content-heading clearfix">
							<div class="heading-left">
								<h1 class="page-title">Update Slide Marketing</h1>
							</div>
							<ul class="breadcrumb">
								<li><a href="<?php echo base_url(); ?>staff/main"><i class="fa fa-home"></i> Dashboad</a></li>
								<li><a href="<?php echo base_url(); ?>staff/slide"><i class="fa fa-home"></i> Slide Marketing</a></li>
								<li class="active">Update Slide Marketing</li>
								<!-- <li><a href="#">Pages</a></li> -->
								<!-- <li class="active">Order Member</li> -->
							</ul>
						</div>
						
						<form id="basic-form" class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>staff/slide/update_do" enctype="multipart/form-data">

							<input type="hidden" name="post_id_slide" value="<?php echo $id_slide ?>" />
							<input type="hidden" name="post_file_path" value="<?php echo $file_path ?>" />
							<input type="hidden" name="post_file_name" value="<?php echo $file_name ?>" />

							<div class="form-group">
								<label for="preview-image" class="col-sm-3 control-label"></label>
								<div class="col-sm-9">
									<img src="<?php echo base_url().$file_path.$file_name; ?>" id="preview-image" style="width: 500px; margin:auto; display:block;" class="w3-border w3-padding" alt="Image">
								</div>
							</div>

							<div class="form-group">
								<label for="upload-pictures-300" class="col-sm-3 control-label">Pictures</label>
								<div class="col-md-9">
									<input type="file" id="upload-pictures-300" name="post_file" accept=".jpg,.jpeg,.png">
									<p class="help-block">
										<em>Valid file type: .jpg, .jpeg, .png. File size max: 300 KB. File dimension: 1110x1110 pixel</em>
									</p>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label">Marketing</label>
								<div class="col-md-9">
									<select id="staff" class="form-control" name="post_id_staff">
										<option value="">Please Select</option>
										<?php foreach ($data_staff as $data_staff) { ?>
											<option <?php if( $id_staff==$data_staff['id_staff']){echo "selected"; } ?> 
												id="staff" value="<?php echo $data_staff['id_staff'] ?>"><?php echo $data_staff['name'] ?>
											</option>
										<?php }; ?>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label for="slide-title" class="col-sm-3 control-label">Title</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="slide-title" name="post_title" placeholder="Title" value="<?php echo $title ?>" required>
								</div>
							</div>

							<div class="form-group">
								<label for="slide-description" class="col-sm-3 control-label">Description</label>
								<div class="col-sm-9">
									<textarea class="summernote_description" id="slide-description" name="post_description"><?php echo $description ?>
									</textarea>
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-9">
									<button type="submit" class="btn btn-primary btn-block" onclick="return confirm('Apakah data sudah benar semua?')">Update Slide</button>
								</div>
							</div>
						</form>
						
					</div>
				</div>
				<!-- END MAIN CONTENT -->
			</div>
			<!-- END MAIN -->