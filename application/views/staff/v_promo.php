<!-- MAIN -->
			<div class="main">
				<!-- MAIN CONTENT -->
				<div class="main-content">
					<div class="container">
						<div class="content-heading clearfix">
							<div class="heading-left">
								<h1 class="page-title">Promo Bulanan</h1>
							</div>
							<ul class="breadcrumb">
								<li><a href="<?php echo base_url(); ?>staff/main"><i class="fa fa-home"></i> Dashboad</a></li>
								<li class="active">Promo Bulanan</li>
								<!-- <li><a href="#">Pages</a></li> -->
								<!-- <li class="active">Order Member</li> -->
							</ul>
						</div>
						<p class="demo-button">
							<a href="<?php echo base_url(); ?>staff/promo/insert" type="button" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="right" title="Insert"><i class="fa fa-plus-square"></i>
								<span class="sr-only">Insert</span>
							</a>
						</p>
						<div class="table-responsive">
							<table id="featured-datatable" class="table table-striped table-hover">
								<thead>
									<tr>
										<th>#</th>
										<th>Picture</th>
										<th>Title</th>
										<th>Price</th>
										<th>Option</th>
									</tr>
								</thead>
								<tbody>
									<?php $no = 1;
									foreach ($data_promo as $data_promo) { ?>
									<tr>
										<td><?php echo $no ?></td>
										<td><img src="<?php echo base_url().$data_promo['promo_file_path'].$data_promo['promo_file_name']; ?>" style="width: 150px;" class="w3-border w3-padding" alt="Image"></td>
										<td><?php echo $data_promo['promo_name']; ?></td>
										<td><?php echo $data_promo['promo_price']; ?></td>
										<td>
											<div class="btn-group">
												<a href="#" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="Detail">
													<span class="sr-only">Detail</span><i class="fa fa-info-circle"></i></a>
												<a href="<?php echo base_url(); ?>staff/promo/update/<?php echo $data_promo['id_promo'] ?>" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Edit">
													<span class="sr-only">Edit</span><i class="fa fa-pencil"></i></a>
												<a href="<?php echo base_url(); ?>staff/promo/delete/<?php echo $data_promo['id_promo'] ?>" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Remove" onclick="return confirm('Anda yakin untuk menghapus data?')">
													<span class="sr-only">Remove</span><i class="fa fa-remove"></i></a>
											</div>
										</td>
									</tr>
									<?php $no++; }; ?>
								</tbody>
							</table>
						</div>
						
					</div>
				</div>
				<!-- END MAIN CONTENT -->
			</div>
			<!-- END MAIN -->