<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	public function template_data()
	{
		$headerCategories = $this->m_template->GetCategories();
		$headerCategorySub = $this->m_template->GetCategorySub();
		$headerCategoryType = $this->m_template->GetCategoryType();
		$contentSlider = $this->m_promo->GetSlidersMain();
		$contentProductAll = $this->m_product->GetProductAll();
		$contentProductAllLimit = $this->m_product->GetProductAllLimit();
		$contentPaket = $this->m_promo->GetPaket();
		$contentPromo = $this->m_promo->GetPromo();
		$contentTestimoni = $this->m_promo->GetTestimoni();

		$arrayData = array(
			'data_categories' => $headerCategories,
			'data_category_sub' => $headerCategorySub,
			'data_category_type' => $headerCategoryType,
			'data_slider' => $contentSlider,
			'data_product_all' => $contentProductAll,
			'data_product_limit' => $contentProductAllLimit,
			'data_paket' => $contentPaket,
			'data_promo' => $contentPromo,
			'data_testimoni' => $contentTestimoni
		);

		return $arrayData;
	}

	public function index()
	{
		//ACCESS LOCATION
		//$countryAccess = $this->m_location->CheckLocation();
		$countryAccess = '1';

		if ($countryAccess == '1') {
			$template_data = $this->template_data();
			$this->load->template_front('front/v_main', $template_data);
		} else {
			echo "Access denied!";
		}

		//INSERT VIEWER
		$this->m_viewer->InsertViewWebsite();

		//LOAD MORE PRODUCT
		//$this->load_more_product();
	}

	public function product_all()
	{
		$template_data = $this->template_data();

		$this->load->template_front('front/v_product_all', $template_data);
	}

	public function paket()
	{
		//INSERT VIEWER
		$this->m_viewer->InsertViewCategory('1');

		$template_data = $this->template_data();

		$this->load->template_front('front/v_paket', $template_data);
	}

	public function bangunan()
	{
		//INSERT VIEWER
		$this->m_viewer->InsertViewCategory('7');

		$template_data = $this->template_data();

		$this->load->template_front('front/v_bangunan', $template_data);
	}

	public function slide_detail($id_slide)
	{
		//INSERT VIEWER
		$this->m_viewer->InsertViewSlide($id_slide);

		$headerCategories = $this->m_template->GetCategories();
		$headerCategorySub = $this->m_template->GetCategorySub();
		$headerCategoryType = $this->m_template->GetCategoryType();
		$contentSliderDetail = $this->m_promo->GetSlideDetail($id_slide);

		$arrayData = array(
			'data_categories' => $headerCategories,
			'data_category_sub' => $headerCategorySub,
			'data_category_type' => $headerCategoryType,
			
			'content_slide_filepath' => $contentSliderDetail[0]['file_path'],
			'content_slide_filename' => $contentSliderDetail[0]['file_name'],
			'content_slide_description' => $contentSliderDetail[0]['description']
		);

		$this->load->template_front('front/v_slide_detail', $arrayData);
	}

	public function promo_detail($id_promo)
	{
		//INSERT VIEWER
		//$this->m_viewer->InsertViewSlide($id_slide);

		$headerCategories = $this->m_template->GetCategories();
		$headerCategorySub = $this->m_template->GetCategorySub();
		$headerCategoryType = $this->m_template->GetCategoryType();
		$contentPromoDetail = $this->m_promo->GetPromoDetail($id_promo);

		$arrayData = array(
			'data_categories' => $headerCategories,
			'data_category_sub' => $headerCategorySub,
			'data_category_type' => $headerCategoryType,
			
			'content_promo_name' => $contentPromoDetail[0]['promo_name'],
			'content_promo_price' => $contentPromoDetail[0]['promo_price'],
			//'content_promo_discount' => $contentPromoDetail[0]['promo_discount'],
			'content_promo_filepath' => $contentPromoDetail[0]['promo_file_path'],
			'content_promo_filename' => $contentPromoDetail[0]['promo_file_name'],
			'content_promo_description' => $contentPromoDetail[0]['promo_description']
		);

		$this->load->template_front('front/v_promo_detail', $arrayData);
	}

	public function load_more_product()
	{
		$limit = $_POST['limit'];
		$offset = $_POST['offset'];
		$output ='';
		//$last_id ='';
		sleep(1);

		$kurs = $this->country_kurs();
		$data_product_load_more = $this->m_product->GetProductAllLoadMore($limit,$offset);

		if($data_product_load_more->num_rows() > 0){

			foreach ($data_product_load_more->result_array() as $data_products) {

				//$last_id = $data_products['id_products'];

				$output .= '<div class="col-4 col-md-3 mb-4 mb-md-0 product">';
				if ($data_products['price_discount']!=NULL) {
				 	//$output .= "<a href='".base_url().'product/view/'.$data_products['id_products']."'><span class='onsale'>Promo</span></a>";
				}
				$output .= "<span class='product-thumb-info border-0'>
								<a href='".base_url().'product/view/'.$data_products['id_products']."'>
									<span class='product-thumb-info-image'>
										<img alt='' class='img-fluid' src='".base_url().$data_products['file_path'].$data_products['file_name']."'>
									</span>
								</a>
								<span class='product-thumb-info-content product-thumb-info-content pl-0 bg-color-light'>
									<a href='".base_url().'product/view/'.$data_products['id_products']."'>
										<h4 class='text-4 text-primary'>".$data_products['product_name']."</h4>
										<span class='price'>";
				if ($data_products['price_discount']==NULL) {
					$output .= "<span class='amount text-dark font-weight-semibold'>".$data_products['price_regular']." ".$kurs."</span>";
				} else {
					$output .= "<del><span class='amount'>".$data_products['price_regular']." ".$kurs."</span></del>
								<ins><span class='amount text-secondary font-weight-semibold'>".$data_products['price_discount']." ".$kurs."</span></ins>";
				};
				$output .= "			</span>
									</a>
								</span>
							</span>
						</div>";
			};

			/*$output .=' <div class="row" id="remove_row">
						<div class="col">
							<div class="heading heading-border heading-middle-border heading-middle-border-center text-center">
								<a id="btn_more" data-vid="'.$last_id.'">
									<h4 class="word-rotator letters scale mb-2">
										<span>Lihat produk lainnya</span>
										<span class="word-rotator-words bg-primary rounded">
											<b class="is-visible">Klik Disini !!!</b>
											<b>Klik Disini !!!</b>
										</span>
									</h4>
								</a>
							</div>
						</div>
						</div>';*/
			echo $output;
		}

	}

	public function country_kurs()
	{
		$this->db->select('*');
		$this->db->from('u_contact_official');
		$query = $this->db->get();
		$data = $query->row();
		$country_kurs = $data->country_kurs;

		return $country_kurs;
	}

	public function location()
	{
		echo $this->m_location->CheckFullLocation();
		//echo $this->m_location->ChechLocationArray();
	}

	//REVIEW DATA ORDER KONSUMEN
	function template_data_confirm_customer($no_order_generate)
	{
		$headerCategories = $this->m_template->GetCategories();
		$headerCategorySub = $this->m_template->GetCategorySub();
		$dataOfficial = $this->m_template->GetOfficialData();
		$detailOrder = $this->m_order->GetDetailOrderByNoOrderGenerate($no_order_generate);
		
		if (count($detailOrder)>=1) {
			$dataListOrderBarang = $this->m_order->GetListBarangOrders($detailOrder[0]['id_orders']);
		} else {
			//show_404();
			redirect(base_url().'myorder/review/'.$no_order_generate);
		}

		$arrayData = array(
			'data_categories' => $headerCategories,
			'data_category_sub' => $headerCategorySub,

			'id_orders' => $detailOrder[0]['id_orders'],
			'no_order' => $detailOrder[0]['no_order'],
			'no_order_generate' => $detailOrder[0]['no_order_generate'],
			'biaya_order' => $detailOrder[0]['biaya_order'],
			'id_order_status' => $detailOrder[0]['id_order_status'],
			'os_name' => $detailOrder[0]['os_name'],

			'id_members' => $detailOrder[0]['id_members'],
			'no_member' => $detailOrder[0]['no_member'],
			'm_nama' => $detailOrder[0]['m_nama'],
			'm_lahir_tmpt' => $detailOrder[0]['m_lahir_tmpt'],
			'm_lahir_tgl' => (DateTime::createFromFormat('d/m/Y', $detailOrder[0]['m_lahir_tgl']))->format('d F Y'),
			'm_status_nikah' => $detailOrder[0]['m_status_nikah'],
			'm_alamat_indo' => $detailOrder[0]['m_alamat_indo'],
			'm_nama_ayah' => $detailOrder[0]['m_nama_ayah'],
			'm_nama_ibu' => $detailOrder[0]['m_nama_ibu'],
			'm_hp_indo' => $detailOrder[0]['m_hp_indo'],
			'm_hp_luar' => $detailOrder[0]['m_hp_luar'],
			'm_status_rumah' => $detailOrder[0]['m_status_rumah'],
			'm_pekerjaan' => $detailOrder[0]['m_pekerjaan'],
			'm_pekerjaan_no_id_luar' => $detailOrder[0]['m_pekerjaan_no_id_luar'],
			'm_pekerjaan_masa_berlaku' => $detailOrder[0]['m_pekerjaan_masa_berlaku'],
			'm_pekerjaan_alamat' => $detailOrder[0]['m_pekerjaan_alamat'],
			'm_pekerjaan_teman_nama' => $detailOrder[0]['m_pekerjaan_teman_nama'],
			'm_pekerjaan_teman_hp' => $detailOrder[0]['m_pekerjaan_teman_hp'],
			'm_medsos_line' => $detailOrder[0]['m_medsos_line'],
			'm_medsos_fb' => $detailOrder[0]['m_medsos_fb'],
			'm_medsos_ig' => $detailOrder[0]['m_medsos_ig'],
			'm_medsos_wa' => $detailOrder[0]['m_medsos_wa'],
			'm_file_id_card' => $detailOrder[0]['m_file_id_card'],
			'm_file_selfie' => $detailOrder[0]['m_file_selfie'],
			'm_file_lain' => $detailOrder[0]['m_file_lain'],
			'm_file_lain_2' => $detailOrder[0]['m_file_lain_2'],
			'm_selfie_pilihan' => $detailOrder[0]['m_selfie_pilihan'],

			'or_nama' => $detailOrder[0]['or_nama'],
			'or_no_id' => $detailOrder[0]['or_no_id'],
			'or_hubungan' => $detailOrder[0]['or_hubungan'],
			'or_alamat_negara' => $detailOrder[0]['or_alamat_negara'],
			'or_alamat_detail' => $detailOrder[0]['or_alamat_detail'],
			'or_hp' => $detailOrder[0]['or_hp'],
			'or_hp_keluarga' => $detailOrder[0]['or_hp_keluarga'],

			'data_list_order_barang' => $dataListOrderBarang,

			'official_country_name' => $dataOfficial[0]['country_name'],
			'official_country_kurs' => $dataOfficial[0]['country_kurs'],
			'official_country_denda' => $dataOfficial[0]['country_denda']
		);

		return $arrayData;
	}

	//ORDER CONFIRM CUSTOMER
	public function order_customer_review($no_order_generate)
	{
		$template_data_confirm_customer = $this->template_data_confirm_customer($no_order_generate);

		$this->load->template_front('front/v_order_customer_review', $template_data_confirm_customer);
	}
	/*public function order_customer_confirm($no_order_generate)
	{
		$template_data_confirm_customer = $this->template_data_confirm_customer($no_order_generate);

		$this->load->template_front('front/v_order_customer_confirm', $template_data_confirm_customer);
	}*/
	public function order_customer_confirm($no_order_generate)
	{
		$template_data_confirm_customer = $this->template_data_confirm_customer($no_order_generate);

		$this->load->template_front('front/v_order_term_condition', $template_data_confirm_customer);
	}

	/*public function curl_prod()
	{
		//$url=$_POST['txturl'];
		$url= 'http://omimerchantapi.tng.asia/test'; //PROD
		//$url= 'http://merchantapi-pp.tng.asia/test'; //PRE
		$ch = curl_init();//I have removed it from here
		curl_setopt($ch, CURLOPT_URL,$url);// This will do
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		$output = curl_exec($ch);
		echo $output;
		curl_close($ch); 
	}*/
}
