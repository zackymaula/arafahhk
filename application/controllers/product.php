<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

	public function template_data($id_products)
	{
		$headerCategories = $this->m_template->GetCategories();
		$headerCategorySub = $this->m_template->GetCategorySub();
		$headerCategoryType = $this->m_template->GetCategoryType();
		$contentProduct = $this->m_product->GetProduct($id_products);
		$contentProductFiles = $this->m_product->GetProductFiles($id_products);
		$contentProductWithoutSub = $this->m_product->GetProductWithoutSub($id_products);
		$idCategoriesProducts = $this->m_product->GetProductIdCategories($id_products);
		$contentProductLainnya = $this->m_product->GetProductAllCategoriesLainnya($idCategoriesProducts);

		if ($contentProduct=='0') {
			$arrayData = array(
				'data_categories' => $headerCategories,
				'data_category_sub' => $headerCategorySub,
				'data_category_type' => $headerCategoryType,
				'data_product_lainnya' => $contentProductLainnya,

				'content_product' => '0',
				'content_product_files' => $contentProductFiles,

				'content_product_id_categories' => $contentProductWithoutSub[0]['id_categories'],
				'content_product_c_name' => $contentProductWithoutSub[0]['c_name'],
	
				'content_product_name' => $contentProductWithoutSub[0]['product_name'],
				'content_product_price_regular' => $contentProductWithoutSub[0]['price_regular'],
				'content_product_price_discount' => $contentProductWithoutSub[0]['price_discount'],
				'content_product_specification' => $contentProductWithoutSub[0]['specification'],
				'content_product_description' => $contentProductWithoutSub[0]['description'],
				'content_product_tags' => $contentProductWithoutSub[0]['product_tags']
			);
		} else {
			$arrayData = array(
				'data_categories' => $headerCategories,
				'data_category_sub' => $headerCategorySub,
				'data_category_type' => $headerCategoryType,
				'data_product_lainnya' => $contentProductLainnya,

				'content_product' => '1',
				'content_product_files' => $contentProductFiles,
	
				'content_product_id_categories' => $contentProduct[0]['id_categories'],
				'content_product_id_category_sub' => $contentProduct[0]['id_category_sub'],
				'content_product_c_name' => $contentProduct[0]['c_name'],
				'content_product_cs_name' => $contentProduct[0]['cs_name'],
	
				'content_product_name' => $contentProduct[0]['product_name'],
				'content_product_price_regular' => $contentProduct[0]['price_regular'],
				'content_product_price_discount' => $contentProduct[0]['price_discount'],
				'content_product_specification' => $contentProduct[0]['specification'],
				'content_product_description' => $contentProduct[0]['description'],
				'content_product_tags' => $contentProduct[0]['product_tags']
			);
		}

		

		return $arrayData;
	}

	public function view($id_products)
	{
		//INSERT VIEWER
		$this->m_viewer->InsertViewProduct($id_products);

		$template_data = $this->template_data($id_products);

		$this->load->template_front('front/v_product', $template_data);
	}
}
