<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Staffurl extends CI_Controller {

	public function template_data($url)
	{
		$headerCategories = $this->m_template->GetCategories();
		$headerCategorySub = $this->m_template->GetCategorySub();
		$headerCategoryType = $this->m_template->GetCategoryType();
		$contentSlider = $this->m_promo->GetSlidersMain();
		$contentProductAllLimit = $this->m_product->GetProductAllLimit();
		$contentPromo = $this->m_promo->GetPromo();
		$contentTestimoni = $this->m_promo->GetTestimoni();
		
		$dataStaff = $this->m_user->Getstaff($url);
		$dataStaffArray = $dataStaff->result_array();

		if ($dataStaff->num_rows()>=1) {
			foreach ($dataStaffArray as $data) {
				$id = $data['id_staff'];
				$nama = $data['name'];
				$cp = $data['contact_person'];
				$email = $data['email'];
				$photo_file_path = $data['photo_file_path'];
				$photo_file_name = $data['photo_file_name'];
			}
			$this->session->set_userdata('staff_id', $id);
			$this->session->set_userdata('staff_nama', $nama);
			$this->session->set_userdata('staff_cp', $cp);
			$this->session->set_userdata('staff_email', $email);
			$this->session->set_userdata('staff_photo', $photo_file_path.$photo_file_name);

		} else {
			//show_404();
			redirect(base_url().'me/'.$url);
		}

		$arrayData = array(
			'data_categories' => $headerCategories,
			'data_category_sub' => $headerCategorySub,
			'data_category_type' => $headerCategoryType,
			'data_slider' => $contentSlider,
			'data_product_limit' => $contentProductAllLimit,
			'data_promo' => $contentPromo,
			'data_testimoni' => $contentTestimoni
		);

		return $arrayData;
	}

	public function view($url)
	{
		//INSERT VIEWER
		$this->m_viewer->InsertViewWebsite();

		//ACCESS LOCATION
		//$countryAccess = $this->m_location->CheckLocation();
		$countryAccess = '1';

		if ($countryAccess == '1') {
			$template_data = $this->template_data($url);
			$this->load->template_front('front/v_main', $template_data);
		} else {
			echo "Access denied!";
		}
	}
}
