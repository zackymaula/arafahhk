<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function template_data()
	{
		$headerCategories = $this->m_template->GetCategories();
		$headerCategorySub = $this->m_template->GetCategorySub();
		$headerCategoryType = $this->m_template->GetCategoryType();

		$arrayData = array(
			'data_categories' => $headerCategories,
			'data_category_sub' => $headerCategorySub,
			'data_category_type' => $headerCategoryType
		);

		return $arrayData;
	}

	public function index()
	{
		$template_data = $this->template_data();
		$this->load->template_front('front/v_user_login', $template_data);
	}

	public function proses_login()
	{
		$usernm = $_POST['post_usernm'];
		$passwd = $_POST['post_passwd'];

		$dataMember = $this->m_login->GetUserMember($usernm,$passwd);
		$arrayDataMember = $dataMember->result_array();

		if ($dataMember->num_rows()>=1) {
			foreach ($arrayDataMember as $data) {
				$id_members = $data['id_members'];
				$no_member = $data['no_member'];
				$m_nama = $this->convert_name($data['m_nama']);
			}
			$this->session->set_userdata('member_id', $id_members);
			$this->session->set_userdata('member_no', $no_member);
			$this->session->set_userdata('member_name', $m_nama);

			redirect('user/transaction');

		} else {
			redirect('login');
		}
	}

	public function proses_logout()
	{
		$this->session->sess_destroy();
		redirect('main');
	}

	public function convert_name($name)
	{
		$pisah_nama = explode(" ", $name);
		$nama_depan = $pisah_nama[0];
		$nama_kedua = $pisah_nama[1];

		$return = $nama_depan.' '.substr($nama_kedua,0,1).'.';

        return $return;
	}
}
