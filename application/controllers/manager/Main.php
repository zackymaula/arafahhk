<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	public function index()
	{
		$dataOrderList = $this->m_order->GetOrderList();
		$arrayData = array(
			'data_order' => $dataOrderList
		);

		$this->load->template_manager('manager/v_main', $arrayData);
	}

	//MASUK VIEW CONFIRM MANAGER
	public function confirm_manager($id_orders)
	{
		$detailOrder = $this->m_order->GetDetailOrder($id_orders);

		$arrayData = array(
			'id_orders' => $detailOrder[0]['id_orders'],
			'id_order_sign' => $detailOrder[0]['id_order_sign'],
			'no_order' => $detailOrder[0]['no_order'],
			'os_name' => $detailOrder[0]['os_name']
		);

		$this->load->template_manager('manager/v_confirm_manager', $arrayData);
	}

	//INSERT PIC SIGN
	public function insert_pic_sign()
	{
		$imagedata = base64_decode($_POST['img_data']);
		$file_name = 'signature-'.date('ymdHis');
		//Location to where you want to created sign image
		$file_path = 'assets/images/signatures/'.$file_name.'.png';
		file_put_contents($file_path,$imagedata);
	}

	//INSERT DATA SIGN MANAGER + UPDATE STATUS ORDER
	public function insert_data_sign_manager()
	{
		$id_orders = $_POST['post_id_order'];
		$id_order_sign = $_POST['post_id_sign'];
		$file_name = 'signature-'.date('ymdHis');
		$file_path = 'assets/images/signatures/'.$file_name.'.png';
		$date = date('Y-m-d H:i:s');

		//DATA SIGN | UPDATE SIGN
		$data_sign = array(
			'id_user_manager' => $this->session->userdata('user_id'),
			'file_sign_manager' => $file_path,
			'updated_at' => $date
		);
		$where_sign = array('id_order_sign' => $id_order_sign);
		$this->m_order->Update('t_order_sign',$data_sign,$where_sign);

		//DATA UPDATE STATUS ORDER
		$data_update_order = array(
			'id_order_status' => '4',
			'updated_at' => $date
		);
		$where_order = array('id_orders' => $id_orders);
		$this->m_order->Update('t_orders',$data_update_order,$where_order);

		redirect('manager/main');
	}

	public function detail($id_orders)
	{
		$detailOrder = $this->m_order->GetDetailOrder($id_orders);
		$dataListOrderBarang = $this->m_order->GetListBarangOrders($detailOrder[0]['id_orders']);

		$arrayData = array(

			'id_orders' => $detailOrder[0]['id_orders'],
			'no_order' => $detailOrder[0]['no_order'],
			'no_order_generate' => $detailOrder[0]['no_order_generate'],
			'biaya_order' => $detailOrder[0]['biaya_order'],
			'id_order_status' => $detailOrder[0]['id_order_status'],
			'os_name' => $detailOrder[0]['os_name'],

			'id_members' => $detailOrder[0]['id_members'],
			'no_member' => $detailOrder[0]['no_member'],
			'm_nama' => $detailOrder[0]['m_nama'],
			'm_lahir_tmpt' => $detailOrder[0]['m_lahir_tmpt'],
			'm_lahir_tgl' => (DateTime::createFromFormat('d/m/Y', $detailOrder[0]['m_lahir_tgl']))->format('d F Y'),
			'm_status_nikah' => $detailOrder[0]['m_status_nikah'],
			'm_alamat_indo' => $detailOrder[0]['m_alamat_indo'],
			'm_nama_ayah' => $detailOrder[0]['m_nama_ayah'],
			'm_nama_ibu' => $detailOrder[0]['m_nama_ibu'],
			'm_hp_indo' => $detailOrder[0]['m_hp_indo'],
			'm_hp_luar' => $detailOrder[0]['m_hp_luar'],
			'm_status_rumah' => $detailOrder[0]['m_status_rumah'],
			'm_pekerjaan' => $detailOrder[0]['m_pekerjaan'],
			'm_pekerjaan_no_id_luar' => $detailOrder[0]['m_pekerjaan_no_id_luar'],
			'm_pekerjaan_masa_berlaku' => $detailOrder[0]['m_pekerjaan_masa_berlaku'],
			'm_pekerjaan_alamat' => $detailOrder[0]['m_pekerjaan_alamat'],
			'm_pekerjaan_teman_nama' => $detailOrder[0]['m_pekerjaan_teman_nama'],
			'm_pekerjaan_teman_hp' => $detailOrder[0]['m_pekerjaan_teman_hp'],
			'm_medsos_line' => $detailOrder[0]['m_medsos_line'],
			'm_medsos_fb' => $detailOrder[0]['m_medsos_fb'],
			'm_medsos_ig' => $detailOrder[0]['m_medsos_ig'],
			'm_medsos_wa' => $detailOrder[0]['m_medsos_wa'],
			'm_file_id_card' => $detailOrder[0]['m_file_id_card'],
			'm_file_selfie' => $detailOrder[0]['m_file_selfie'],
			'm_file_lain' => $detailOrder[0]['m_file_lain'],
			'm_file_lain_2' => $detailOrder[0]['m_file_lain_2'],
			'm_selfie_pilihan' => $detailOrder[0]['m_selfie_pilihan'],

			'or_nama' => $detailOrder[0]['or_nama'],
			'or_no_id' => $detailOrder[0]['or_no_id'],
			'or_hubungan' => $detailOrder[0]['or_hubungan'],
			'or_alamat_negara' => $detailOrder[0]['or_alamat_negara'],
			'or_alamat_detail' => $detailOrder[0]['or_alamat_detail'],
			'or_hp' => $detailOrder[0]['or_hp'],
			'or_hp_keluarga' => $detailOrder[0]['or_hp_keluarga'],

			'file_sign_member' => $detailOrder[0]['file_sign_member'],
			'file_sign_marketing' => $detailOrder[0]['file_sign_marketing'],
			'file_sign_manager' => $detailOrder[0]['file_sign_manager'],

			'data_list_order_barang' => $dataListOrderBarang
		);

		$this->load->template_manager('manager/v_order_detail', $arrayData);
	}

	public function cancel_order($id_orders)
	{
		//DATA UPDATE STATUS ORDER
		$data_update_order = array(
			'id_order_status' => '7',
			'updated_at' => $date
		);

		$where_order = array('id_orders' => $id_orders);
		$this->m_order->Update('t_orders',$data_update_order,$where_order);
		
		redirect('manager/main');
	}
}