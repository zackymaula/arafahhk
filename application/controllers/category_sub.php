<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_sub extends CI_Controller {

	public function template_data($id_category_sub)
	{
		$headerCategories = $this->m_template->GetCategories();
		$headerCategorySub = $this->m_template->GetCategorySub();
		$headerCategoryType = $this->m_template->GetCategoryType();
		$contentCategorySub = $this->m_category->GetCategorySub($id_category_sub);
		$contentProductAllCategorySub = $this->m_product->GetProductAllCategorySub($id_category_sub);

		$arrayData = array(
			'data_categories' => $headerCategories,
			'data_category_sub' => $headerCategorySub,
			'data_category_type' => $headerCategoryType,

			'content_categories_id' => $contentCategorySub[0]['id_categories'],
			'content_categories_name' => $contentCategorySub[0]['c_name'],
			'content_category_sub_name' => $contentCategorySub[0]['cs_name'],
			'data_products' => $contentProductAllCategorySub
		);

		return $arrayData;
	}

	public function view($id_category_sub)
	{
		$template_data = $this->template_data($id_category_sub);
		
		$this->load->template_front('front/v_category_sub', $template_data);
	}
}
