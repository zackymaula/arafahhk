<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tng extends CI_Controller {

	public function api_callback()
	{
		/*$postdata = array(
			'checksum' => $_POST['checksum'],
			'merchant_mid' => $_POST['merchant_mid'],
			'order_no' => $_POST['order_no'],
			'amount' => $_POST['amount'],
			'tng_tx_no' => $_POST['tng_tx_no'],
			'process_date' => $_POST['process_date'] 
		);

		$opts = array('http' =>
			array(
				'method' => 'POST', 
				'header' => 'Content-type: application/x-www-form-urlencoded', 
				'content' => http_build_query($postdata)
			)
		);

		$context  = stream_context_create($opts);
		$result = file_get_contents(base_url().'tng/request', false, $context);
		echo $result;*/

		$checkSum = $_POST['checksum'];
		$merchant_mid = $_POST['merchant_mid'];
		$order_no = $_POST['order_no'];
		$amount = $_POST['amount'];
		$tng_tx_no = $_POST['tng_tx_no'];
		$process_date = $_POST['process_date'];

		$status = $this->update_order_tng_db($order_no);

		$postdata = array(
			'status' => $status
		);

		echo json_encode($postdata);
	}

	/*public function request()
	{
		$postdata = array(
			'status' => '0'
		);

		echo json_encode($postdata);
	}*/

	function callAPIByParam($url, $data)
	{
		$options = array(
			'http' => array(
				'method'  => 'POST',
				'header'  => 'Content-type: application/x-www-form-urlencoded',
				'content' => http_build_query($data)
			)
		);
		$context  = stream_context_create($options);
		$result = file_get_contents($url, false, $context);
		return $result;	
	}

	//DETAIL TRANSAKSI
	public function update_order_tng_db($order_no)
	{
		$secretKey = 'jv97q13zfj7ccgg7njykbain5yyp17v'; //PROD
		//$secretKey = '5cosn53p237fjtmbj5i1la3p8vv4b1n';	//PRE
		$merchant_mid = 'MID801-000466';
		//$order_no = '5d9c310b97eb9';

		$checkSum = hash('sha256', $secretKey.$merchant_mid.$order_no);

		$map = array('checksum' => $checkSum,
             'merchant_mid' => $merchant_mid,
             'order_no' => $order_no);

		$END_POINT="https://omimerchantapi.tng.asia/tng"; //PROD
		//$END_POINT="https://merchantapi-pp.tng.asia/tng"; //PRE	
		$REQUEST_OMI="/omi/request-qrcode";
		$GET_TRANSACTION_STATUS="/omi/get-transaction-info";
		$VOID_TRANSACTION="/omi/void-tx";

		$url = $END_POINT.$GET_TRANSACTION_STATUS;

		//echo $this->callAPIByParam($url, $map);

		$API = $this->callAPIByParam($url, $map);	//GET JSON FROM TNG
		$API_item = json_decode($API);				//DECODE JSON
		$str_tx_status = $API_item->omiRequest->status;
		//$str_tx_no = $API_item->omiRequest->tng_tx_no;
		//$str_tx_amount = $API_item->omiRequest->amount;

		//echo $API;

		//PROSES UPDATE DB
		$data_update_order_tng = array(
			//'tng_tx_no' => $str_tx_no,
			//'tng_amount' => $str_tx_amount,
			'tng_status' => $str_tx_status,
			'updated_at' => date('Y-m-d H:i:s')
		);

		$where = array('order_no' => $order_no);		
		$query = $this->m_qrc->Update('t_order_tng',$data_update_order_tng,$where);

		$return = '';
		if ($str_tx_status == 'SUCCEED') {
			$return = '1';
		} else if ($str_tx_status == 'FAIL') {
			$return = '0';
		}
		
		return $return;

		//update DB
		//$this->update_tx_order_tng($order_no,$str_tx_no,$str_tx_amount,$str_tx_status);

		/*$dataOrderTng = $this->m_qrc->GetOrderTngDetail($order_no);
		$template_data = array(
			'data_order_no' => $dataOrderTng[0]['order_no'],
			'data_ksm_no_id' => $dataOrderTng[0]['ksm_no_id'], 
			'data_ksm_nama' => $dataOrderTng[0]['ksm_nama'],
			'data_ksm_keterangan' => $dataOrderTng[0]['ksm_keterangan'],
			'data_amount' => $dataOrderTng[0]['tng_amount'],
			'data_imageqrcode' => $dataOrderTng[0]['tng_qrcode_photo'], 
			'data_status' => $dataOrderTng[0]['tng_status']
		);

		$this->load->template_qrc('qrc/v_transaksi_detail', $template_data);*/
	}

	/*function update_tx_order_tng($order_no,$tng_tx_no,$tng_amount,$tng_status)
	{
		if ($tng_amount == '') {
			$data_update_order_tng = array(
				'tng_tx_no' => $tng_tx_no,
				'tng_status' => $tng_status,
				'updated_at' => date('Y-m-d H:i:s')
			);
		} else {
			$data_update_order_tng = array(
				'tng_tx_no' => $tng_tx_no,
				'tng_amount' => $tng_amount,
				'tng_status' => $tng_status,
				'updated_at' => date('Y-m-d H:i:s')
			);
		}

		$where = array('order_no' => $order_no);		
		$query = $this->m_qrc->Update('t_order_tng',$data_update_order_tng,$where);
	}*/

}