<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {

	public function template_data()
	{
		$headerCategories = $this->m_template->GetCategories();
		$headerCategorySub = $this->m_template->GetCategorySub();
		$headerCategoryType = $this->m_template->GetCategoryType();
		
		$arrayData = array(
			'data_categories' => $headerCategories,
			'data_category_sub' => $headerCategorySub,
			'data_category_type' => $headerCategoryType
		);

		return $arrayData;
	}

	public function index()
	{
		$template_data = $this->template_data();

		$this->load->template_front('front/v_user_cart', $template_data);
	}
}
