<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	public function index()
	{
		$arrayStatistic['data_statistic_product_view'] = $this->m_statistic->GetProductViewStatistic();
		$arrayStatistic['data_statistic_slide_view'] = $this->m_statistic->GetSlideViewStatistic();
		$arrayStatistic['data_statistic_web_view_location'] = $this->m_statistic->GetWebViewLocationStatistic();
		$arrayStatistic['data_statistic_web_view'] = $this->m_statistic->GetWebViewStatistic();
		$arrayStatistic['data_statistic_web_view_bulan'] = $this->m_statistic->AllMonth();

		$this->load->template_back('back/v_main', $arrayStatistic);
	}

	//EXAMPLE
	public function statistic_product_view()
	{
		$dataStatisticProductView = $this->m_statistic->GetProductViewStatistic();
		echo json_encode($dataStatisticProductView);
	}

	//EXAMPLE
	public function statistic_slide_view()
	{
		$dataStatisticSlideView = $this->m_statistic->GetSlideViewStatistic();
		echo json_encode($dataStatisticSlideView);
	}

	//EXAMPLE
	public function statistic_web_view()
	{
		$dataStatisticWebView = $this->m_statistic->GetWebViewStatistic();
		echo json_encode($dataStatisticWebView);
	}

	//EXAMPLE
	public function statistic_web_view_location()
	{
		$dataStatisticWebViewLocation = $this->m_statistic->GetWebViewLocationStatistic();
		echo json_encode($dataStatisticWebViewLocation);
	}

	//EXAMPLE
	public function bulan()
	{
		$data = $this->m_statistic->AllMonth();
		echo json_encode($data);
	}
}
