<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promo extends CI_Controller {

	public function index()
	{	
		$dataPromoList = $this->m_promo->GetPromo();
		$arrayData = array(
			'data_promo' => $dataPromoList
		);

		$this->load->template_back('back/v_promo', $arrayData);
	}

	public function insert()
	{
		$this->load->template_back('back/v_promo_insert');
	}

	public function insert_do()
	{
		$name = $_POST['post_name'];
		$price = $_POST['post_price'];
		$description = $_POST['post_description'];
		$created_at = date('Y-m-d H:i:s');

		if(!empty($_FILES['post_file']['name'])){

			$file = $_FILES['post_file']['name'];
            $ext = pathinfo($file, PATHINFO_EXTENSION);
            $file_name = 'promo-'.date('ymdHis').'.'.$ext;
            $file_path = 'assets/images/promo/';

            $config['upload_path'] = $file_path;
            $config['allowed_types'] = 'jpg|jpeg|png';
            $config['file_name'] = $file_name;

			$this->load->library('upload',$config);
			$this->upload->do_upload('post_file');
		}

		$data_promo = array(
			'promo_name' => $name,
			'promo_price' => $price,
			'promo_file_path' => $file_path,
			'promo_file_name' => $file_name,
			'promo_description' => $description,
			'created_at' => $created_at
		);

		$this->m_promo->Insert('c_promo',$data_promo);

		redirect('admin/promo');
	}

	public function delete($id_promo)
	{
		//DELETE FILE
		$dataFile = $this->m_promo->GetPromoDetail($id_promo);
		foreach ($dataFile as $data) {
			$file_path = $data['promo_file_path'];
			$file_name = $data['promo_file_name'];

			$src = '/'.$file_path.$file_name;
			if (file_exists(getcwd() . $src)) {
			  unlink(getcwd() . $src);
			}
		}

		//DELETE DATA DB
		$where = array('id_promo' => $id_promo);
		$query = $this->m_promo->Delete('c_promo',$where);

		if ($query >= 1) {
			redirect('admin/promo');
		} else {
			echo "Delete Data Gagal";
		}
	}

	public function update($id_promo)
	{
		$dataPromoDetail = $this->m_promo->GetPromoDetail($id_promo);
		$template_data = array(
			'id_promo' => $dataPromoDetail[0]['id_promo'],
			'promo_name' => $dataPromoDetail[0]['promo_name'],
			'promo_price' => $dataPromoDetail[0]['promo_price'],
			'promo_file_path' => $dataPromoDetail[0]['promo_file_path'],
			'promo_file_name' => $dataPromoDetail[0]['promo_file_name'],
			'promo_description' => $dataPromoDetail[0]['promo_description']
		);

		$this->load->template_back('back/v_promo_update', $template_data);
	}

	public function update_do()
	{
		$id_promo = $_POST['post_id_promo'];
		$promo_name = $_POST['post_name'];
		$promo_price = $_POST['post_price'];
		$promo_file_path = $_POST['post_file_path'];
		$promo_file_name = $_POST['post_file_name'];
		$promo_description = $_POST['post_description'];
		$updated_at = date('Y-m-d H:i:s');

		if(!empty($_FILES['post_file']['name'])){

            $config['upload_path'] = $promo_file_path;
            $config['allowed_types'] = 'jpg|jpeg|png';
            $config['file_name'] = $promo_file_name;
            $config['overwrite'] = TRUE;

			$this->load->library('upload',$config);
			$this->upload->do_upload('post_file');
		}

		$data_update_promo = array(
			'promo_name' => $promo_name,
			'promo_price' => $promo_price,
			'promo_description' => $promo_description,
			'updated_at' => $updated_at
		);

		$where = array('id_promo' => $id_promo);		
		$query = $this->m_promo->Update('c_promo',$data_update_promo,$where);

		if ($query >= 1) {
			redirect('admin/promo');
		} else {
			echo "Update Data Gagal";
		}
	}
}