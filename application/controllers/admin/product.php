<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

	public function index()
	{	
		$dataProductList = $this->m_product_admin->GetProductList();
		$arrayData = array(
			'data_product' => $dataProductList
		);

		$this->load->template_back('back/v_product', $arrayData);
	}

	public function insert()
	{
		$dataCategories = $this->m_category->GetAllCategories();
		$dataCategorySub = $this->m_category->GetAllCategorySub();
		
		$template_data = array(
			'data_categories' => $dataCategories,
			'data_category_sub' => $dataCategorySub
		);

		$this->load->template_back('back/v_product_insert', $template_data);
	}

	public function insert_do()
	{

		$product_id_category = $_POST['post_category'];
		$product_id_sub_category = $_POST['post_sub_category'];
		$product_name = $_POST['post_name'];
		$product_price = $_POST['post_price'];
		$product_price_discount = $_POST['post_price_discount'];
		//$product_specification = $_POST['post_specification'];
		$product_description = $_POST['post_description'];
		$created_at = date('Y-m-d H:i:s');

		//product tag
		/*$product_tag = '';
		foreach ($_REQUEST['post_tag'] as $selectTag) {
			$product_tag = $selectTag.' '.$product_tag;
		}*/

		$data_product = array(
			'product_name' => $product_name,
			'price_regular' => $product_price,
			'price_discount' => $product_price_discount,
			//'specification' => $product_specification,
			'description' => $product_description,
			'created_at' => $created_at
		);
		

		//START INSERT
		$this->db->trans_start();

		//INSERT PRODUCT
		$insert_product_id = $this->m_product_admin->InsertProduct('p_products',$data_product);

		//INSERT PRODUCT CATEGORY
		$data_product_category = array(
			'id_products' => $insert_product_id,
			'id_categories' => $product_id_category,
			'id_category_sub' => $product_id_sub_category,
			//'product_tags' => $product_tag,
			'created_at' => $created_at
		);
		$query_product_category = $this->m_product_admin->InsertProductCategory('p_product_categories',$data_product_category);


		//INSERT PRODUCT FILES
        if(!empty($_FILES['post_files']['name'][0])){
            $filesCount = count($_FILES['post_files']['name']);
            for($i = 0; $i < $filesCount; $i++){
                $_FILES['post_file']['name'] = $_FILES['post_files']['name'][$i];
                $_FILES['post_file']['type'] = $_FILES['post_files']['type'][$i];
                $_FILES['post_file']['tmp_name'] = $_FILES['post_files']['tmp_name'][$i];
                $_FILES['post_file']['error'] = $_FILES['post_files']['error'][$i];
                $_FILES['post_file']['size'] = $_FILES['post_files']['size'][$i];

                $file = $_FILES['post_file']['name'];
                $ext = pathinfo($file, PATHINFO_EXTENSION);
                $file_name = date('ymdHis').'-'.$i.'.'.$ext;
                $file_path = 'assets/images/products/';

                $config['upload_path'] = $file_path;
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['file_name'] = $file_name;
                
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if($this->upload->do_upload('post_file')){
                    $uploadData[$i]['id_products'] = $insert_product_id;
                    $uploadData[$i]['file_path'] = $file_path;
                    $uploadData[$i]['file_name'] = $file_name;
                    $uploadData[$i]['created_at'] = $created_at;
                }
            }
            
            if(!empty($uploadData)){
                $query_product_files = $this->m_product_admin->InsertProductFiles($uploadData);
            }
        }

		$this->db->trans_complete();
		//STOP INSERT

		redirect('admin/product');
	}

	public function detail($id_products)
	{
		$dataCategories = $this->m_category->GetAllCategories();
		$dataCategorySub = $this->m_category->GetAllCategorySub();
		$dataProductDetail = $this->m_product_admin->GetProductDetail($id_products);
		$dataProductFile = $this->m_product_admin->GetProductFile($id_products);
		$template_data = array(
			'data_categories' => $dataCategories,
			'data_category_sub' => $dataCategorySub,
			'data_product_file' => $dataProductFile,
			'id_products' => $dataProductDetail[0]['id_products'],
			'c_name' => $dataProductDetail[0]['c_name'],
			'cs_name' => $dataProductDetail[0]['cs_name'],
			'product_name' => $dataProductDetail[0]['product_name'],
			'price_regular' => $dataProductDetail[0]['price_regular'],
			'price_discount' => $dataProductDetail[0]['price_discount'],
			'description' => $dataProductDetail[0]['description']
		);

		$this->load->template_back('back/v_product_detail', $template_data);
	}

	public function delete($id_products)
	{
		//DELETE PRODUCT FILE
		$dataFile = $this->m_product_admin->GetProductFile($id_products);
		foreach ($dataFile as $data) {
			$file_path = $data['file_path'];
			$file_name = $data['file_name'];

			$src = '/'.$file_path.$file_name;
			if (file_exists(getcwd() . $src)) {
			  unlink(getcwd() . $src);
			}
		}

		//DELETE DATA DB
		$where = array('id_products' => $id_products);
		$query = $this->m_product_admin->ProductDelete('p_products',$where);
		$query = $this->m_product_admin->ProductDelete('p_product_categories',$where);
		$query = $this->m_product_admin->ProductDelete('p_product_files',$where);

		if ($query >= 1) {
			redirect('admin/product');
		} else {
			echo "Delete Data Gagal";
		}
	}

	public function update($id_products)
	{
		$dataCategories = $this->m_category->GetAllCategories();
		$dataCategorySub = $this->m_category->GetAllCategorySub();
		$dataProductDetail = $this->m_product_admin->GetProductDetail($id_products);
		$dataProductFile = $this->m_product_admin->GetProductFile($id_products);
		$template_data = array(
			'data_categories' => $dataCategories,
			'data_category_sub' => $dataCategorySub,
			'data_product_file' => $dataProductFile,
			'id_products' => $dataProductDetail[0]['id_products'],
			'id_categories' => $dataProductDetail[0]['id_categories'],
			'id_category_sub' => $dataProductDetail[0]['id_category_sub'],
			'product_name' => $dataProductDetail[0]['product_name'],
			'price_regular' => $dataProductDetail[0]['price_regular'],
			'price_discount' => $dataProductDetail[0]['price_discount'],
			'description' => $dataProductDetail[0]['description']
		);

		$this->load->template_back('back/v_product_update', $template_data);
	}

	public function update_do()
	{
		$product_id_products = $_POST['post_id_products'];
		$product_id_category = $_POST['post_category'];
		$product_id_sub_category = $_POST['post_sub_category'];
		$product_name = $_POST['post_name'];
		$product_price = $_POST['post_price'];
		$product_price_discount = $_POST['post_price_discount'];
		$product_description = $_POST['post_description'];
		$created_at = date('Y-m-d H:i:s');
		$updated_at = date('Y-m-d H:i:s');

		$data_update_product = array(
			'product_name' => $product_name,
			'price_regular' => $product_price,
			'price_discount' => $product_price_discount,
			'description' => $product_description,
			'updated_at' => $updated_at
		);

		$data_update_product_category = array(
			'id_categories' => $product_id_category,
			'id_category_sub' => $product_id_sub_category,
			'updated_at' => $updated_at
		);
		

		//PROSES UDPATE FILE : HAPUS LAMA > INSERT BARU
        if(!empty($_FILES['post_files']['name'][0])){
            
            //DELETE PRODUCT FILE LAMA
			$dataFile = $this->m_product_admin->GetProductFile($product_id_products);
			foreach ($dataFile as $data) {
				$file_path = $data['file_path'];
				$file_name = $data['file_name'];

				$src = '/'.$file_path.$file_name;
				if (file_exists(getcwd() . $src)) {
				  unlink(getcwd() . $src);
				}
			}
			$where = array('id_products' => $product_id_products);
			$query = $this->m_product_admin->ProductDelete('p_product_files',$where);

			//INSERT PRODUCT FILES BARU
            $filesCount = count($_FILES['post_files']['name']);
            for($i = 0; $i < $filesCount; $i++){
                $_FILES['post_file']['name'] = $_FILES['post_files']['name'][$i];
                $_FILES['post_file']['type'] = $_FILES['post_files']['type'][$i];
                $_FILES['post_file']['tmp_name'] = $_FILES['post_files']['tmp_name'][$i];
                $_FILES['post_file']['error'] = $_FILES['post_files']['error'][$i];
                $_FILES['post_file']['size'] = $_FILES['post_files']['size'][$i];

                $file = $_FILES['post_file']['name'];
                $ext = pathinfo($file, PATHINFO_EXTENSION);
                $file_name = date('ymdHis').'-'.$i.'.'.$ext;
                $file_path = 'assets/images/products/';

                $config['upload_path'] = $file_path;
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['file_name'] = $file_name;
                
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if($this->upload->do_upload('post_file')){
                    $uploadData[$i]['id_products'] = $product_id_products;
                    $uploadData[$i]['file_path'] = $file_path;
                    $uploadData[$i]['file_name'] = $file_name;
                    $uploadData[$i]['created_at'] = $created_at;
                    $uploadData[$i]['updated_at'] = $updated_at;
                }
            }
            if(!empty($uploadData)){
                $query_product_files = $this->m_product_admin->InsertProductFiles($uploadData);
            }
        }

		$where = array('id_products' => $product_id_products);		

		$query = $this->m_product_admin->ProductUpdate('p_products',$data_update_product,$where);
		$query = $this->m_product_admin->ProductUpdate('p_product_categories',$data_update_product_category,$where);

		if ($query >= 1) {
			redirect('admin/product');
		} else {
			echo "Update Data Gagal";
		}
	}
}
