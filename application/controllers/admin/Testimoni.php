<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testimoni extends CI_Controller {

	public function index()
	{
		$dataTestimoniList = $this->m_promo->GetTestimoni();
		$arrayData = array(
			'data_testimoni' => $dataTestimoniList
		);

		$this->load->template_back('back/v_testimoni', $arrayData);
	}

	public function insert()
	{
		$this->load->template_back('back/v_testimoni_insert');
	}

	public function insert_do()
	{
		$created_at = date('Y-m-d H:i:s');

		if(!empty($_FILES['post_file']['name'])){

			$file = $_FILES['post_file']['name'];
            $ext = pathinfo($file, PATHINFO_EXTENSION);
            $file_name = 'testimoni-'.date('ymdHis').'.'.$ext;
            $file_path = 'assets/images/testimoni/';

            $config['upload_path'] = $file_path;
            $config['allowed_types'] = 'jpg|jpeg|png';
            $config['file_name'] = $file_name;

			$this->load->library('upload',$config);
			$this->upload->do_upload('post_file');
		}

		$data_testimoni = array(
			'testi_file_path' => $file_path,
			'testi_file_name' => $file_name,
			'created_at' => $created_at
		);

		$this->m_promo->Insert('c_testimoni',$data_testimoni);

		redirect('admin/testimoni');
	}

	public function delete($id_testimoni)
	{
		//DELETE FILE
		$dataFile = $this->m_promo->GetTestimoniDetail($id_testimoni);
		foreach ($dataFile as $data) {
			$file_path = $data['testi_file_path'];
			$file_name = $data['testi_file_name'];

			$src = '/'.$file_path.$file_name;
			if (file_exists(getcwd() . $src)) {
			  unlink(getcwd() . $src);
			}
		}

		//DELETE DATA DB
		$where = array('id_testimoni' => $id_testimoni);
		$query = $this->m_promo->Delete('c_testimoni',$where);

		if ($query >= 1) {
			redirect('admin/testimoni');
		} else {
			echo "Delete Data Gagal";
		}
	}

	public function update($id_testimoni)
	{
		$dataTestimoniDetail = $this->m_promo->GetTestimoniDetail($id_testimoni);
		$template_data = array(
			'id_testimoni' => $dataTestimoniDetail[0]['id_testimoni'],
			'testi_file_path' => $dataTestimoniDetail[0]['testi_file_path'],
			'testi_file_name' => $dataTestimoniDetail[0]['testi_file_name']
		);

		$this->load->template_back('back/v_testimoni_update', $template_data);
	}

	public function update_do()
	{
		$id_testimoni = $_POST['post_id_testimoni'];
		$file_path = $_POST['post_file_path'];
		$file_name = $_POST['post_file_name'];
		$updated_at = date('Y-m-d H:i:s');

		if(!empty($_FILES['post_file']['name'])){

            $config['upload_path'] = $file_path;
            $config['allowed_types'] = 'jpg|jpeg|png';
            $config['file_name'] = $file_name;
            $config['overwrite'] = TRUE;

			$this->load->library('upload',$config);
			$this->upload->do_upload('post_file');
		}

		$data_update_testimoni = array(
			'updated_at' => $updated_at
		);

		$where = array('id_testimoni' => $id_testimoni);		
		$query = $this->m_promo->Update('c_testimoni',$data_update_testimoni,$where);

		if ($query >= 1) {
			redirect('admin/testimoni');
		} else {
			echo "Update Data Gagal";
		}
	}
}