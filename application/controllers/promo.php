<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promo extends CI_Controller {

	
	public function template_data()
	{
		$dataCategories = $this->m_template->GetCategories();
		$dataCategorySub = $this->m_template->GetCategorySub();
		$dataCategoryType = $this->m_template->GetCategoryType();

		$arrayData = array(
			'data_categories' => $dataCategories,
			'data_category_sub' => $dataCategorySub,
			'data_category_type' => $dataCategoryType
		);

		return $arrayData;
	}

	public function index()
	{
		$template_data = $this->template_data();

		$this->load->template_front('front/v_promo', $template_data);
	}

	public function detail()
	{
		
	}
}
