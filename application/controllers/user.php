<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function template_data()
	{
		$headerCategories = $this->m_template->GetCategories();
		$headerCategorySub = $this->m_template->GetCategorySub();
		$headerCategoryType = $this->m_template->GetCategoryType();

		$dataCicilanList = $this->m_cicilan->GetCicilanListPerMember($this->session->userdata('member_id'));
		
		$arrayData = array(
			'data_categories' => $headerCategories,
			'data_category_sub' => $headerCategorySub,
			'data_category_type' => $headerCategoryType,
			'data_cicilan' => $dataCicilanList
		);

		return $arrayData;
	}

	/*public function profile()
	{
		$template_data = $this->template_data();
		$this->load->template_front('front/v_user_profile', $template_data);
	}

	public function orders()
	{
		$template_data = $this->template_data();
		$this->load->template_front('front/v_user_orders', $template_data);
	}

	public function cart()
	{
		$template_data = $this->template_data();
		$this->load->template_front('front/v_user_cart', $template_data);
	}*/


	public function transaction()
	{

		$template_data = $this->template_data();
		$this->load->template_front('front/v_user_transaction', $template_data);
	}
}
