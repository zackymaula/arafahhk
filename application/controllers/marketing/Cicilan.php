<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cicilan extends CI_Controller {

	public function index()
	{
		$dataCicilanList = $this->m_cicilan->GetCicilanListPerMarketing($this->session->userdata('user_id'));
		$arrayData = array(
			'data_cicilan' => $dataCicilanList
		);

		$this->load->template_marketing('marketing/v_cicilan_list', $arrayData);
	}

}