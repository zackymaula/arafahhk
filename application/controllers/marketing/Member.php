<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Controller {

	public function index()
	{
		$dataMemberList = $this->m_member->GetListMember();
		$arrayData = array(
			'data_member' => $dataMemberList
		);

		$this->load->template_marketing('marketing/v_member_list', $arrayData);
	}

	public function detail($id_members)
	{
		$detailMember = $this->m_member->GetMemberDetail($id_members);

		$arrayData = array(

			'id_members' => $detailMember[0]['id_members'],
			'no_member' => $detailMember[0]['no_member'],
			'pass_member' => $detailMember[0]['pass_member'],
			'm_nama' => $detailMember[0]['m_nama'],
			'm_lahir_tmpt' => $detailMember[0]['m_lahir_tmpt'],
			'm_lahir_tgl' => (DateTime::createFromFormat('d/m/Y', $detailMember[0]['m_lahir_tgl']))->format('d F Y'),
			'm_status_nikah' => $detailMember[0]['m_status_nikah'],
			'm_alamat_indo' => $detailMember[0]['m_alamat_indo'],
			'm_nama_ayah' => $detailMember[0]['m_nama_ayah'],
			'm_nama_ibu' => $detailMember[0]['m_nama_ibu'],
			'm_hp_indo' => $detailMember[0]['m_hp_indo'],
			'm_hp_luar' => $detailMember[0]['m_hp_luar'],
			'm_status_rumah' => $detailMember[0]['m_status_rumah'],
			'm_pekerjaan' => $detailMember[0]['m_pekerjaan'],
			'm_pekerjaan_no_id_luar' => $detailMember[0]['m_pekerjaan_no_id_luar'],
			'm_pekerjaan_masa_berlaku' => $detailMember[0]['m_pekerjaan_masa_berlaku'],
			'm_pekerjaan_alamat' => $detailMember[0]['m_pekerjaan_alamat'],
			'm_pekerjaan_teman_nama' => $detailMember[0]['m_pekerjaan_teman_nama'],
			'm_pekerjaan_teman_hp' => $detailMember[0]['m_pekerjaan_teman_hp'],
			'm_medsos_line' => $detailMember[0]['m_medsos_line'],
			'm_medsos_fb' => $detailMember[0]['m_medsos_fb'],
			'm_medsos_ig' => $detailMember[0]['m_medsos_ig'],
			'm_medsos_wa' => $detailMember[0]['m_medsos_wa'],
			'm_file_id_card' => $detailMember[0]['m_file_id_card'],
			'm_file_selfie' => $detailMember[0]['m_file_selfie'],
			'm_file_lain' => $detailMember[0]['m_file_lain'],
			'm_file_lain_2' => $detailMember[0]['m_file_lain_2'],
			'm_selfie_pilihan' => $detailMember[0]['m_selfie_pilihan']
		);

		$this->load->template_marketing('marketing/v_member_detail', $arrayData);
	}
}