<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_promo extends CI_Model {

	//MAIN + ADMIN LIST
	public function GetSlidersMain()
	{
		$this->db->select('*');
		$this->db->from('c_sliders');
		$this->db->order_by('created_at', 'desc');
		$this->db->where('id_staff', '0');
		$query = $this->db->get();
		return $query->result_array();
	}

	//MAIN | STAFF
	public function GetSliderStaffAll()
	{
		$this->db->select('*');
		$this->db->from('c_sliders cs');
		$this->db->join('u_staff us', 'cs.id_staff = us.id_staff');
		$this->db->order_by('cs.id_staff', 'asc');
		$query = $this->db->get();
		return $query->result_array();
	}

	//MAIN + ADMIN UPDATE DETAIL
	public function GetSlideDetail($id_slide)
	{
		$this->db->select('*');
		$this->db->from('c_sliders');
		$this->db->where('id_slide', $id_slide);
		$query = $this->db->get();
		return $query->result_array();
	}

	//MAIN + ADMIN LIST
	public function GetPaket()
	{
		$this->db->select('*');
		$this->db->from('c_paket');
		$this->db->order_by('created_at', 'desc');
		$query = $this->db->get();
		return $query->result_array();
	}

	//ADMIN UPDATE DETAIL
	public function GetPaketDetail($id_paket)
	{
		$this->db->select('*');
		$this->db->from('c_paket');
		$this->db->where('id_paket', $id_paket);
		$query = $this->db->get();
		return $query->result_array();
	}

	//MAIN + ADMIN LIST
	public function GetPromo()
	{
		$this->db->select('*');
		$this->db->from('c_promo');
		$this->db->order_by('created_at', 'desc');
		$query = $this->db->get();
		return $query->result_array();
	}

	//ADMIN UPDATE DETAIL
	public function GetPromoDetail($id_promo)
	{
		$this->db->select('*');
		$this->db->from('c_promo');
		$this->db->where('id_promo', $id_promo);
		$query = $this->db->get();
		return $query->result_array();
	}

	//MAIN + ADMIN LIST
	public function GetTestimoni()
	{
		$this->db->select('*');
		$this->db->from('c_testimoni');
		$this->db->order_by('created_at', 'desc');
		$query = $this->db->get();
		return $query->result_array();
	}

	//ADMIN UPDATE DETAIL
	public function GetTestimoniDetail($id_testimoni)
	{
		$this->db->select('*');
		$this->db->from('c_testimoni');
		$this->db->where('id_testimoni', $id_testimoni);
		$query = $this->db->get();
		return $query->result_array();
	}

	//ADMIN INSERT
	public function Insert($tabelName,$data)
	{
		$res = $this->db->insert($tabelName,$data);
		return $res;
	}

	//ADMIN DELETE
    public function Delete($tabelName,$where)
	{
		$res = $this->db->delete($tabelName,$where);
		return $res;
	}

	//ADMIN UPDATE
	public function Update($tabelName,$data,$where)
	{
		$res = $this->db->update($tabelName,$data,$where);
		return $res;
	}
}