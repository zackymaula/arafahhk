<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_generate extends CI_Model {

	

	//MARKETING GENERATE
	public function Insert($tabelName,$data)
	{
		$res = $this->db->insert($tabelName,$data);
		return $res;
	}

	//MARKETING GENERATE DELETE
    public function Delete($tabelName,$where)
	{
		$res = $this->db->delete($tabelName,$where);
		return $res;
	}

	//MARKETING GENERATE UPDATE
	public function Update($tabelName,$data,$where)
	{
		$res = $this->db->update($tabelName,$data,$where);
		return $res;
	}

}