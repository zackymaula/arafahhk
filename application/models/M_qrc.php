<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_qrc extends CI_Model {

	//ADMIN USER
	public function GetOrderTngDetail($where)
	{
		$this->db->select('*');
		$this->db->from('t_order_tng');
		$this->db->where('order_no', $where);
		$query = $this->db->get();
		return $query->result_array();
	}

	//QRC
	public function Insert($tabelName,$data)
	{
		$res = $this->db->insert($tabelName,$data);
		return $res;
	}

	//QRC DELETE
    public function Delete($tabelName,$where)
	{
		$res = $this->db->delete($tabelName,$where);
		return $res;
	}

	//QRC UPDATE
	public function Update($tabelName,$data,$where)
	{
		$res = $this->db->update($tabelName,$data,$where);
		return $res;
	}

}