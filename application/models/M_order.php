<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_order extends CI_Model {

	//INSERT ORDER
	/*public function Insert($tabelName,$data)
	{
		$res = $this->db->insert($tabelName,$data);
		return $res;
	}*/

	//INSERT ORDER
	public function Insert($tabelName,$data)
	{
		$res = $this->db->insert($tabelName,$data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}

	public function InsertBarang($data = array()){
        $res = $this->db->insert_batch('t_order_items',$data);
        return $res;
    }

    // DELETE
    public function Delete($tabelName,$where)
	{
		$res = $this->db->delete($tabelName,$where);
		return $res;
	}

    // UPDATE
	public function Update($tabelName,$data,$where)
	{
		$res = $this->db->update($tabelName,$data,$where);
		return $res;
	}

    // LIST ORDER
    public function GetOrderList()
    {
    	$this->db->select('*, o.created_at AS tgl_order');
		$this->db->from('t_orders o');
		$this->db->join('u_members m', 'o.id_members = m.id_members');
		$this->db->join('t_order_sign os', 'o.id_order_sign = os.id_order_sign', 'left');
		$this->db->join('t_order_receiver or', 'o.id_order_receiver = or.id_order_receiver');
		$this->db->join('t_ref_order_status ros', 'o.id_order_status = ros.id_order_status');
		$this->db->group_by('o.id_orders');
		$this->db->order_by('o.created_at', 'desc');
		$query = $this->db->get();
		return $query->result_array();
    }

    public function GetOrderListPerMarketing($id_user_marketing)
    {
    	$this->db->select('*, o.created_at AS tgl_order');
		$this->db->from('t_orders o');
		$this->db->join('u_members m', 'o.id_members = m.id_members');
		$this->db->join('t_order_sign os', 'o.id_order_sign = os.id_order_sign', 'left');
		$this->db->join('t_order_receiver or', 'o.id_order_receiver = or.id_order_receiver');
		$this->db->join('t_ref_order_status ros', 'o.id_order_status = ros.id_order_status');
		$this->db->group_by('o.id_orders');
		$this->db->order_by('o.created_at', 'desc');
		$this->db->where('o.id_user_marketing', $id_user_marketing);
		$query = $this->db->get();
		return $query->result_array();
    }

    //MAIN REVIEW CUSTOMERS, GET DETAIL ORDERS BY NO-ORDER-GENERATE
    public function GetDetailOrderByNoOrderGenerate($no_order_generate)
    {
    	$this->db->select('*');
		$this->db->from('t_orders o');
		$this->db->join('u_members m', 'o.id_members = m.id_members');
		$this->db->join('t_order_sign os', 'o.id_order_sign = os.id_order_sign', 'left');
		$this->db->join('t_order_receiver or', 'o.id_order_receiver = or.id_order_receiver');
		$this->db->join('t_ref_order_status ros', 'o.id_order_status = ros.id_order_status');
		$this->db->group_by('o.id_orders');
		$this->db->where('o.no_order_generate', $no_order_generate);
		$query = $this->db->get();
		return $query->result_array();
    }

    //MAIN REVIEW CUSTOMERS, GET LIST BARANG YG DI ORDER
    public function GetListBarangOrders($id_orders)
    {
    	$this->db->select('*');
		$this->db->from('t_order_items');
		$this->db->where('id_orders', $id_orders);
		$query = $this->db->get();
		return $query->result_array();
    }

    //GET DETAIL ORDERS, MARKETING | MANAGER
    public function GetDetailOrder($id_orders)
    {
    	$this->db->select('*');
		$this->db->from('t_orders o');
		$this->db->join('u_members m', 'o.id_members = m.id_members');
		$this->db->join('t_order_sign os', 'o.id_order_sign = os.id_order_sign', 'left');
		$this->db->join('t_order_receiver or', 'o.id_order_receiver = or.id_order_receiver');
		$this->db->join('t_ref_order_status ros', 'o.id_order_status = ros.id_order_status');
		$this->db->group_by('o.id_orders');
		$this->db->where('o.id_orders', $id_orders);
		$query = $this->db->get();
		return $query->result_array();
    }

    //MARKETING GENERATE 
	public function GetSubTotalOrder($id_orders)
	{
		$this->db->select('sum(oi_total) as sub_total');
		$this->db->from('t_order_items');
		$this->db->where('id_orders', $id_orders);
		$query = $this->db->get();		
		$data = $query->row();
		$sub_total = $data->sub_total;
		return $sub_total;
	}
}