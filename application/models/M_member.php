<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_member extends CI_Model {

    public function GetListMember()
	{
		$this->db->select('*');
		$this->db->from('u_members');
		$this->db->order_by('created_at', 'desc');
		$query = $this->db->get();
		return $query->result_array();
	}

    public function GetMemberDetail($id_members)
    {
    	$this->db->select('*');
		$this->db->from('u_members');
		$this->db->where('id_members', $id_members);
		$query = $this->db->get();
		return $query->result_array();
    }
}