<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Viewer extends CI_Model {

	/*public function InsertViewWebsite()
	{
		$arrayLocation = $this->m_location->ip_info("Visitor", "Location");

		$tabelName = 's_website_view';
		$data = array(
			'country' => $arrayLocation['country'],
			'country_code' => $arrayLocation['country_code'],
			'state' => $arrayLocation['state'],
			'city' => $arrayLocation['city'],
			'ip_address' => $arrayLocation['ip_address'],
			'created_at' => date('Y-m-d H:i:s')
		);

		$res = $this->db->insert($tabelName,$data);
		return $res;
	}*/

	//LOCALHOST
	public function InsertViewWebsite()
	{
		$tabelName = 's_website_view';
		$data = array(
			'created_at' => date('Y-m-d H:i:s')
		);

		$res = $this->db->insert($tabelName,$data);
		return $res;
	}

	public function InsertViewCategory($id_categories)
	{
		$tabelName = 's_category_view';
		$data = array(
			'id_categories' => $id_categories,
			'created_at' => date('Y-m-d H:i:s')
		);

		$res = $this->db->insert($tabelName,$data);
		return $res;
	}

	/*public function InsertViewCategorySub($id_category_sub)
	{
		$tabelName = 's_category_sub_view';
		$data = array(
			'id_category_sub' => $id_category_sub,
			'created_at' => date('Y-m-d H:i:s')
		);

		$res = $this->db->insert($tabelName,$data);
		return $res;
	}*/

	public function InsertViewProduct($id_products)
	{
		$tabelName = 's_product_view';
		$data = array(
			'id_products' => $id_products,
			'created_at' => date('Y-m-d H:i:s')
		);

		$res = $this->db->insert($tabelName,$data);
		return $res;
	}

	public function InsertViewSlide($id_slide)
	{
		$tabelName = 's_slide_view';
		$data = array(
			'id_slide' => $id_slide,
			'created_at' => date('Y-m-d H:i:s')
		);

		$res = $this->db->insert($tabelName,$data);
		return $res;
	}
}