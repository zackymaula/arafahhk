<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Product_Admin extends CI_Model {

	
	public function InsertProduct($tabelName,$data)
	{
		$res = $this->db->insert($tabelName,$data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}

	public function InsertProductCategory($tabelName,$data)
	{
		$res = $this->db->insert($tabelName,$data);
		return $res;
	}

	public function InsertProductFiles($data = array()){
        $res = $this->db->insert_batch('p_product_files',$data);
        return $res;
    }

	// LIST PRODUCT
    public function GetProductList()
    {
    	$this->db->select('p.id_products,
    						p.product_name, 
    						c.name as c_name, 
    						cs.name as cs_name,
    						p.price_regular');
		$this->db->from('p_products p');
		$this->db->join('p_product_categories pc', 'p.id_products = pc.id_products');
		$this->db->join('p_categories c', 'pc.id_categories = c.id_categories');
		$this->db->join('p_category_sub cs', 'pc.id_category_sub = cs.id_category_sub', 'left');
		$this->db->group_by('p.id_products');
		$this->db->order_by('p.created_at', 'desc');
		$query = $this->db->get();
		return $query->result_array();
    }

    // GET DATA PRODUCT FILE
    public function GetProductFile($where)
	{
		$this->db->select('*');
		$this->db->from('p_product_files');
		$this->db->where('id_products', $where);
		$query = $this->db->get();
		return $query->result_array();
	}

	//GET PRODUCT DETAIL
	public function GetProductDetail($where)
	{
		$this->db->select('p.id_products,
							c.id_categories,
							cs.id_category_sub,
    						p.product_name, 
    						c.name as c_name, 
    						cs.name as cs_name,
    						p.price_regular,
    						p.price_discount,
    						p.description');
		$this->db->from('p_products p');
		$this->db->join('p_product_categories pc', 'p.id_products = pc.id_products');
		$this->db->join('p_categories c', 'pc.id_categories = c.id_categories');
		$this->db->join('p_category_sub cs', 'pc.id_category_sub = cs.id_category_sub', 'left');
		$this->db->group_by('p.id_products');
		$this->db->where('p.id_products', $where);
		$query = $this->db->get();
		return $query->result_array();
	}

    //ADMIN PRODUCT DELETE
    public function ProductDelete($tabelName,$where)
	{
		$res = $this->db->delete($tabelName,$where);
		return $res;
	}

	public function ProductUpdate($tabelName,$data,$where)
	{
		$res = $this->db->update($tabelName,$data,$where);
		return $res;
	}
}